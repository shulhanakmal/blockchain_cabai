<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('v1/login', 'API\AuthController@login');
Route::post('v1/login-roaster', 'API\AuthController@login_roaster');

//Traceability without Login Grosir App
Route::get('v1/detail-product/{code}/{id}', 'API\ProductController@detailProdukGuest');
Route::get('v1/list-batch/{code}', 'API\BatchController@indexGuest');
Route::get('v1/add-detail/{code}', 'API\BatchController@add_detailGuest');
Route::get('v1/detail-batch/{code}/{id}', 'API\BatchController@detailGuest');
Route::get('v1/get-unit-detail/{code}', 'API\UnitController@get_unit_detailGuest');
Route::get('v1/add-detail-product/{code}', 'API\ProductController@add_detailGuest');

//Traceability without Login Roastery App
Route::get('v1/detail-roasting/{code}/{id}', 'API\RoastingController@detailGuest');
Route::get('v1/roaster-detail-product/{code}/{id}', 'API\RoasterProductController@detailGuest');
Route::get('v1/detail-batch-by-batchID/{code}/{id}', 'API\BatchController@detailByBatchIDGuest');
Route::get('v1/add-detail-master-data/{code}', 'API\MasterDataController@add_detailGuest');

Route::get('{code}/order/data/{order}', 'APIController@order')->name('katalog.monitoring.order');

Route::group(['middleware' => 'auth:api'], function () {

    Route::get('v1/list-jenis', 'API\JenisController@index');
    Route::get('v1/detail-jenis/{id}', 'API\JenisController@detail');
    Route::get('v1/delete-jenis/{id}', 'API\JenisController@delete');
    Route::post('v1/add-jenis', 'API\JenisController@add');
    Route::post('v1/edit-jenis/{id}', 'API\JenisController@edit');

    Route::get('v1/list-supplier', 'API\SupplierController@index');
    Route::get('v1/detail-supplier/{id}', 'API\SupplierController@detail');
    Route::get('v1/delete-supplier/{id}', 'API\SupplierController@delete');
    Route::post('v1/add-supplier', 'API\SupplierController@add');
    Route::post('v1/edit-supplier/{id}', 'API\SupplierController@edit');

    Route::get('v1/list-batch', 'API\BatchController@index');
    Route::get('v1/detail-batch/{id}', 'API\BatchController@detail');
    Route::get('v1/delete-batch/{id}', 'API\BatchController@delete');
    Route::post('v1/add-batch', 'API\BatchController@add');
    Route::get('v1/add-detail', 'API\BatchController@add_detail');
    Route::post('v1/edit-batch/{id}', 'API\BatchController@edit');
    Route::get('v1/detail-batch-by-batchID/{id}', 'API\BatchController@detailByBatchID');

    Route::get('v1/list-product', 'API\ProductController@index');
    Route::get('v1/add-detail-product', 'API\ProductController@add_detail');
    Route::post('v1/add-product', 'API\ProductController@add');
    Route::get('v1/get-sub-category/{id}', 'API\ProductController@sub_category');
    Route::get('v1/detail-product/{id}', 'API\ProductController@detail');
    Route::get('v1/detail-produk/{id}', 'API\ProductController@detailProduk');
    Route::get('v1/delete-product/{id}', 'API\ProductController@delete');
    Route::post('v1/update-qr-product/{id}', 'API\ProductController@update_qr');

    Route::post('v1/add-biji', 'API\BijiController@add');
    Route::get('v1/delete-biji/{id}', 'API\BijiController@delete');
    Route::get('v1/detail-biji/{id}', 'API\BijiController@detail');
    Route::post('v1/edit-biji/{id}', 'API\BijiController@edit');

    Route::post('v1/add-unit', 'API\UnitController@add');
    Route::get('v1/delete-unit/{id}', 'API\UnitController@delete');
    Route::get('v1/get-unit-detail', 'API\UnitController@get_unit_detail');
    Route::get('v1/detail-unit/{id}', 'API\UnitController@detail');
    Route::post('v1/edit-unit/{id}', 'API\UnitController@edit');

    Route::post('v1/add-proses', 'API\ProsesController@add');
    Route::get('v1/delete-proses/{id}', 'API\ProsesController@delete');
    Route::get('v1/detail-proses/{id}', 'API\ProsesController@detail');
    Route::post('v1/edit-proses/{id}', 'API\ProsesController@edit');

    Route::get('v1/list-transaction-hash', 'API\TransactionExplorerController@index');
    Route::post('v1/add-transaction-hash', 'API\TransactionExplorerController@add');

    //app Roastery
    Route::get('v1/list-master-data', 'API\MasterDataController@index');

    Route::post('v1/add-roasting-profile', 'API\RoastingProfileController@add');
    Route::get('v1/delete-roasting-profile/{id}', 'API\RoastingProfileController@delete');
    Route::get('v1/detail-roasting-profile/{id}', 'API\RoastingProfileController@detail');
    Route::post('v1/edit-roasting-profile/{id}', 'API\RoastingProfileController@edit');

    Route::post('v1/add-whole-bean', 'API\WholeBeanController@add');
    Route::get('v1/delete-whole-bean/{id}', 'API\WholeBeanController@delete');
    Route::get('v1/detail-whole-bean/{id}', 'API\WholeBeanController@detail');
    Route::post('v1/edit-whole-bean/{id}', 'API\WholeBeanController@edit');

    Route::post('v1/add-roaster-unit', 'API\RoasterUnitController@add');
    Route::get('v1/delete-roaster-unit/{id}', 'API\RoasterUnitController@delete');
    Route::post('v1/edit-roaster-unit/{id}', 'API\RoasterUnitController@edit');
    Route::get('v1/detail-roaster-unit/{id}', 'API\RoasterUnitController@detail');

    Route::post('v1/add-roaster-jenis', 'API\RoasterJenisController@add');
    Route::get('v1/delete-roaster-jenis/{id}', 'API\RoasterJenisController@delete');
    Route::post('v1/edit-roaster-jenis/{id}', 'API\RoasterJenisController@edit');
    Route::get('v1/detail-roaster-jenis/{id}', 'API\RoasterJenisController@detail');

    Route::post('v1/add-roaster-biji', 'API\RoasterBijiController@add');
    Route::get('v1/delete-roaster-biji/{id}', 'API\RoasterBijiController@delete');
    Route::post('v1/edit-roaster-biji/{id}', 'API\RoasterBijiController@edit');
    Route::get('v1/detail-roaster-biji/{id}', 'API\RoasterBijiController@detail');

    Route::post('v1/add-roaster-proses', 'API\RoasterProsesController@add');
    Route::get('v1/delete-roaster-proses/{id}', 'API\RoasterProsesController@delete');
    Route::post('v1/edit-roaster-proses/{id}', 'API\RoasterProsesController@edit');
    Route::get('v1/detail-roaster-proses/{id}', 'API\RoasterProsesController@detail');

    Route::get('v1/list-roaster-supplier', 'API\RoasterSupplierController@index');
    Route::get('v1/detail-roaster-supplier/{id}', 'API\RoasterSupplierController@detail');
    Route::get('v1/delete-roaster-supplier/{id}', 'API\RoasterSupplierController@delete');
    Route::post('v1/add-roaster-supplier', 'API\RoasterSupplierController@add');
    Route::post('v1/edit-roaster-supplier/{id}', 'API\RoasterSupplierController@edit');

    Route::post('v1/add-weight', 'API\WeightController@add');
    Route::get('v1/delete-weight/{id}', 'API\WeightController@delete');

    Route::post('v1/add-roasting', 'API\RoastingController@add');
    Route::get('v1/delete-roasting/{id}', 'API\RoastingController@delete');
    Route::get('v1/list-roasting', 'API\RoastingController@index');
    Route::get('v1/detail-roasting/{id}', 'API\RoastingController@detail');
    Route::post('v1/edit-roasting/{id}', 'API\RoastingController@edit');
    Route::post('v1/update-qr-roasting/{id}', 'API\RoastingController@update_qr');

    Route::get('v1/roaster-list-product', 'API\RoasterProductController@index');
    Route::get('v1/roaster-add-detail', 'API\RoasterProductController@add_detail_roaster');
    Route::get('v1/roaster-add-detail-product', 'API\RoasterProductController@add_detail');
    Route::post('v1/roaster-add-product', 'API\RoasterProductController@add');
    Route::get('v1/roaster-get-sub-category/{id}', 'API\RoasterProductController@sub_category');
    Route::get('v1/roaster-detail-product/{id}', 'API\RoasterProductController@detail');
    Route::get('v1/roaster-delete-product/{id}', 'API\RoasterProductController@delete');
    Route::post('v1/roaster-update-qr-product/{id}', 'API\RoasterProductController@update_qr');
    Route::post('v1/roaster-edit-product/{id}', 'API\RoasterProductController@edit');
});

// app pabrik
    Route::post('v2/login', 'API\AuthController@loginPabrik');

    Route::get('v2/list-production', 'API\ProductionController@index');
    Route::post('v2/add-production', 'API\ProductionController@add');
    Route::get('v2/summary-production', 'API\ProductionController@summary');

    Route::get('v2/list-logistic', 'API\LogisticController@index');
    Route::get('v2/get-buyer-by-date/{date}', 'API\LogisticController@getBuyer');
    Route::get('v2/get-maxlength/{buyer}/{date}', 'API\LogisticController@getMaxlength');
    Route::post('v2/add-logistic', 'API\LogisticController@add');
    Route::get('v2/summary-logistic', 'API\LogisticController@summary');

    Route::get('v2/sales', 'API\SalesController@index');
    Route::post('v2/add-sales', 'API\SalesController@add');
    Route::get('v2/summary-sales', 'API\SalesController@summary');

    Route::get('v2/list-user', 'API\PabrikUserController@listUser');
    Route::post('v2/add-user', 'API\PabrikUserController@addUser');
// end app pabrik


