<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('cabai/login/Gapoktan', 'Auth\LoginController@showAdminLoginForm')->name('login.Gapoktan');
Route::post('cabai/login/Gapoktan', 'Auth\LoginController@adminLogin')->name('login.post.Gapoktan');

Route::get('cabai/login', 'Auth\LoginController@showCustomerLoginForm')->name('login.customer');
Route::post('cabai/login', 'Auth\LoginController@customerLogin')->name('login.post.customer');

Route::get('cabai/login/Petani', 'Auth\LoginController@showVendorLoginForm')->name('login.Petani');
Route::post('cabai/login/Petani', 'Auth\LoginController@vendorLogin')->name('login.post.Petani');


Route::group(['middleware' => 'auth:admin'], function () {
    Route::get('cabai/admin', 'Admin\AdminController@redirect')->name('admin.index');
});
Route::group(['middleware' => 'auth:vendor'], function () {
    Route::get('cabai/Petani', 'Vendor\VendorController@redirect')->name('vendor.index');
});
Route::group(['middleware' => 'auth:customer'], function () {
    Route::get('cabai/redirect', 'Katalog\KatalogController@redirect')->name('katalog.index');
});

Route::get('/', 'Katalog\GuestController@redirect')->name('guest.index');
Route::get('cabai/', 'Katalog\GuestController@index')->name('guest.index');
Route::get('cabai/detail/product/{code}/{hash}', 'Katalog\GuestController@detail_product')->name('guest.detail.product');

Route::get('cabai/daftar-tenant', 'Katalog\GuestController@daftarTenant')->name('daftar.tenant');
Route::post('cabai/daftar-tenant/simpan', 'Katalog\GuestController@storeDaftarTenant')->name('daftar.tenant.store');

Route::get('cabai/daftar-buyer', 'Katalog\GuestController@daftarBuyer')->name('daftar.buyer');
Route::post('cabai/daftar-buyer/simpan', 'Katalog\GuestController@storeDaftarBuyer')->name('daftar.buyer.store');
Route::get('cabai/getGroup/', 'Katalog\GuestController@getGroupCustomer')->name('get.group');

Route::get('cabai/login/reg-code', 'Katalog\RegistrationController@LoginIndex')->name('regcode');
Route::post('cabai/login/reg-code', 'Katalog\RegistrationController@Login')->name('login.regcode');
Route::get('cabai/reg-code/{hash}', 'Katalog\RegistrationController@index')->name('index.regcode');
// Route::group(['middleware' => 'auth:reg'], function () {
// });
Route::post('cabai/store/reg-code', 'Katalog\RegistrationController@store')->name('store.regcode');

Route::get('cabai/test', 'TestController@index')->name('test');
Route::get('cabai/index/7', 'TestController@index7')->name('index7');

Route::get('qrcode_blade', function () {
    return view('qr_code');
});
