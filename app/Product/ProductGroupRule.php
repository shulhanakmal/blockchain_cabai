<?php

namespace App\Product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductGroupRule extends Model {
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function product_group() {
        return $this->hasMany('\App\Product\ProductGroup', 'rule_id', 'id');
    }
}
