<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoasterInventory extends Model
{
    //
    public function product()
    {  
        return $this->belongsTo('App\RoasterProduct', 'bean_id');
    }
}
