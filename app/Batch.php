<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Batch extends Model
{
    protected $table = 'batchs';

    public function jenis()
    {  
        return $this->belongsTo('App\Jenis', 'jenis_id');
    }

    public function proses()
    {
        return $this->belongsTo('App\Proses', 'proses_id');
    }

    public function supplier()
    {
        return $this->belongsTo('App\Supplier', 'supplier_id');
    }

    public function biji()
    {
        return $this->belongsTo('App\Biji', 'biji_id');
    }
}
