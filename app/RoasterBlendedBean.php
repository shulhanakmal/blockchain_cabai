<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoasterBlendedBean extends Model
{
    //
    public function product()
    {  
        return $this->belongsTo('App\RoasterProduct', 'bean_id');
    }
}
