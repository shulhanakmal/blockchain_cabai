<?php

namespace App\Customer;

use Illuminate\Database\Eloquent\Model;

class CustomerDoc extends Model {

    public function customer() {
        return $this->hasOne('\App\Customer\Customer', 'id', 'customer_id');
    }
}
