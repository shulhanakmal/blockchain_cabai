<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelHasRole extends Model {

    public function roles() {
        return $this->hasOne(Role::class, 'id', 'role_id');
    }
}
