<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    //

    public function unit()
    {  
        return $this->belongsTo('App\Unit', 'unit');
    }
}
