<?php

namespace App\Flow;

use Illuminate\Database\Eloquent\Model;
use App\Flow\WorkOrder;
use Auth;

class Process extends Model {

    public static function findByName($name) {
        return static::query()->where('name', $name)->first();
    }

    public function createWorkOrder($title) {
        return WorkOrder::create(['process_id' => $this->id, 'user_id' => Auth::user()->id, 'title' => $title, 'updated_by' => Auth::user()->id]);
    }

    public function groups() {
        return $this->hasMany('App\Flow\Group');
    }

    public function states() {
        return $this->hasMany('App\Flow\State');
    }

    public function work_orders() {
        return $this->hasMany('App\Flow\WorkOrder');
    }
}
