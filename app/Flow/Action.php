<?php

namespace App\Flow;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Traits\HasRoles;
use App\Flow\ActionType;

class Action extends Model {
    use HasRoles;
    use SoftDeletes;

    protected $guard_name = 'web';

    public function process() {
        return $this->belongsTo('App\Flow\Process');
    }

    public function action_type() {
        return $this->belongsTo('App\Flow\ActionType');
    }

    public function action_targets() {
        return $this->hasOne('App\Flow\ActionTarget', 'action_id', 'id');
    }

    public function transitions() {
        return $this->belongsToMany('App\Flow\Transition');
    }

    public function targets() {
        return $this->belongsToMany('App\Flow\Target')->withPivot('group_id');
    }

    public function groups() {
        return $this->belongsToMany('App\Flow\Group', 'action_target')->withPivot('target_id');
    }

    public function scopeWithActionTypeName($query, $name) {
        $actionTypeIds = ActionType::name($name)->get()->pluck('id');
        return $query->whereIn('action_type_id', $actionTypeIds);
    }

    public function hasActionType($name) {
        if ($actionType = $this->action_type) {
            if ($actionType->name === $name) {
                return true;
            }
        }
        return false;
    }

    public function get_data_index($request) {
        return $this->when($request->keyword, function ($query) use ($request) {
            $query->where('name', 'like', "%{$request->keyword}%")
                ->orWhere('description', 'like', "%{$request->keyword}%");
        });
    }
}
