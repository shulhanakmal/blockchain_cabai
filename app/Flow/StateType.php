<?php

namespace App\Flow;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StateType extends Model {
    use SoftDeletes;

    public function states() {
        return $this->hasMany('App\Flow\State');
    }
}
