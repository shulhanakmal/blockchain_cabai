<?php

namespace App\Flow;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Target extends Model {
    use SoftDeletes;

    public function actions() {
        return $this->belongsToMany('App\Flow\Action');
    }

    public function activities() {
        return $this->belongsToMany('App\Flow\Activity');
    }

    public function scopeName($query, $name) {
        return $query->where('name', $name);
    }
}
