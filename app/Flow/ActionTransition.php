<?php

namespace App\Flow;

use Illuminate\Database\Eloquent\Model;

class ActionTransition extends Model {

    protected $table = 'action_transition';

    public function actions() {
        return $this->hasOne('App\Flow\Action', 'id', 'action_id');
    }
}
