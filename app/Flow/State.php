<?php

namespace App\Flow;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class State extends Model {
    use SoftDeletes;

    public function process() {
        return $this->belongsTo('App\Flow\Process');
    }

    public function state_type() {
        return $this->belongsTo('App\Flow\StateType');
    }

    public function transitions() {
        return $this->hasMany('App\Flow\Transition');
    }

    public function work_order() {
        return $this->hasMany('App\Flow\WorkOrder');
    }

    public function hasName($name) {
        if ($this->name === $name) {
            return true;
        }
        return false;
    }
}
