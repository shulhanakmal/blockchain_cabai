<?php

namespace App\Flow;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActionTarget extends Model {

    protected $table = 'action_target';

    public function actions() {
        return $this->belongsToMany('App\Flow\Action');
    }

    public function action() {
        return $this->hasOne('App\Flow\Action', 'id', 'action_id');
    }

    public function groups() {
        return $this->hasOne('App\Flow\Group', 'id', 'group_id');
    }

    public function transitions() {
        return $this->belongsToMany('App\Flow\Transition');
    }

    public function targets() {
        return $this->hasOne('App\Flow\Target', 'id', 'target_id');
    }
}
