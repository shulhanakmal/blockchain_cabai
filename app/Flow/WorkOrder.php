<?php

namespace App\Flow;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Flow\Transition;
use App\Flow\WorkOrderAction;
use App\Flow\Target;
use Auth;
use DateTimeInterface;
use DB;

class WorkOrder extends Model {
    use SoftDeletes;

    protected $fillable = [
        'process_id', 'user_id', 'state_id', 'title', 'updated_by'
    ];

    public static function create(array $attributes = []) {
        $attributes['state_id'] = 1;

        return static::query()->create($attributes);
    }

    public function process() {
        return $this->belongsTo('App\Flow\Process');
    }

    public function state() {
        return $this->belongsTo('App\Flow\State');
    }

    public function actions() {
        return $this->belongsToMany('App\Flow\Action', 'work_order_actions')->withPivot('is_active');
    }

    public function work_order_actions() {
        return $this->hasMany('App\Flow\WorkOrderAction', 'work_order_id', 'id');
    }

    public function submitAction($user, $actionType) {
        if ($action = $this->findActiveActionByActionType($user, $actionType)) {
            $workOrderAction = WorkOrderAction::where('work_order_id', $this->id)
                ->where('action_id', $action->id)
                ->where('is_active', 1)
                ->first();
            $workOrderAction->update(['is_active' => 0, 'is_complete' => 1]);
            $activeActionsForTransitionInWorkOrder =  WorkOrderAction::where('transition_id', $workOrderAction->transition_id)
                ->where('work_order_id', $this->id)
                ->where('is_active', 1)->get();
            if ($activeActionsForTransitionInWorkOrder->isEmpty()) {
                $this->disableAllActions();
                $this->moveToState($workOrderAction->transition_id);
                $this->addActiveActions();
            }
            return true;
        }
        return false;
    }

    public function disableAllActions() {
        $workOrderActions = WorkOrderAction::where('work_order_id', $this->id)->get();
        foreach ($workOrderActions as $workOrderAction) {
            if ($workOrderAction->is_active === 1) {
                $workOrderAction->is_active = 0;
                $workOrderAction->save();
            }
        }
    }

    public function addActiveActions() {
        $stateTransitions = Transition::where('state_id', $this->state_id)->get();
        foreach ($stateTransitions as $stateTransition) {
            $stateActions = $stateTransition->actions()->get();
            foreach ($stateActions as $stateAction) {
                WorkOrderAction::create([
                    'work_order_id' => $this->id,
                    'action_id' => $stateAction->id,
                    'transition_id' => $stateTransition->id,
                    'is_active' => 1,
                    'is_complete' => 0,
                ]);
            }
        }
    }

    public function moveToState($transitionId) {
        $transition = Transition::findOrFail($transitionId);
        $newState = $transition->next_state_id;
        $this->state_id = $newState;
        $this->updated_by = Auth::user()->id;
        $this->save();
    }

    public function findActiveActionByActionType($user, $actionType) {
        DB::setDefaultConnection('mysql');
        $targetIds = Target::name('group members')->get()->pluck('id');
        $activeActions = $this->getActiveActions();
        foreach ($activeActions as $activeAction) {
            if ($activeAction->hasActionType($actionType)) {
                $groups = $activeAction->groups()->whereIn('target_id', $targetIds)->get();
                foreach ($groups as $group) {
                    $roleNames = $group->roles->pluck('name')->all();
                    if ($user->hasAnyRole($roleNames)) {
                        return $activeAction;
                    }
                }
            }
        }
        return null;
    }

    public function getActiveActions() {
        return $this->actions()->where('is_active', 1)->get();
    }
}
