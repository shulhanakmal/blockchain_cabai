<?php

namespace App\Flow;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WorkOrderAction extends Model {
    use SoftDeletes;

    protected $fillable = ['work_order_id', 'action_id', 'transition_id', 'is_active', 'is_complete'];
}
