<?php

namespace App\Flow;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transition extends Model {
    use SoftDeletes;

    public function process() {
        return $this->belongsTo('App\Flow\Process');
    }

    public function state() {
        return $this->belongsTo('App\Flow\State');
    }

    public function states() {
        return $this->hasOne('App\Flow\State', 'id', 'state_id');
    }

    public function next_states() {
        return $this->hasOne('App\Flow\State', 'id', 'next_state_id');
    }

    public function action_transitions() {
        return $this->hasOne('App\Flow\ActionTransition', 'transition_id', 'id');
    }

    public function actions() {
        return $this->belongsToMany('App\Flow\Action');
    }

    public function activities() {
        return $this->hasMany('App\Flow\Activity');
    }
}
