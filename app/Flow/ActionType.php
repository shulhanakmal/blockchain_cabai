<?php

namespace App\Flow;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActionType extends Model
{
    use SoftDeletes;

    public function actions()
    {
        return $this->hasMany('App\Flow\Action');
    }

    public function scopeName($query, $name)
    {
        return $query->where('name', $name);
    }

    public function scopeActionsWithTypeName($query, $name)
    {
        return $query->where('name', $name);
    }
}
