<?php

namespace App\Flow;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Traits\HasRoles;
Use App\Role;

class Group extends Model {
    use SoftDeletes;
    use HasRoles;

    protected $guard_name = 'web';

    public function process() {
        return $this->belongsTo('App\Flow\Process');
    }

    public function customers() {
        return $this->belongsToMany('App\Customer\Customer');
    }

    public function vendors() {
        return $this->belongsToMany('App\Vendor\VendorMaster');
    }

    public function admins() {
        return $this->belongsToMany('App\Admin\AdminUser');
    }

    public function model_has_roles() {
        return $this->hasMany('App\ModelHasRole', 'model_id', 'id')->where('model_type', 'App\\Group');
    }

    public function saves($request) {
        $this->process_id = $request->process;
        $this->name       = $request->name;
        $this->save();
        if($request->has('roles')) {
            $this->assignRole($request->roles);
        }
        return true;
    }

    public function updates($request) {
        $this->process_id = $request->process;
        $this->name       = $request->name;
        if($request->has('roles')) {
            $this->assignRole($request->roles);
        }
        $this->save();
        return true;
    }
}
