<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoasterSupplier extends Model
{
    //
    public function unit()
    {  
        return $this->belongsTo('App\RoasterUnit', 'unit');
    }
}
