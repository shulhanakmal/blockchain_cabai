<?php

namespace App\Currencies;

use Illuminate\Database\Eloquent\Model;

class currencies extends Model {

    protected $table = "m_currencies";
    protected $primaryKey = 'id';
}
