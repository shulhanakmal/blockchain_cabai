<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionExplorer extends Model
{
    protected $table = 'transaction_explorer';
}
