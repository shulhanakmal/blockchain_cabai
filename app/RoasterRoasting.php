<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoasterRoasting extends Model
{
    //

    public function product()
    {  
        return $this->belongsTo('App\RoasterProduct', 'bean_id');
    }

    public function roastingProfile()
    {  
        return $this->belongsTo('App\RoasterRoastingProfile', 'roasting_profile_id');
    }
}
