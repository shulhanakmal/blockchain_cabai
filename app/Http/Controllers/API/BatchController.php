<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Group\group;
use App\Batch;
use App\Biji;
use App\CertificateBatch;
use App\ImageBatch;
use App\Jenis;
use App\Proses;
use App\Supplier;
use DB;

class BatchController extends Controller
{
    public $successStatus = 401;

    function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function indexGuest($code)
    {
        DB::setDefaultConnection(group::where('code', $code)->first()->katalog);
        $this->successStatus = 200;
        $success['success'] = true;
        $success['batch']   = Batch::with('jenis','proses', 'supplier', 'biji')->orderBy('created_at', 'desc')->get();

        return response()->json($success, $this->successStatus);
    }

    public function index()
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        $this->successStatus = 200;
        $success['success'] = true;
        // $success['batch']   = Batch::orderBy('created_at', 'desc')->get();

        $success['batch']   = Batch::with('jenis','proses', 'supplier', 'biji')->orderBy('created_at', 'desc')->get();

        return response()->json($success, $this->successStatus);
    }

    public function detail(Request $request, $id)
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        $this->successStatus = 200;
        $success['success'] = true;
        // $success['batch']   = Batch::find($id);
        $success['batch']   = Batch::where('id', $id)->with('jenis','proses', 'supplier', 'biji')->get();
        $success['certificateBatch']   = CertificateBatch::where('batchID', $id)->get();
        $success['imageBatch']   = ImageBatch::where('batchID', $id)->get();

        return response()->json($success, $this->successStatus);
    }

    public function detailGuest(Request $request, $code, $id)
    {
        DB::setDefaultConnection(group::where('code', $code)->first()->katalog);
        $this->successStatus = 200;
        $success['success'] = true;
        $success['batch']   = Batch::where('id', $id)->with('jenis','proses', 'supplier', 'biji')->get();
        $success['certificateBatch']   = CertificateBatch::where('batchID', $id)->get();
        $success['imageBatch']   = ImageBatch::where('batchID', $id)->get();

        return response()->json($success, $this->successStatus);
    }

    public function detailByBatchID(Request $request, $id)
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        $this->successStatus = 200;
        $success['success'] = true;
        $success['batch']   = Batch::where('batch_id',$id)->first();

        return response()->json($success, $this->successStatus);
    }

    public function detailByBatchIDGuest(Request $request, $code, $id)
    {
        DB::setDefaultConnection(group::where('code', $code)->first()->katalog);
        $this->successStatus = 200;
        $success['success'] = true;
        $batch = Batch::where('batch_id',$id)->with('jenis','proses', 'supplier', 'biji')->first();
        $success['batch']   = $batch;
        $success['certificateBatch']   = CertificateBatch::where('batchID', $batch->id)->get();
        $success['imageBatch']   = ImageBatch::where('batchID', $batch->id)->get();

        return response()->json($success, $this->successStatus);
    }

    public function add_detail(Request $request)
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        $this->successStatus = 200;
        $success['success']  = true;
        $success['biji']     = Biji::all();
        $success['jenis']    = Jenis::all();
        $success['proses']   = Proses::all();
        $success['supplier'] = Supplier::all();

        return response()->json($success, $this->successStatus);
    }

    public function add_detailGuest(Request $request, $code)
    {
        DB::setDefaultConnection(group::where('code', $code)->first()->katalog);
        $this->successStatus = 200;
        $success['success']  = true;
        $success['biji']     = Biji::all();
        $success['jenis']    = Jenis::all();
        $success['proses']   = Proses::all();
        $success['supplier'] = Supplier::all();

        return response()->json($success, $this->successStatus);
    }

    public function delete(Request $request, $id)
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        $batch = Batch::find($id);
        $batch->delete();
        ImageBatch::where('batchID', $id)->delete();
        CertificateBatch::where('batchID', $id)->delete();
        $this->successStatus = 200;
        $success['success'] = true;
        return response()->json($success, $this->successStatus);
    }

    public function add(Request $request)
    {
        $raw = array(
            'batch_id'    => $request->batch_id,
            'jenis_id'    => $request->jenis_id,
            'proses_id'   => $request->proses_id,
            'supplier_id' => $request->supplier_id,
            'biji_id'     => $request->biji_id,
            'volume'      => $request->volume,
            'gambar'      => $request->gambar,
            'tgl_panen'   => $request->tgl_panen
        );
        $batch_id = hash('sha256', json_encode($raw));
        $batch              = new Batch();
        $batch              = $batch->setConnection($this->user->group->katalog);
        // $batch->batch       = $batch_id;
        $batch->batch_id    = $request->batch_id;
        $batch->jenis_id    = $request->jenis_id;
        $batch->proses_id   = $request->proses_id;
        $batch->supplier_id = $request->supplier_id;
        $batch->biji_id     = $request->biji_id;
        $batch->volume      = $request->volume;

        $batch->tgl_panen   = date('Y-m-d', strtotime($request->tgl_panen));
        $batch->save();

        if ($request->hasFile('files')) {
            $files          = $request->file('files');

            foreach($files as $file)
            {
                $name=$file->getClientOriginalName();
                $file->move(public_path("images/batch/") . $batch->id . '/', $name);

                $image              = new ImageBatch();
                $image              = $image->setConnection($this->user->group->katalog);

                $image->images  = "images/batch/" . $batch->id . '/' . $name;
                $image->batchID  = $batch->id;

                $image->save();
            }
        }

        if ($request->hasFile('certificate')) {
            $certificates          = $request->file('certificate');

            foreach($certificates as $certificate)
            {
                $name=$certificate->getClientOriginalName();
                $certificate->move(public_path("certificates/batch/") . $batch->id . '/', $name);

                $certificateImage              = new CertificateBatch();
                $certificateImage              = $certificateImage->setConnection($this->user->group->katalog);

                $certificateImage->certificates  = "certificates/batch/" . $batch->id . '/' . $name;
                $certificateImage->batchID       = $batch->id;

                $certificateImage->save();
            }
        }

        $this->successStatus = 200;
        $success['success']  = true;
        $success['data']     = $batch;

        return response()->json($request, $this->successStatus);
    }

    public function edit(Request $request, $id)
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        if (Batch::where('id', $id)->exists()) {
            $batch = Batch::find($id);
            if ($request->has('biji_id')) {
                $batch->biji_id      = $request->biji_id;
            }
            if ($request->has('jenis_id')) {
                $batch->jenis_id = $request->jenis_id;
            }
            if ($request->has('proses_id')) {
                $batch->proses_id = $request->proses_id;
            }
            if ($request->has('supplier_id')) {
                $batch->supplier_id = $request->supplier_id;
            }
            if ($request->hasFile('files')) {
                ImageBatch::where('batchID', $id)
                            ->delete();

                $files          = $request->file('files');

                foreach($files as $file)
                {
                    $name=$file->getClientOriginalName();
                    $file->move(public_path("images/batch/") . $batch->id . '/', $name);

                    $image              = new ImageBatch();
                    $image              = $image->setConnection($this->user->group->katalog);

                    $image->images  = "images/batch/" . $batch->id . '/' . $name;
                    $image->batchID  = $batch->id;

                    $image->save();
                }
            }
            if ($request->hasFile('certificate')) {
                CertificateBatch::where('batchID', $id)->delete();
                
                $certificates          = $request->file('certificate');
                
                foreach($certificates as $certificate)
                {
                    $name=$certificate->getClientOriginalName();
                    $certificate->move(public_path("certificates/batch/") . $batch->id . '/', $name);
                
                    $certificateImage              = new CertificateBatch();
                    $certificateImage              = $certificateImage->setConnection($this->user->group->katalog);
                
                    $certificateImage->certificates  = "certificates/batch/" . $batch->id . '/' . $name;
                    $certificateImage->batchID       = $batch->id;
                
                    $certificateImage->save();
                }
            }
            if ($request->has('tgl_panen')) {
                $batch->tgl_panen   = date('Y-m-d', strtotime($request->tgl_panen));
            }
            if ($request->has('volume')) {
                $batch->volume = $request->volume;
            }
            $batch->save();

            return response()->json([
                "message" => "records updated successfully"
            ], 200);
        } else {
            return response()->json([
                "message" => "Product not found"
            ], 404);
        }
    }
}
