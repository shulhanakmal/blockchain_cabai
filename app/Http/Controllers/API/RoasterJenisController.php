<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\RoasterJenis;
use DB;

class RoasterJenisController extends Controller
{
    public $successStatus = 401;

    function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function detail(Request $request, $id)
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        $this->successStatus = 200;
        $success['success'] = true;
        $success['jenis']   = RoasterJenis::find($id);

        return response()->json($success, $this->successStatus);
    }

    public function delete(Request $request, $id)
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        $jenis = RoasterJenis::find($id);
        $jenis->delete();
        $this->successStatus = 200;
        $success['success'] = true;
        return response()->json($success, $this->successStatus);
    }

    public function add(Request $request)
    {
        $jenis = new RoasterJenis();
        $jenis = $jenis->setConnection($this->user->group->katalog);
        $jenis->nama_jenis      = $request->nama_jenis;
        $jenis->deskripsi_jenis = $request->deskripsi_jenis;
        $jenis->save();

        $this->successStatus = 200;
        $success['success']  = true;
        $success['data']     = $jenis;

        return response()->json($success, $this->successStatus);
    }

    public function edit(Request $request, $id)
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        if (RoasterJenis::where('id', $id)->exists()) {
            $jenis = RoasterJenis::find($id);
            if ($request->has('nama_jenis')) {
                $jenis->nama_jenis      = $request->nama_jenis;
            }
            if ($request->has('deskripsi_jenis')) {
                $jenis->deskripsi_jenis = $request->deskripsi_jenis;
            }
            $jenis->save();

            return response()->json([
                "message" => "records updated successfully"
            ], 200);
        } else {
            return response()->json([
                "message" => "Product not found"
            ], 404);
        }
    }
}
