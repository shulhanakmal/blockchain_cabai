<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\RoasterRoastingProfile;
use Auth;
use DB;

class RoastingProfileController extends Controller
{
    public $successStatus = 401;

    function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function delete(Request $request, $id)
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        $roastingProfile = RoasterRoastingProfile::find($id);
        $roastingProfile->delete();
        $this->successStatus = 200;
        $success['success'] = true;
        return response()->json($success, $this->successStatus);
    }

    public function add(Request $request)
    {
        $roastingProfile = new RoasterRoastingProfile();
        $roastingProfile = $roastingProfile->setConnection($this->user->group->katalog);
        $roastingProfile->roasting_profile      = $request->roasting_profile;
        $roastingProfile->save();

        $this->successStatus = 200;
        $success['success']  = true;
        $success['data']     = $roastingProfile;

        return response()->json($success, $this->successStatus);
    }

    public function detail(Request $request, $id)
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        $this->successStatus = 200;
        $success['success'] = true;
        $success['roasting_profile']   = RoasterRoastingProfile::find($id);

        return response()->json($success, $this->successStatus);
    }

    public function edit(Request $request, $id)
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        if (RoasterRoastingProfile::where('id', $id)->exists()) {
            $roastingProfile = RoasterRoastingProfile::find($id);
            if ($request->has('roasting_profile')) {
                $roastingProfile->roasting_profile      = $request->roasting_profile;
            }
            $roastingProfile->save();

            return response()->json([
                "message" => "records updated successfully"
            ], 200);
        } else {
            return response()->json([
                "message" => "Product not found"
            ], 404);
        }
    }
}
