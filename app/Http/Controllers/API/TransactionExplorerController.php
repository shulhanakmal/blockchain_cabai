<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\TransactionExplorer;
use Illuminate\Http\Request;
use Auth;
use DB;

class TransactionExplorerController extends Controller
{
    public $successStatus = 401;

    function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function index()
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        $this->successStatus         = 200;
        $success['success']          = true;
        $success['transaction_explorer'] = TransactionExplorer::orderBy('created_at', 'desc')->get();

        return response()->json($success, $this->successStatus);
    }

    public function add(Request $request)
    {
        $transactionExplorer = new TransactionExplorer();
        $transactionExplorer = $transactionExplorer->setConnection($this->user->group->katalog);
        $transactionExplorer->product_id      = $request->product_id;
        $transactionExplorer->t_hash = $request->t_hash;
        $transactionExplorer->save();

        $this->successStatus = 200;
        $success['success']  = true;
        $success['data']     = $transactionExplorer;

        return response()->json($success, $this->successStatus);
    }
}
