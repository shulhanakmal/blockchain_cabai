<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Vendor\VendorMaster;
use App\Pabrik\PabrikUser;
use App\Customer\Customer;
use Auth;
use DB;

class AuthController extends Controller {

    public $successStatus = 401;

    public function login(Request $request) {
        $success['success']     = false;
        $success['token']       = null;
        $success['user_detail'] = null;

        if (Auth::guard('vendor')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {
            $this->successStatus = 200;
            $user                   = VendorMaster::where('email', request('email'))->first();
            $success['success']     = true;
            $success['token']       = $user->createToken('MyApp')->accessToken;
            $success['logo']        = $user->group->style->logo;
            $success['user_detail'] = $user;
        } else {
            $success['success']     = false;
            $success['token']       = null;
            $success['user_detail'] = null;
            $success['logo']        = null;
            $success['message']     = 'User tidak ditemukan';
        }
        return response()->json($success, $this->successStatus);
    }

    public function login_roaster(Request $request) {
        $success['success']     = false;
        $success['token']       = null;
        $success['user_detail'] = null;

        if (Auth::guard('customer')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {
            $this->successStatus = 200;
            $user                   = Customer::where('email', request('email'))->first();
            if($user->hasAnyRole(['Roaster'])) {
                $success['success']     = true;
                $success['token']       = $user->createToken('MyApp')->accessToken;
                $success['logo']        = $user->group->style->logo;
                $success['user_detail'] = $user;
            } else {
                $success['success']     = false;
                $success['token']       = null;
                $success['user_detail'] = null;
                $success['logo']        = null;
                $success['message']     = 'Hanya roaster yang bisa login';
            }
        } else {
            $success['success']     = false;
            $success['token']       = null;
            $success['user_detail'] = null;
            $success['logo']        = null;
            $success['message']     = 'User tidak ditemukan';
        }
        return response()->json($success, $this->successStatus);
    }

    public function loginPabrik(Request $request) {
        $success['success']     = false;
        $success['token']       = null;
        $success['user_detail'] = null;

        DB::setDefaultConnection('mysql');
        if (Auth::guard('pabrik')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {
            $this->successStatus = 200;
            $user                   = PabrikUser::where('email', request('email'))->where('status', 1)->first();
            $success['success']     = true;
            $success['token']       = $user->createToken('MyApp')->accessToken;
            $success['user_detail'] = $user;
            $success['user_role']   = $user->getRole;
        } else {
            $success['success']     = false;
            $success['token']       = null;
            $success['user_detail'] = null;
            $success['user_role']   = null;
            $success['message']     = 'User tidak ditemukan';
        }
        return response()->json($success, $this->successStatus);
    }
}
