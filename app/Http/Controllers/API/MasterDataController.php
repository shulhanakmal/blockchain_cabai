<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Group\group;
use App\RoasterJenis;
use App\RoasterBiji;
use App\RoasterProses;
use App\RoasterRoastingProfile;
use App\RoasterSupplier;
use App\RoasterUnit;
use App\RoasterWholeBean;
use Auth;
use DB;

class MasterDataController extends Controller
{
    public $successStatus = 401;

    function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function index()
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        $this->successStatus         = 200;
        $success['success']          = true;
        $success['roasting_profile'] = RoasterRoastingProfile::all();
        $success['whole_bean']       = RoasterWholeBean::all();
        $success['unit']             = RoasterUnit::all();
        $success['biji']             = RoasterBiji::all();
        $success['jenis']            = RoasterJenis::all();
        $success['proses']           = RoasterProses::all();
        $success['supplier']         = RoasterSupplier::all();

        return response()->json($success, $this->successStatus);
    }

    public function add_detailGuest(Request $request, $code)
    {
        DB::setDefaultConnection(group::where('code', $code)->first()->katalog);
        $this->successStatus = 200;
        $success['success']  = true;
        $success['roasting_profile'] = RoasterRoastingProfile::all();
        $success['whole_bean']       = RoasterWholeBean::all();
        $success['unit']             = RoasterUnit::all();
        $success['biji']             = RoasterBiji::all();
        $success['jenis']            = RoasterJenis::all();
        $success['proses']           = RoasterProses::all();
        $success['supplier']         = RoasterSupplier::all();

        return response()->json($success, $this->successStatus);
    }
}
