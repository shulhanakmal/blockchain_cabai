<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Group\group;
use App\RoasterBlendedBean;
use App\RoasterCertificateRoasting;
use App\RoasterInventory;
use App\RoasterRoasting;
use Auth;
use DB;

class RoastingController extends Controller
{
    public $successStatus = 401;

    function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function index()
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        $this->successStatus = 200;
        $success['success'] = true;
        $success['roasting']   = RoasterRoasting::with('product','roastingProfile')->get();
        $success['inventory']   = RoasterInventory::with('product')->get();

        return response()->json($success, $this->successStatus);
    }

    public function detail(Request $request, $id)
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        $this->successStatus = 200;
        $success['success'] = true;
        $success['roasting']   = RoasterRoasting::where('id', $id)->with('product','roastingProfile')->get();
        $success['blendedBeans']   = RoasterBlendedBean::where('roasting_id', $id)->with('product')->get();
        $success['certificateRoasting']   = RoasterCertificateRoasting::where('roastingID', $id)->get();

        return response()->json($success, $this->successStatus);
    }

    public function detailGuest(Request $request,$code, $id)
    {
        DB::setDefaultConnection(group::where('code', $code)->first()->katalog);
        $this->successStatus = 200;
        $success['success'] = true;
        $success['roasting']   = RoasterRoasting::where('id', $id)->with('product.biji','product.jenis','product.proses','product.supplier.unit', 'product.greenBeans.batch.jenis','product.greenBeans.batch.proses', 'product.greenBeans.batch.supplier.unit', 'product.greenBeans.batch.biji', 'roastingProfile')->get();
        $success['blendedBeans']   = RoasterBlendedBean::where('roasting_id', $id)->with('product','product.biji','product.jenis','product.proses','product.supplier.unit','product.greenBeans.batch.jenis','product.greenBeans.batch.proses', 'product.greenBeans.batch.supplier.unit', 'product.greenBeans.batch.biji')->get();
        $success['certificateRoasting']   = RoasterCertificateRoasting::where('roastingID', $id)->get();

        return response()->json($success, $this->successStatus);
    }

    public function delete(Request $request, $id)
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        $roasting = RoasterRoasting::find($id);
        $roasting->delete();
        RoasterBlendedBean::where('roasting_id', $id)->delete();
        RoasterCertificateRoasting::where('roastingID', $id)->delete();
        $this->successStatus = 200;
        $success['success'] = true;
        return response()->json($success, $this->successStatus);
    }

    public function update_qr(Request $request, $id)
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        if (RoasterRoasting::where('id', $id)->exists()) {
            $roasting = RoasterRoasting::find($id);

            if ($request->hasFile('files_qr')) {
                $files          = $request->file('files_qr');
                $files->move(public_path("images/roasting/") . $this->user->group->katalog . '/' . $this->user->id . '/image/' . $roasting->id, $request->fileName_qr);
                $roasting->qrcode  = "images/roasting/" . $this->user->group->katalog . '/' . $this->user->id . '/image/' . $roasting->id . '/' . $request->fileName_qr;
                $roasting->save();
            }

            return response()->json([
                "message" => "records updated successfully"
            ], 200);
        } else {
            return response()->json([
                "message" => "Product not found"
            ], 404);
        }
    }

    public function add(Request $request)
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);

        $roasting = new RoasterRoasting();
        $roasting = $roasting->setConnection($this->user->group->katalog);

        if ($request->roastingMenu == "Single") {
            $roasting->roasting_name            = $request->roasting_name;
            $roasting->roasting_menu            = $request->roastingMenu;
            $roasting->roasting_profile_id      = $request->roasting_profile_id;
            $roasting->bean_id                  = $request->bean_id;
            $roasting->flavor_note              = $request->flavor_note;
            $roasting->acidity                  = $request->acidity;
            $roasting->intensity                = $request->intensity;
            $roasting->tgl_roasting             = $request->tgl_roasting;
            $roasting->save();

            $success['roasting'] = $roasting;

            if(RoasterInventory::where('bean_id', $request->bean_id)->first())
            {
                $inventory = RoasterInventory::where('bean_id', $request->bean_id)->first();
                $inventory->weight              = $inventory->weight - $request->weight;
                $inventory->save();
            }
            else
            {
                $inventory = new RoasterInventory();
                $inventory = $inventory->setConnection($this->user->group->katalog);
                $inventory->bean_id                  = $request->bean_id;
                $inventory->weight                   = $request->weight;
                $inventory->unit                     = $request->unit;
                $inventory->save();
            }
        }

        else if ($request->roastingMenu == "Blended") {

            if ($request->has('beans')) {
                $products = json_decode($request->beans);

                $roasting->roasting_name            = $request->roasting_name;
                $roasting->roasting_menu            = $request->roastingMenu;
                $roasting->roasting_profile_id      = $request->roasting_profile_id;
                $roasting->flavor_note              = $request->flavor_note;
                $roasting->acidity                  = $request->acidity;
                $roasting->intensity                = $request->intensity;
                $roasting->tgl_roasting             = $request->tgl_roasting;
                $roasting->save();

                $success['roasting'] = $roasting;

                foreach ($products as $product)
                {
                    $blendedBeans = new RoasterBlendedBean();
                    $blendedBeans-> bean_id         = $product->selectBean;
                    $blendedBeans-> roasting_id     = $roasting->id;
                    $blendedBeans-> persentase      = $product->persentase;
                    $blendedBeans->save();

                    if(RoasterInventory::where('bean_id', $product->selectBean)->first())
                    {
                        $inventory = RoasterInventory::where('bean_id', $product->selectBean)->first();
                        $inventory->weight                   = $inventory->weight - ($request->weight * $product->persentase / 100);
                        $inventory->save();
                    }
                    else
                    {
                        $inventory = new RoasterInventory();
                        $inventory = $inventory->setConnection($this->user->group->katalog);
                        $inventory->bean_id                  = $product->selectBean;
                        $inventory->weight                   = $request->weight * $product->persentase / 100;
                        $inventory->unit                     = $request->unit;
                        $inventory->save();
                    }
                }
            }
        }

        if ($request->hasFile('certificate')) {
            $certificates          = $request->file('certificate');

            foreach($certificates as $certificate)
            {
                $name=$certificate->getClientOriginalName();
                $certificate->move(public_path("certificates/roasting/") . $roasting->id . '/', $name);

                $certificateImage              = new RoasterCertificateRoasting();
                $certificateImage              = $certificateImage->setConnection($this->user->group->katalog);

                $certificateImage->certificates     = "certificates/roasting/" . $roasting->id . '/' . $name;
                $certificateImage->roastingID       = $roasting->id;

                $certificateImage->save();
            }
        }

        if ($request->hasFile('files_gambar')) {
            $files          = $request->file('files_gambar');
            $files->move(public_path("images/roasting/") . $this->user->group->katalog . '/' . $this->user->id . '/image/' . $roasting->id, $request->fileName_gambar);
            $roasting->thumbnail  = "images/roasting/" . $this->user->group->katalog . '/' . $this->user->id . '/image/' . $roasting->id . '/' . $request->fileName_gambar;
            $roasting->save();
        }

        $this->successStatus = 200;
        $success['success']  = true;
        $success['roasting'] = $roasting;

        return response()->json($success, $this->successStatus);
    }

    public function edit(Request $request, $id)
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        if (RoasterRoasting::where('id', $id)->exists()) {
            $roasting = RoasterRoasting::find($id);
            if ($request->has('roasting_name')) {
                $roasting->roasting_name      = $request->roasting_name;
            }
            if ($request->has('roasting_profile_id')) {
                $roasting->roasting_profile_id      = $request->roasting_profile_id;
            }
            if ($request->has('flavor_note')) {
                $roasting->flavor_note = $request->flavor_note;
            }
            if ($request->has('acidity')) {
                $roasting->acidity                  = $request->acidity;
            }
            if ($request->has('intensity')) {
                $roasting->intensity                = $request->intensity;
            } 
            if ($request->has('tgl_roasting')) {
                $roasting->tgl_roasting             = $request->tgl_roasting;
            } 
            if ($request->has('bean_id')) {
                $roasting->bean_id = $request->bean_id;
            }
            if ($request->has('beans')) {

                $products = json_decode($request->beans);
                $blendedBeans = RoasterBlendedBean::where('roasting_id', $id)->get();

                foreach ($products as $product)
                {
                    foreach ($blendedBeans as $blendedBean)
                    {
                        $blendedBean-> bean_id         = $product->selectBean;
                        $blendedBean-> roasting_id     = $roasting->id;
                        $blendedBean-> persentase      = $product->persentase;
                        $blendedBean->save();
                    }
                }
            }
            $roasting->save();

            if ($request->hasFile('certificate')) {
                RoasterCertificateRoasting::where('roastingID', $id)->delete();
                
                $certificates          = $request->file('certificate');
                
                foreach($certificates as $certificate)
                {
                    $name=$certificate->getClientOriginalName();
                    $certificate->move(public_path("certificates/roasting/") . $roasting->id . '/', $name);
                
                    $certificateImage              = new RoasterCertificateRoasting();
                    $certificateImage              = $certificateImage->setConnection($this->user->group->katalog);
                
                    $certificateImage->certificates  = "certificates/roasting/" . $roasting->id . '/' . $name;
                    $certificateImage->roastingID       = $roasting->id;
                
                    $certificateImage->save();
                }
            }

            if ($request->hasFile('files_gambar')) {
                $files          = $request->file('files_gambar');
                $files->move(public_path("images/roasting/") . $this->user->group->katalog . '/' . $this->user->id . '/image/' . $roasting->id, $request->fileName_gambar);
                $roasting->thumbnail  = "images/roasting/" . $this->user->group->katalog . '/' . $this->user->id . '/image/' . $roasting->id . '/' . $request->fileName_gambar;
                $roasting->save();
            }

            return response()->json([
                "message" => "records updated successfully"
            ], 200);
        } else {
            return response()->json([
                "message" => "Product not found"
            ], 404);
        }
    }
}
