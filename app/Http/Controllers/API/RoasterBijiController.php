<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\RoasterBiji;
use DB;

class RoasterBijiController extends Controller
{
    public $successStatus = 401;

    function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function delete(Request $request, $id)
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        $biji = RoasterBiji::find($id);
        $biji->delete();
        $this->successStatus = 200;
        $success['success'] = true;
        return response()->json($success, $this->successStatus);
    }

    public function add(Request $request)
    {
        $bijis = new RoasterBiji();
        $bijis = $bijis->setConnection($this->user->group->katalog);
        $bijis->nama_biji      = $request->nama_biji;
        $bijis->deskripsi_biji = $request->deskripsi_biji;
        $bijis->save();

        $this->successStatus = 200;
        $success['success']  = true;
        $success['data']     = $bijis;

        return response()->json($success, $this->successStatus);
    }

    public function detail(Request $request, $id)
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        $this->successStatus = 200;
        $success['success'] = true;
        $success['biji']   = RoasterBiji::find($id);

        return response()->json($success, $this->successStatus);
    }

    public function edit(Request $request, $id)
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        if (RoasterBiji::where('id', $id)->exists()) {
            $bijis = RoasterBiji::find($id);
            if ($request->has('nama_biji')) {
                $bijis->nama_biji      = $request->nama_biji;
            }
            if ($request->has('deskripsi_biji')) {
                $bijis->deskripsi_biji = $request->deskripsi_biji;
            }
            $bijis->save();

            return response()->json([
                "message" => "records updated successfully"
            ], 200);
        } else {
            return response()->json([
                "message" => "Product not found"
            ], 404);
        }
    }
}
