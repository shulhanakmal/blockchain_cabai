<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\RoasterWeight;
use Auth;
use DB;

class WeightController extends Controller
{
    public $successStatus = 401;

    function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function delete(Request $request, $id)
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        $weight = RoasterWeight::find($id);
        $weight->delete();
        $this->successStatus = 200;
        $success['success'] = true;
        return response()->json($success, $this->successStatus);
    }

    public function add(Request $request)
    {
        $weight = new RoasterWeight();
        $weight = $weight->setConnection($this->user->group->katalog);
        $weight->weight      = $request->weight;
        $weight->save();

        $this->successStatus = 200;
        $success['success']  = true;
        $success['data']     = $weight;

        return response()->json($success, $this->successStatus);
    }
}
