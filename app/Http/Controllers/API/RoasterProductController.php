<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Group\group;
use App\RoasterProduct;
use App\Product\ProductGroupRule;
use App\Vendor\VendorContractMaster;
use App\Category\Category;
use App\Product\Product;
use DB;
use App\RoasterBiji;
use App\RoasterInventory;
use App\RoasterJenis;
use App\RoasterProses;
use App\RoasterSupplier;

class RoasterProductController extends Controller
{
    public $successStatus = 401;

    function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function index()
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        $this->successStatus = 200;
        $success['success'] = true;
        $success['product']   = RoasterProduct::with('jenis','proses', 'supplier', 'biji')->get();

        return response()->json($success, $this->successStatus);
    }

    public function detail(Request $request, $id)
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        $this->successStatus = 200;
        $success['success'] = true;
        $success['product']   = RoasterProduct::where('id', $id)->with('jenis','proses', 'supplier', 'biji')->get();

        return response()->json($success, $this->successStatus);
    }

    public function detailGuest(Request $request,$code, $id)
    {
        DB::setDefaultConnection(group::where('code', $code)->first()->katalog);
        $this->successStatus = 200;
        $success['success'] = true;
        $success['product']   = RoasterProduct::where('id', $id)->with('jenis','proses', 'supplier.unit', 'biji')->get();

        return response()->json($success, $this->successStatus);
    }

    public function update_qr(Request $request, $id)
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        if (RoasterProduct::where('id', $id)->exists()) {
            $product = RoasterProduct::find($id);

            if ($request->hasFile('files_qr')) {
                $files          = $request->file('files_qr');
                $files->move(public_path("images/product/") . $this->user->group->katalog . '/' . $this->user->id . '/image/' . $product->id, $request->fileName_qr);
                $product->qrcode  = "images/product/" . $this->user->group->katalog . '/' . $this->user->id . '/image/' . $product->id . '/' . $request->fileName_qr;
                $product->save();
            }

            return response()->json([
                "message" => "records updated successfully"
            ], 200);
        } else {
            return response()->json([
                "message" => "Product not found"
            ], 404);
        }
    }

    public function delete(Request $request, $id)
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        $product = RoasterProduct::find($id);
        $product->delete();
        RoasterInventory::where('bean_id', $id)->delete();
        $this->successStatus = 200;
        $success['success'] = true;
        return response()->json($success, $this->successStatus);
    }

    public function add_detail(Request $request)
    {
        DB::setDefaultConnection('mysql');
        $cat = VendorContractMaster::select('category')->where('vendor_master_id', Auth::user()->id)->get()->pluck('category');

        $category = new Category;
        $category = $category->setConnection($this->user->group->katalog);
        $success['category'] = $category->whereIn('id', $cat)->get();

        $product_group_rules = new ProductGroupRule;
        $product_group_rules = $product_group_rules->setConnection($this->user->group->katalog);
        $success['product_group_rules'] = $product_group_rules->get();

        DB::setDefaultConnection($this->user->group()->first()->katalog);
        $success['batch']   = Batch::orderBy('created_at', 'desc')->get();

        // $batch = new Batch;
        // $batch = $batch->setConnection($this->user->group->katalog);
        // $success['batch'] = $batch->get();

        $this->successStatus = 200;
        $success['success'] = true;

        return response()->json($success, $this->successStatus);
    }

    public function add_detail_roaster(Request $request)
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        $this->successStatus = 200;
        $success['success']  = true;
        $success['biji']     = RoasterBiji::all();
        $success['jenis']    = RoasterJenis::all();
        $success['proses']   = RoasterProses::all();
        $success['supplier'] = RoasterSupplier::all();

        return response()->json($success, $this->successStatus);
    }

    public function sub_category(Request $request, $id)
    {
        $category = new Category;
        $category = $category->setConnection($this->user->group->katalog);
        $success['subcategory'] = $category->where('level', 3)->where('parent_id', $id)->get();

        $this->successStatus = 200;
        $success['success'] = true;

        return response()->json($success, $this->successStatus);
    }

    public function add(Request $request)
    {
        $product = new RoasterProduct;
        $product = $product->setConnection($this->user->group->katalog);
        $product                        = new RoasterProduct;
        $product                        = $product->setConnection($this->user->group->katalog);

        if ($request->has('productID')) {
            $product->productID             = $request->productID;
            $batch = new Product();
            $batch = $batch->setConnection($this->user->group->katalog);
            $batch = $batch->find($request->productID);
            $product->name                  = $batch->name;
        } else{
            $product->name                  = $request->name;
            $product->biji_id               = $request->biji_id;
            $product->jenis_id              = $request->jenis_id;
            $product->proses_id             = $request->proses_id;
            $product->supplier_id           = $request->supplier_id;
        }
        
        $product->save();

        $inventory = new RoasterInventory();
        $inventory = $inventory->setConnection($this->user->group->katalog);
        $inventory->bean_id                  = $product->id;

        if ($request->has('productID')) {
            $batch = new Product();
            $batch = $batch->setConnection($this->user->group->katalog);
            $batch = $batch->find($request->productID);
            $inventory->weight                   = $batch->weight;
            $inventory->unit                     = "Kg";
        } else{
            $inventory->weight                   = $request->weight;
            $inventory->unit                     = "Kg";
        }

        $inventory->save();

        $this->successStatus = 200;
        $success['success']  = true;
        $success['product']     = $product;
        if ($request->has('productID')) {
            $product->productID             = $request->productID;
            $batch = new Product();
            $batch = $batch->setConnection($this->user->group->katalog);
            $success['batch']    = $batch->find($request->productID);
        }
        

        return response()->json($success, $this->successStatus);
    }

    public function edit(Request $request, $id)
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        if (RoasterProduct::where('id', $id)->exists()) {
            $product = RoasterProduct::find($id);
            if ($request->has('productID')) {
                $product->productID             = $request->productID;
                $batch = new Product();
                $batch = $batch->setConnection($this->user->group->katalog);
                $batch = $batch->find($request->productID);
                $product->name                  = $batch->name;
                $product->biji_id               = NULL;
                $product->jenis_id              = NULL;
                $product->proses_id             = NULL;
                $product->supplier_id           = NULL;
            } else {
                $product->productID = NULL;
                if ($request->has('name')) {
                    $product->name = $request->name;
                }
                if ($request->has('biji_id')) {
                    $product->biji_id = $request->biji_id;
                }
                if ($request->has('jenis_id')) {
                    $product->jenis_id = $request->jenis_id;
                }
                if ($request->has('proses_id')) {
                    $product->proses_id = $request->proses_id;
                }
                if ($request->has('supplier_id')) {
                    $product->supplier_id = $request->supplier_id;
                }
            }
            $product->save();

            return response()->json([
                "message" => "records updated successfully"
            ], 200);
        } else {
            return response()->json([
                "message" => "Product not found"
            ], 404);
        }
    }
}
