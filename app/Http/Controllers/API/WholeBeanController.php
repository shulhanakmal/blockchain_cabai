<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\RoasterWholeBean;
use Auth;
use DB;

class WholeBeanController extends Controller
{
    public $successStatus = 401;

    function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function delete(Request $request, $id)
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        $wholeBean = RoasterWholeBean::find($id);
        $wholeBean->delete();
        $this->successStatus = 200;
        $success['success'] = true;
        return response()->json($success, $this->successStatus);
    }

    public function add(Request $request)
    {
        $wholeBean = new RoasterWholeBean();
        $wholeBean = $wholeBean->setConnection($this->user->group->katalog);
        $wholeBean->whole_bean      = $request->whole_bean;
        $wholeBean->save();

        $this->successStatus = 200;
        $success['success']  = true;
        $success['data']     = $wholeBean;

        return response()->json($success, $this->successStatus);
    }

    public function detail(Request $request, $id)
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        $this->successStatus = 200;
        $success['success'] = true;
        $success['whole_bean']   = RoasterWholeBean::find($id);

        return response()->json($success, $this->successStatus);
    }

    public function edit(Request $request, $id)
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        if (RoasterWholeBean::where('id', $id)->exists()) {
            $wholeBean = RoasterWholeBean::find($id);
            if ($request->has('whole_bean')) {
                $wholeBean->whole_bean      = $request->whole_bean;
            }
            $wholeBean->save();

            return response()->json([
                "message" => "records updated successfully"
            ], 200);
        } else {
            return response()->json([
                "message" => "Product not found"
            ], 404);
        }
    }
}
