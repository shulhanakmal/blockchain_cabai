<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\RoasterSupplier;
use DB;

class RoasterSupplierController extends Controller
{
    public $successStatus = 401;

    function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function index()
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        $this->successStatus = 200;
        $success['success'] = true;
        $success['supplier']   = RoasterSupplier::all();

        return response()->json($success, $this->successStatus);
    }

    public function detail(Request $request, $id)
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        $this->successStatus = 200;
        $success['success'] = true;
        $success['supplier']   = RoasterSupplier::find($id);

        return response()->json($success, $this->successStatus);
    }

    public function delete(Request $request, $id)
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        $supplier = RoasterSupplier::find($id);
        $supplier->delete();
        $this->successStatus = 200;
        $success['success'] = true;
        return response()->json($success, $this->successStatus);
    }

    public function add(Request $request)
    {
        $supplier = new RoasterSupplier();
        $supplier = $supplier->setConnection($this->user->group->katalog);
        $supplier->nama_supplier      = $request->nama_supplier;
        $supplier->lokasi_supplier = $request->lokasi_supplier;
        $supplier->nama_petani = $request->nama_petani;
        $supplier->lat = $request->latitude;
        $supplier->long = $request->longitude;
        $supplier->estimated_production = $request->estimatedProduction;
        $supplier->elevation = $request->elevation;
        $supplier->unit = $request->unit;

        if ($request->hasFile('files')) {
            $files          = $request->file('files');
            $files->move(public_path("images/roasterSupplier/") . $this->user->group->katalog . '/' . $this->user->id . '/image/' . $supplier->id, $request->fileName);
            $supplier->gambar_petani  = "images/roasterSupplier/" . $this->user->group->katalog . '/' . $this->user->id . '/image/' . $supplier->id . '/' . $request->fileName;
        }

        $supplier->save();

        $this->successStatus = 200;
        $success['success']  = true;
        $success['data']     = $supplier;

        return response()->json($success, $this->successStatus);
    }

    public function edit(Request $request, $id)
    {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        if (RoasterSupplier::where('id', $id)->exists()) {
            $supplier = RoasterSupplier::find($id);
            if ($request->has('nama_supplier')) {
                $supplier->nama_supplier      = $request->nama_supplier;
            }
            if ($request->has('lokasi_supplier')) {
                $supplier->lokasi_supplier = $request->lokasi_supplier;
            }
            if ($request->has('nama_petani')) {
                $supplier->nama_petani = $request->nama_petani;
            }
            if ($request->has('latitude')) {
                $supplier->lat = $request->latitude;
            }
            if ($request->has('longitude')) {
                $supplier->long = $request->longitude;
            }
            if ($request->has('estimatedProduction')) {
                $supplier->estimated_production = $request->estimatedProduction;
            }
            if ($request->has('elevation')) {
                $supplier->elevation = $request->elevation;
            }
            if ($request->has('unit')) {
                $supplier->unit = $request->unit;
            }
            if ($request->hasFile('files')) {
                $files          = $request->file('files');
                $files->move(public_path("images/roasterSupplier/") . $this->user->group->katalog . '/' . $this->user->id . '/image/' . $supplier->id, $request->fileName);
                $supplier->gambar_petani  = "images/roasterSupplier/" . $this->user->group->katalog . '/' . $this->user->id . '/image/' . $supplier->id . '/' . $request->fileName;
            }
            $supplier->save();

            return response()->json([
                "message" => "records updated successfully"
            ], 200);
        } else {
            return response()->json([
                "message" => "Product not found"
            ], 404);
        }
    }
}
