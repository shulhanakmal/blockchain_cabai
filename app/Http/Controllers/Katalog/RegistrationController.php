<?php

namespace App\Http\Controllers\Katalog;

use App\Http\Controllers\Controller;
use App\Http\Controllers\LayoutController;
use Illuminate\Http\Request;
use Response;
use Auth;
use App\Group\group;
use App\Repositories\GuestRepository;
use Illuminate\Support\Facades\Crypt;
use App\Style\style;
use App\MCurrencies;
use App\Customer\Customer;
use App\Customer\CustomerDoc;
use Illuminate\Support\Facades\Artisan;

class RegistrationController extends LayoutController {

    public function __construct() {
        //
    }

    public function LoginIndex(Request $request) {
        return view('auth.loginRegCode');
    }

    public function login(Request $request) {
        $this->validateForm($request, 'create');
        $reg_code = $request->code;
        $cek_code = Customer::where('reg_code', $reg_code)->where('status', 1)->count();
        if ($cek_code > 0) {
            session(['registration_code' => $request->code]);
            return redirect()->back()->with(['message' => 'Login with registration code success', 'reg_code' => session('registration_code')]);
        } else {
            toastr()->error('Your registration number is not registered yet. Or Your Account Is Locked');
            return back();
        }
    }

    public function validateForm($request, $type) {
        $messages = [
            'code.required' => 'Registration code is required!'
        ];
        $this->validate($request,[
            'code' => 'required'
        ],$messages);
        return view('auth.loginRegCode', [
            'data' => $request
        ]);
    }

    public function index($hash) {
        $code = Crypt::decryptString($hash);
        $data = Customer::where('reg_code', $code)->first();
        return view('buyerIsiData', ['data' => $data]);
    }

    public function store(Request $request) {        
        $simpan = Customer::find($request->id_customer);
        $simpan->roasting_capacity = $request->roastingCapacity;
        $simpan->save();

        if($request->has('file')){
            $files = $request->file('file');

            // insert table customer_docs
            foreach($files as $index => $f){
                $doc = new CustomerDoc;
                $doc->customer_id = $request->id_customer;
                $doc->nama_dokumen = $request->docName[$index];
                $doc->filename = $f->getClientOriginalName();
                $doc->path = $f->store('public/files');
                $doc->save();
            }
        }

        toastr()->success('Data successfully stored');
        return redirect()->back();
    }

}