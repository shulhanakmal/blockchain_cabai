<?php

namespace App\Http\Controllers\Katalog;

use App\Http\Controllers\Controller;
use App\Http\Controllers\LayoutController;
use Illuminate\Http\Request;
use Response;
use Auth;
use DB;
use Hash;
use App\Category\Category;
use App\Product\Product;
use App\Vendor\vendor;
use App\Product\ProductVendor;
use App\Product\ProductGroupRule;
use App\Category\CategoryProduct;
use App\Lokasi\Location;
use App\Katalog\ListKatalog;
use App\Group\group;
use App\Admin\AdminUser;
use App\ModelHasRole;
use Spatie\Permission\Models\Role;
use App\Repositories\GuestRepository;
use Illuminate\Support\Facades\Crypt;
use App\Style\style;
use App\MCurrencies;
use App\Customer\Customer;
use App\Customer\CustomerDoc;
use App\Customer\CustomerGroup;
use App\Customer\CustomerAddress;
use App\Customer\CustomerGrouping;
use Illuminate\Support\Facades\Artisan;

class GuestController extends LayoutController {

    protected $guestRepository;

    function __construct(GuestRepository $guestRepository) {
        $this->guestRepository = $guestRepository;
    }
    public function redirect(Request $request) {
        return redirect('/cabai');
    }

    public function index(Request $request) {
        DB::setDefaultConnection('mysql');

        $prod = ListKatalog::all();
        $dbkat = array();

        foreach($prod as $p) {
            array_push($dbkat, group::where('code', $p->code)->first());
        }

        $product = null;
        $product2 = [];
        $product3 = null;

        $dbkatNew = array();

        foreach($dbkat as $index => $db) {
            $conn = $db->katalog;
            $prodv = new ProductVendor();
            $prodv->setConnection($conn);
            $prodv = $prodv->count();
            if($prodv > 0) {
                array_push($dbkatNew, $db);
            }
        }

        if(count($dbkatNew) > 0) {
            $dbkatNew2 = array();

            foreach($dbkatNew as $index => $db) {
                $conn = $db->katalog;
                $q = new ProductVendor();
                $q->setConnection($conn);
                $q = $q->where('status', 1)->count();
                if($q > 0) {
                    array_push($dbkatNew2, $db);
                }
            }

            foreach($dbkatNew2 as $index2 => $db2) {
                $q = DB::table($db2->katalog . '.product_vendors')->select(DB::raw("product_vendors.*, products.name, products.short_description, products.small_image, vendors.vendor_name, '".$db2->code."' as code"))
                ->join($db2->katalog . '.product_groups as pg', 'pg.product_id', 'product_vendors.product_id')
                ->join($db2->katalog . '.product_group_rules as pgr', function($join) {
                    $join->on('pg.rule_id', 'pgr.id')->whereIn('cust_groups', [1]);
                })
                ->join($db2->katalog . '.vendor as vendors', function($join) {
                    $join->on('vendors.id', 'product_vendors.vendor_id')->where('product_vendors.status', 1);
                })
                ->join($db2->katalog . '.products as products', function($join) {
                    $join->on('products.id', 'product_vendors.product_id')->where('product_vendors.status', 1);
                })
                ->where('product_vendors.status', 1);
                if($index2 == 0) {
                    $product = $q;
                } else {
                    $product->union($q);
                }
            }
        } else {
            $q = DB::table('cabai_guest.product_vendors')->select(DB::raw("product_vendors.*, products.name, products.short_description, products.small_image, vendors.vendor_name, '".$dbkat[0]->code."' as code"))
            ->join('cabai_guest.product_groups as pg', 'pg.product_id', 'product_vendors.product_id')
            ->join('cabai_guest.product_group_rules as pgr', function($join) {
                $join->on('pg.rule_id', 'pgr.id')->whereIn('cust_groups', [1]);
            })
            ->join('cabai_guest.vendor as vendors', function($join) {
                $join->on('vendors.id', 'product_vendors.vendor_id')->where('vendors.status', 1);
            })
            ->join('cabai_guest.products as products', function($join) {
                $join->on('products.id', 'product_vendors.product_id')->where('products.status', 1);
            })
            ->where('product_vendors.status', 1);
            $product = $q;
        }

        $prod = $product->paginate(10);
        // dd($prod);
        $prod->appends($request->only('keyword'));
        // echo json_encode($prod);
        // die();
        return view('katalog.index', [
            'product' => $prod,
            'style' => $this->get_style()
        ])
        ->with('i', ($request->input('page', 1) - 1) * 8);
    }

    public function detail_product(Request $request, $code, $hash) {
        $id = Crypt::decryptString($hash);
        DB::setDefaultConnection(group::where('code', $code)->first()->katalog);
        $product_detail = $this->guestRepository->katalog_detail_product($id);
        $related_product = $this->guestRepository->related_product($product_detail, $id, $code)->paginate(5)->onEachSide(1);
        return view('katalog.product.detail.index', [
            'product_detail' => $product_detail->first(),
            'category' => $this->get_ctg(),
            'related_product' => $related_product,
            'style' => $this->get_style(),
            'code_url' => $code
        ])
        ->with('i', ($request->input('page', 1) - 1) * 5)->with('search', $request->input('search'));
    }

    public function daftarTenant() {
        $currency = MCurrencies::where('status', 1)->where('flag', 1)->get();

        return view('katalog.daftarTenant', [
            'style' => $this->get_style(),
            'currency' => $currency
        ]);
    }

    public function setEnvironmentValue($envKey, $envValue)
    {
        $envFile = base_path() . "/config/database.php";
        $str = file_get_contents($envFile);

        $oldValue = strtok($str, $envKey);

        $str = str_replace($envKey, $envValue, $str);

        $fp = fopen($envFile, 'w');
        fwrite($fp, $str);
        fclose($fp);
    }

    public function storeDaftarTenant(Request $request) {
        $tenantLow = strtolower($request->tenant);
        $tenantUp = strtoupper($request->tenant);
        $envFile = app()->environmentFilePath();
        $str = file_get_contents($envFile);

        $conf = $this->setEnvironmentValue("'connections' => [",
            "'connections' => [\r\n
        'ecatcoffee_".$tenantLow."' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST_KATALOG_".$tenantUp."', '127.0.0.1'),
            'port' => env('DB_PORT_KATALOG_".$tenantUp."', '3306'),
            'database' => env('DB_DATABASE_KATALOG_".$tenantUp."', 'forge'),
            'username' => env('DB_USERNAME_KATALOG_".$tenantUp."', 'forge'),
            'password' => env('DB_PASSWORD_KATALOG_".$tenantUp."', ''),
            'unix_socket' => env('DB_SOCKET_KATALOG_".$tenantUp."', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null
        ],\r\n"
        );

        $fp = fopen($envFile, 'w');
        fwrite($fp, $str . "DB_CONNECTION_KATALOG_".$tenantUp."=mysql\r\nDB_HOST_KATALOG_".$tenantUp."=127.0.0.1\r\nDB_PORT_KATALOG_".$tenantUp."=3306\r\nDB_DATABASE_KATALOG_".$tenantUp."=ecatcoffee_".$tenantLow."\r\nDB_USERNAME_KATALOG_".$tenantUp."=root\r\nDB_PASSWORD_KATALOG_".$tenantUp."=Aryosengkun1!\r\n\r\n");
        fclose($fp);

        $output = array();
        sleep(20);

        DB::beginTransaction();
        try {
            DB::statement('drop database IF EXISTS ecatcoffee_'.$tenantLow);
            DB::statement('create database ecatcoffee_'.$tenantLow);
            shell_exec(base_path().'/artisan config:cache');
            DB::statement('drop database ecatcoffee_'.$tenantLow);
            shell_exec(base_path().'/artisan config:cache');
            DB::statement('create database ecatcoffee_'.$tenantLow);
            shell_exec(base_path().'/artisan config:cache');
            shell_exec('(cd '. base_path() .' && composer dump-autoload)');
            DB::statement("create table ecatcoffee_".$tenantLow.".batches LIKE cabai_guest.batches");
            DB::statement("create table ecatcoffee_".$tenantLow.".batchs LIKE cabai_guest.batchs");
            DB::statement("create table ecatcoffee_".$tenantLow.".bijis LIKE cabai_guest.bijis");
            DB::statement("create table ecatcoffee_".$tenantLow.".blended_beans LIKE cabai_guest.blended_beans");
            DB::statement("create table ecatcoffee_".$tenantLow.".categories LIKE cabai_guest.categories");
            DB::statement("create table ecatcoffee_".$tenantLow.".category_products LIKE cabai_guest.category_products");
            DB::statement("create table ecatcoffee_".$tenantLow.".certificate_batchs LIKE cabai_guest.certificate_batchs");
            DB::statement("create table ecatcoffee_".$tenantLow.".failed_jobs LIKE cabai_guest.failed_jobs");
            DB::statement("create table ecatcoffee_".$tenantLow.".image_batchs LIKE cabai_guest.image_batchs");
            DB::statement("create table ecatcoffee_".$tenantLow.".image_products LIKE cabai_guest.image_products");
            DB::statement("create table ecatcoffee_".$tenantLow.".jenis LIKE cabai_guest.jenis");
            DB::statement("create table ecatcoffee_".$tenantLow.".locations LIKE cabai_guest.locations");
            DB::statement("create table ecatcoffee_".$tenantLow.".migrations LIKE cabai_guest.migrations");
            DB::statement("create table ecatcoffee_".$tenantLow.".product_contracts LIKE cabai_guest.product_contracts");
            DB::statement("create table ecatcoffee_".$tenantLow.".product_group_rules LIKE cabai_guest.product_group_rules");
            DB::statement("create table ecatcoffee_".$tenantLow.".product_groups LIKE cabai_guest.product_groups");
            DB::statement("create table ecatcoffee_".$tenantLow.".product_media_galleries LIKE cabai_guest.product_media_galleries");
            DB::statement("create table ecatcoffee_".$tenantLow.".product_media_gallery_values LIKE cabai_guest.product_media_gallery_values");
            DB::statement("create table ecatcoffee_".$tenantLow.".products LIKE cabai_guest.products");
            DB::statement("create table ecatcoffee_".$tenantLow.".product_shippings LIKE cabai_guest.product_shippings");
            DB::statement("create table ecatcoffee_".$tenantLow.".product_vendors LIKE cabai_guest.product_vendors");
            DB::statement("create table ecatcoffee_".$tenantLow.".proses LIKE cabai_guest.proses");
            DB::statement("create table ecatcoffee_".$tenantLow.".proseses LIKE cabai_guest.proseses");
            DB::statement("create table ecatcoffee_".$tenantLow.".roaster_bijis LIKE cabai_guest.roaster_bijis");
            DB::statement("create table ecatcoffee_".$tenantLow.".roaster_blended_beans LIKE cabai_guest.roaster_blended_beans");
            DB::statement("create table ecatcoffee_".$tenantLow.".roaster_certificate_roasting LIKE cabai_guest.roaster_certificate_roasting");
            DB::statement("create table ecatcoffee_".$tenantLow.".roaster_inventories LIKE cabai_guest.roaster_inventories");
            DB::statement("create table ecatcoffee_".$tenantLow.".roaster_jenis LIKE cabai_guest.roaster_jenis");
            DB::statement("create table ecatcoffee_".$tenantLow.".roaster_products LIKE cabai_guest.roaster_products");
            DB::statement("create table ecatcoffee_".$tenantLow.".roaster_proses LIKE cabai_guest.roaster_proses");
            DB::statement("create table ecatcoffee_".$tenantLow.".roaster_roasting_profiles LIKE cabai_guest.roaster_roasting_profiles");
            DB::statement("create table ecatcoffee_".$tenantLow.".roaster_roastings LIKE cabai_guest.roaster_roastings");
            DB::statement("create table ecatcoffee_".$tenantLow.".roaster_suppliers LIKE cabai_guest.roaster_suppliers");
            DB::statement("create table ecatcoffee_".$tenantLow.".roaster_units LIKE cabai_guest.roaster_units");
            DB::statement("create table ecatcoffee_".$tenantLow.".roaster_weights LIKE cabai_guest.roaster_weights");
            DB::statement("create table ecatcoffee_".$tenantLow.".roaster_whole_beans LIKE cabai_guest.roaster_whole_beans");
            DB::statement("create table ecatcoffee_".$tenantLow.".style LIKE cabai_guest.style");
            DB::statement("create table ecatcoffee_".$tenantLow.".suppliers LIKE cabai_guest.suppliers");
            DB::statement("create table ecatcoffee_".$tenantLow.".transaction_explorer LIKE cabai_guest.transaction_explorer");
            DB::statement("create table ecatcoffee_".$tenantLow.".units LIKE cabai_guest.units");
            DB::statement("create table ecatcoffee_".$tenantLow.".vendor LIKE cabai_guest.vendor");
            DB::statement("create table ecatcoffee_".$tenantLow.".vendor_addresses LIKE cabai_guest.vendor_addresses");

            // tambah group di db master
            DB::connection("mysql")->table('group')->insert([
                'name' => $request->tenant,
                'katalog' => "ecatcoffee_".$tenantLow,
                'code' => strtolower($request->tenant),
                'module' => "katalog".$tenantLow,
                'currency' => $request->currency,
                'email' => $request->email,
                'round_price' => $request->round_price,
            ]);
            $group_id = DB::getPdo()->lastInsertId();

            DB::connection("mysql")->table('customer_groups')->insert([
                'ids' => 1,
                'group_id' => $group_id,
                'customer_group_code' => 'General',
                'tax_class_id' => 3,
            ]);

            // tambah admin user di db master
            DB::connection("mysql")->table('admin_users')->insert([
                'firstname' => $request->firstName,
                'lastname' => $request->LastName,
                'email' => $request->email,
                'username' => $request->username,
                'password' => Hash::make($request->passowrd),
                'group_id' => $group_id,
                'status' => 1,
            ]);
            $admin_id = DB::getPdo()->lastInsertId();
            $admin = AdminUser::find($admin_id);

            // tambah data ModelHasRole
            $admin->assignRole(['Admin']);

            // tambah list katalog
            DB::connection("mysql")->table('list_katalogs')->insert([
                'name' => $request->tenant,
                'group_id' => $group_id,
                'code' => $tenantLow,
                'module' => "katalog".$tenantLow,
            ]);

            // tambah style di db master
            $logo = null;
            $small_logo = null;
            if($request->has('file')){
                $logo = 'logo_'.$tenantLow.'.png';
                $small_logo = 'logo_'.$tenantLow.'.png';
            }

            DB::connection("mysql")->table('style')->insert([
                'group_id' => $group_id,
                'logo' => $logo,
                'small_logo' => $small_logo,
                'color' => "#000000",
                'url_image' => "/storage/files/".$tenantLow.'.png',
            ]);

            $files          = $request->file('file');
            $files->move(public_path('techone/images/' . '/'), 'logo_'.$tenantLow.'.png');

        } catch (\Exception $e) {
            DB::rollBack();
            echo json_encode($e->getMessage());
            die();
        }
        DB::commit();

        return redirect()->route('guest.index')->with('success','Tenant created successfully');
    }

    public function createStyle($tenantLow) {
        shell_exec(base_path().'/artisan config:clear');
        shell_exec('(cd '. base_path() .' && composer dumpauto)');
        DB::setDefaultConnection('ecatcoffee_'.$tenantLow);
        $style = style::all();
        // dd($style);
        return true;
    }

    public function daftarBuyer() {
        $currency = MCurrencies::where('status', 1)->where('flag', 1)->get();
        $tenant = ListKatalog::all();
        $roles = Role::where('guard_name', 'customer')->where('name', '!=', 'Customer')->get();

        return view('katalog.daftarBuyer', [
            'style' => $this->get_style(),
            'tenant' => $tenant,
            'roles' => $roles
        ]);
    }

    public function storeDaftarBuyer(Request $request) {
        // validasi repeat password
        if ($request->password != $request->pass) {
            toastr()->error('Password is not the same!');
            return redirect()->back();
        }

        // insert table customer
        $save = new Customer;
        $save->first_name = $request->first_name;
        $save->middle_name = $request->middle_name;
        $save->last_name = $request->last_name;
        $save->email = $request->emailBuyer; // email untuk login
        $save->password = Hash::make($request->password);
        $save->group_id = $request->tenant;
        $save->company = $request->company;
        $save->gender = $request->gender;
        $save->status = 1;
        $save->tlp = $request->telephone;
        $save->address = $request->street;
        $save->contact_name = $request->name;
        $save->email_contact = $request->email;
        $save->reg_code = str_random(8);
        $save->save();

        $save->assignRole(['Customer']);
        $save->assignRole($request->roles);

        // insert table customer address
        $cam = new CustomerAddress;
        $cam->customer_id = $save->id;
        $cam->street = $request->street;
        $cam->province = $request->region;
        $cam->city = $request->city;
        $cam->region = $request->region;
        $cam->telephone = $request->telephone;
        $cam->postcode = $request->postcode;
        if($request->fax){
            $cam->fax = $request->fax;
        }
        $cam->save();

        // insert table customer grouping
        if($request->has('group')) {
            $custGrouping = new CustomerGrouping;
            $custGrouping->setConnection('mysql');
            $custGrouping = $custGrouping->where('customer_id', $save->id)->get();
            if(count($custGrouping) > 0) {
                $custGrouping->delete();
            }

            foreach($request->group as $g) {
                $cg = new CustomerGrouping;
                $cg->customer_id = $save->id;
                $cg->customer_group_id = $g;
                $cg->save();
            }
        }

        // send email
        send_email_daftar_buyer($save->id, $save->group_id);
        send_email_tenant_daftar_buyer($save->id, $save->group_id);

        toastr()->success('Data successfully stored!');
        return redirect()->route('guest.index');
    }

    public function getGroupCustomer(Request $request) {
        $customer_group = CustomerGroup::where('group_id', $request->groupId)->get();

        return $customer_group;
    }
}
