<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlendedBean extends Model
{
    protected $table = 'blended_beans';

    public function batch()
    {  
        return $this->belongsTo('App\Batch', 'batch_id');
    }

    public function product()
    {  
        return $this->belongsTo('App\Product\Product', 'product_id');
    }
}
