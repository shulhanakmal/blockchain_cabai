<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoasterProduct extends Model
{
    //

    public function jenis()
    {  
        return $this->belongsTo('App\RoasterJenis', 'jenis_id');
    }

    public function proses()
    {
        return $this->belongsTo('App\RoasterProses', 'proses_id');
    }

    public function supplier()
    {
        return $this->belongsTo('App\RoasterSupplier', 'supplier_id');
    }

    public function biji()
    {
        return $this->belongsTo('App\RoasterBiji', 'biji_id');
    }

    public function greenBeans()
    {
        return $this->belongsTo('App\Product\Product', 'productID');
    }
}
