<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Mail\DaftarBuyerEmailTenant;
use Mail;

class DaftarBuyerEmailTenantJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $data;
    protected $group;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data, $group)
    {
        $this->data = $data;
        $this->group = $group;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = $this->data;
        $group = $this->group;
        $email = new DaftarBuyerEmailTenant($data, $group);
        Mail::to($this->group->email)->send($email);
    }
}
