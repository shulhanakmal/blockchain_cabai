<?php

namespace App\Policies;

use App\Customer\Customer;
use App\Vendor\VendorMaster;
use App\Admin\AdminUser;
use App\Order\Order;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrderPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any work orders.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(AdminUser $amdins, VendorMaster $vendors, Customer $customers)
    {
        //
    }

    /**
     * Determine whether the user can view the work order.
     *
     * @param  \App\Order  $orders
     * @return mixed
     */
    public function view(AdminUser $amdins, VendorMaster $vendors, Customer $customers, Order $orders)
    {
        //
    }

    public function send(Customer $customers, Order $orders) {
        $workOrder = $orders->work_order;
        if ($customers->can('send', $workOrder)) {
            return true;
        }
        return false;
    }

    public function cancelorder(Customer $customers, Order $orders) {
        $workOrder = $orders->work_order;
        if ($customers->can('cancelorder', $workOrder)) {
            return true;
        }
        return false;
    }

    public function reorder(Customer $customers, Order $orders) {
        $workOrder = $orders->work_order;
        if ($customers->can('reorder', $workOrder)) {
            return true;
        }
        return false;
    }

    public function createpo(Customer $customers, Order $orders) {
        $workOrder = $orders->work_order;
        if ($customers->can('createpo', $workOrder)) {
            return true;
        }
        return false;
    }

    public function completeorder(Customer $customers, Order $orders) {
        $workOrder = $orders->work_order;
        if ($customers->can('completeorder', $workOrder)) {
            return true;
        }
        return false;
    }

    public function approve(AdminUser $admins, Order $orders) {
        $workOrder = $orders->work_order;
        if ($admins->can('approve', $workOrder)) {
            return true;
        }
        return false;
    }

    public function decline(AdminUser $admins, Order $orders) {
        $workOrder = $orders->work_order;
        if ($admins->can('decline', $workOrder)) {
            return true;
        }
        return false;
    }

    public function confirm(VendorMaster $vendors, Order $orders) {
        $workOrder = $orders->work_order;
        if ($vendors->can('confirm', $workOrder)) {
            return true;
        }
        return false;
    }

    public function reject(VendorMaster $vendors, Order $orders) {
        $workOrder = $orders->work_order;
        if ($vendors->can('reject', $workOrder)) {
            return true;
        }
        return false;
    }

    public function deliver(VendorMaster $vendors, Order $orders) {
        $workOrder = $orders->work_order;
        if ($vendors->can('deliver', $workOrder)) {
            return true;
        }
        return false;
    }
}
