<?php

namespace App\Policies;

use App\Action;
use App\Target;
use App\Customer\Customer;
use App\Vendor\VendorMaster;
use App\Admin\AdminUser;
use App\Flow\WorkOrder;
use Illuminate\Auth\Access\HandlesAuthorization;

class WorkOrderPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any work orders.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(AdminUser $amdins, VendorMaster $vendors, Customer $customers)
    {
        //
    }

    /**
     * Determine whether the user can view the work order.
     *
     * @param  \App\User  $user
     * @param  \App\WorkOrder  $workOrder
     * @return mixed
     */
    public function view(AdminUser $amdins, VendorMaster $vendors, Customer $customers, WorkOrder $workOrder)
    {
        //
    }

    public function send(Customer $customers, WorkOrder $workOrder) {
        if ($workOrder->findActiveActionByActionType($customers,'send')) {
            return true;
        }
        return false;
    }

    public function cancelorder(Customer $customers, WorkOrder $workOrder) {
        if ($workOrder->findActiveActionByActionType($customers,'cancel send')) {
            return true;
        }
        return false;
    }

    public function reorder(Customer $customers, WorkOrder $workOrder) {
        if ($workOrder->findActiveActionByActionType($customers,'reorder')) {
            return true;
        }
        return false;
    }

    public function createpo(Customer $customers, WorkOrder $workOrder) {
        if ($workOrder->findActiveActionByActionType($customers, 'send')) {
            return true;
        }
        return false;
    }

    public function completeorder(Customer $customers, WorkOrder $workOrder) {
        if ($workOrder->findActiveActionByActionType($customers,'complete')) {
            return true;
        }
        return false;
    }

    public function approve(AdminUser $admins, WorkOrder $workOrder) {
        if ($workOrder->findActiveActionByActionType($admins,'approve')) {
            return true;
        }
        return false;
    }

    public function decline(AdminUser $admins, WorkOrder $workOrder) {
        if ($workOrder->findActiveActionByActionType($admins,'decline')) {
            return true;
        }
        return false;
    }

    public function confirm(VendorMaster $vendors, WorkOrder $workOrder) {
        if ($workOrder->findActiveActionByActionType($vendors, 'approve')) {
            return true;
        }
        return false;
    }

    public function reject(VendorMaster $vendors, WorkOrder $workOrder) {
        if ($workOrder->findActiveActionByActionType($vendors,'decline')) {
            return true;
        }
        return false;
    }

    public function deliver(VendorMaster $vendors, WorkOrder $workOrder) {
        if ($workOrder->findActiveActionByActionType($vendors,'send')) {
            return true;
        }
        return false;
    }
}
