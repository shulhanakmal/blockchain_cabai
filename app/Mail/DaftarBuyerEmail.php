<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DaftarBuyerEmail extends Mailable
{
    use Queueable, SerializesModels;
    protected $data;
    protected $group;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data, $group)
    {
        $this->data = $data;
        $this->group = $group;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data['buyer'] = $this->data;
        $data['group'] = $this->group;

        return $this
            ->from($address = $data['group']->email)
            ->view('Email.email_daftar_buyer', $data)
            ->subject('B2B Platform - Buyer Registration');
    }
}
