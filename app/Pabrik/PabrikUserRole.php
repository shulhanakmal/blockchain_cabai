<?php

namespace App\Pabrik;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Hash;
use Laravel\Passport\HasApiTokens;

class PabrikUserRole extends Authenticatable {

    protected $connection= 'mysql';

    protected $guard = 'pabrik';
}
