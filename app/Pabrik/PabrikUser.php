<?php

namespace App\Pabrik;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Hash;
use Laravel\Passport\HasApiTokens;

class PabrikUser extends Authenticatable {
    use Notifiable, HasApiTokens;

    protected $connection= 'mysql';

    protected $guard = 'pabrik';

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getRole(){
        return $this->hasOne('App\Pabrik\PabrikUserRole', 'role', 'role');
    }
}
