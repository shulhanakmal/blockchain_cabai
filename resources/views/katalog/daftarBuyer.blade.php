<!DOCTYPE html>
<html lang="en">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
    <title>B2B Platform</title>
    <!-- Icons-->
    <link href="{{ asset('assets/node_modules/@coreui/icons/css/coreui-icons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/node_modules/flag-icon-css/css/flag-icon.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/node_modules/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/node_modules/simple-line-icons/css/simple-line-icons.css') }}" rel="stylesheet">
    <!-- Main styles for this application-->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/pace-progress/css/pace.min.css') }}" rel="stylesheet">
    {{-- select2 --}}
    <link href="{{ asset('assets/node_modules/select2/dist/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/node_modules/select2/dist/css/select-2.min.css') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
    @toastr_css
    <style>
        .label{
            padding-top: 7px;
        }
    </style>
  </head>
  <body class="app flex-row align-items-center" style="background-color: white">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-12"><br><br>

            <div class="card shadow-lg bg-light rounded">
                <form action="{{route('daftar.buyer.store')}}" method="POST" enctype="multipart/form-data">@csrf
                    <div class="card-body">
                        <h1>B2B Platform Buyer</h1>
                        <p class="text-muted">Please fill in the registration form below</p>
                        <hr>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="first_name">First Name <i class="text-danger">(*)</i></label>
                            <div class="col-md-10">
                                <input class="form-control @if($errors->has('first_name')) is-invalid @endif" id="first_name" name="first_name" type="text">
                                <em id="firstname-error" class="error invalid-feedback">Enter First Name</em>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="middle_name">Middle Name</label>
                            <div class="col-md-4">
                                <input class="form-control @if($errors->has('middle_name')) is-invalid @endif" id="middle_name" name="middle_name" type="text">
                                <em id="firstname-error" class="error invalid-feedback">Enter Middle Name</em>
                            </div>
                            <label class="col-md-2 col-form-label text-right" for="last_name">Last Name <i class="text-danger">(*)</i></label>
                            <div class="col-md-4">
                                <input class="form-control @if($errors->has('last_name')) is-invalid @endif" id="last_name" name="last_name" type="text">
                                <em id="firstname-error" class="error invalid-feedback">Enter Last Name</em>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="emailBuyer">Email <i class="text-danger">(*)</i></label>
                            <div class="col-md-4">
                                <input class="form-control @if($errors->has('email')) is-invalid @endif" id="emailBuyer" name="emailBuyer" type="email">
                                <em id="firstname-error" class="error invalid-feedback">Enter Email</em>
                            </div>
                            <label class="col-md-2 col-form-label text-right" for="email">Gender <i class="text-danger">(*)</i></label>
                            <div class="col-md-4">
                                <select class="custom-select val-custom form-control select2 @if($errors->has('gender')) is-invalid @endif" data-placeholder="Choose Gender" name="gender" id="gender">
                                    <option></option>
                                    <option value="1">Male</option>
                                    <option value="2">Female</option>
                                </select>
                                <em id="firstname-error" class="error invalid-feedback">Enter Gender</em>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="password">Password <i class="text-danger">(*)</i></label>
                            <div class="col-md-4">
                                <input class="form-control @if($errors->has('password')) is-invalid @endif" id="password" name="password" type="password">
                                <em id="firstname-error" class="error invalid-feedback">Enter Password</em>
                            </div>
                            <label class="col-md-2 col-form-label text-right" for="pass">Confirm Password <i class="text-danger">(*)</i></label>
                            <div class="col-md-4">
                                <input class="form-control @if($errors->has('pass')) is-invalid @endif" id="pass" name="pass" type="password">
                                <em id="firstname-error" class="error invalid-feedback">Enter Password</em>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="email">Tenant <i class="text-danger">(*)</i></label>
                            <div class="col-md-4">
                                <select name="tenant" id="tenant" class="form-control select2" onchange="getGroup()">
                                    <option value="">Choose Tenant</option>
                                    @foreach($tenant as $t)
                                        <option value="{{$t->group_id}}">{{$t->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <label class="col-md-2 col-form-label text-right" for="email">Group <i class="text-danger">(*)</i></label>
                            <div class="col-md-4">
                                <select class="form-control select2 select2-multiple select2-hidden-accessible @if($errors->has('group')) is-invalid @endif" data-placeholder="Choose Group" name="group[]" id="group" multiple="" data-select2-id="select2-3" tabindex="-1" aria-hidden="true">
                                    <option></option>
                                </select>
                                <em id="firstname-error" class="error invalid-feedback">Choose Group</em>
                            </div>
                        </div>

                        <h5 class="card-title" style="margin-top:40px;"><b>Address</b></h5>
                        <hr>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Company Name<i class="text-danger">(*)</i></label>
                            <div class="col-md-4">
                                <input class="form-control shadow-sm" type="text" name="company" required>
                            </div>
                            <label class="col-md-2 col-form-label text-right">Country<i class="text-danger">(*)</i></label>
                            <div class="col-md-4">
                                <input class="form-control shadow-sm" type="text" name="country" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="street">Address <i class="text-danger">(*)</i></label>
                            <div class="col-md-10">
                                <textarea rows="3" class="form-control @if($errors->has('street')) is-invalid @endif" id="street" name="street" type="text"></textarea>
                                <em id="firstname-error" class="error invalid-feedback">Enter Address</em>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="city">City <i class="text-danger">(*)</i></label>
                            <div class="col-md-10">
                                <input class="form-control @if($errors->has('city')) is-invalid @endif" id="city" name="city" type="text">
                                <em id="firstname-error" class="error invalid-feedback">Enter City</em>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="region">Province <i class="text-danger">(*)</i></label>
                            <div class="col-md-4">
                                <input class="form-control @if($errors->has('region')) is-invalid @endif" id="region" name="region" type="text">
                                <em id="firstname-error" class="error invalid-feedback">Enter Province</em>
                            </div>
                            <label class="col-md-2 col-form-label text-right" for="postcode">Zip Code <i class="text-danger">(*)</i></label>
                            <div class="col-md-4">
                                <input class="form-control @if($errors->has('postcode')) is-invalid @endif" id="postcode" name="postcode" type="text">
                                <em id="firstname-error" class="error invalid-feedback">Enter Zip Code</em>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="roles">Role<i class="text-danger">(*)</i></label>
                            <div class="col-md-10">
                                <select class="form-control select2 select2-multiple select2-hidden-accessible @if($errors->has('roles')) is-invalid @endif" data-placeholder="Choose Role" name="roles[]" id="roles" multiple="" data-select2-id="select2-2" tabindex="-1" aria-hidden="true">
                                    <option></option>
                                    @foreach($roles as $r)
                                        <option value="{{ $r->id }}">{{ $r->name }}</option>
                                    @endforeach
                                </select>
                                <em id="firstname-error" class="error invalid-feedback">Choose Role</em>
                            </div>
                        </div>

                        <h5 class="card-title" style="margin-top:40px;"><b>Contact</b></h5>
                        <hr>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="Name">Name <i class="text-danger">(*)</i></label>
                            <div class="col-md-4">
                                <input class="form-control @if($errors->has('name')) is-invalid @endif" id="name" name="name" type="text">
                                <em id="firstname-error" class="error invalid-feedback">Enter Name</em>
                            </div>
                            <label class="col-md-2 col-form-label text-right" for="email">Email <i class="text-danger">(*)</i></label>
                            <div class="col-md-4">
                                <input class="form-control @if($errors->has('email')) is-invalid @endif" id="email" name="email" type="email">
                                <em id="firstname-error" class="error invalid-feedback">Enter Fax</em>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="telephone">Phone <i class="text-danger">(*)</i></label>
                            <div class="col-md-4">
                                <input class="form-control @if($errors->has('telephone')) is-invalid @endif" id="telephone" name="telephone" type="number">
                                <em id="firstname-error" class="error invalid-feedback">Enter Phone</em>
                            </div>
                            <label class="col-md-2 col-form-label text-right" for="fax">Fax</label>
                            <div class="col-md-4">
                                <input class="form-control @if($errors->has('fax')) is-invalid @endif" id="fax" name="fax" type="text">
                                <em id="firstname-error" class="error invalid-feedback">Enter Fax</em>
                            </div>
                        </div>

                        {{-- <h5 class="card-title" style="margin-top:40px;"><b>Details</b></h5>
                        <hr>
                        <div class="form-group row">
                            <div class="col-md-6" id="only-number">
                                <label class="label">Roasting Capacity per Month<i class="text-danger">(*)</i></label>
                                <input class="form-control shadow-sm number" type="text" name="roastingCapacity" placeholder="-Kg" required>
                            </div>
                        </div>
                        <hr>
                        <label class="label"><h5>Legal Document(Optional)</h5></label>
                        <div id="docField">
                            <div class="form-group row" id="field">
                                <label class="label col-md-2">Document Name</label>
                                <div class="col-md-4">
                                    <input class="form-control shadow-sm" type="text" name="docName[]">
                                </div>
                                <label class="label col-md-1">File</label>
                                <div class="col-md-4">
                                    <input class="form-control shadow-sm" type="file" name="file[]">
                                </div>
                                <div class="col-md-1">
                                    <button type="button" id="tambahDokumen" onclick="tambahDoc()" class="btn btn-primary" title="Add Document"><i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                        </div> --}}

                        {{-- <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="stock">Status <i class="text-danger">*</i></label>
                            <div class="col-md-10">
                                <div class="input-group">
                                    <label class="col-form-label switch switch-md switch-label switch-pill switch-primary">
                                        <input class="switch-input @if($errors->has('status_customer')) is-invalid @endif" type="checkbox" id="status_customer" name="status_customer" onclick="checkBox()">
                                        <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
                                    </label>
                                    &nbsp;&nbsp;&nbsp;<label id="status_name" name="status_name" class="col-form-label">Inactive</label>
                                </div>
                                <em id="firstname-error" class="error invalid-feedback">Choose Status</em>
                            </div>
                        </div> --}}
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-primary float-right" type="submit">
                            Save
                        </button>
                        <a class="btn btn-danger" href="{{ route('guest.index') }}">Back</a>
                    </div>
                </form>
            </div>
        </div>
      </div>
    </div>
    <!-- CoreUI and necessary plugins-->
    <script src="{{ asset('assets/node_modules/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/node_modules/popper.js/dist/umd/popper.min.js') }}"></script>
    <script src="{{ asset('assets/node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/node_modules/pace-progress/pace.min.js') }}"></script>
    <script src="{{ asset('assets/node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js') }}"></script>
    <script src="{{ asset('assets/node_modules/@coreui/coreui-pro/dist/js/coreui.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    @toastr_js
    @toastr_render
    <script src="{{ asset('assets/node_modules/select2/dist/js/select2.min.js') }}"></script>
    <script>
        @foreach ($errors->all() as $error)
            toastr.error("{{$error}}")
        @endforeach

        $('#tenant').select2({
            allowClear: false,
            theme: 'bootstrap',
            width: "100%",
            placeholder: "Choose Tenant",
        });

        $('#gender').select2({
            allowClear: false,
            theme: 'bootstrap',
            width: "100%"
        });

        $('#group').select2({
            allowClear: false,
            theme: 'bootstrap',
            width: "100%"
        });

        $('#roles').select2({
            allowClear: false,
            theme: 'bootstrap',
            width: "100%"
        });

        function getGroup() {
            var groupId = $('#tenant').val();

            $.ajax({
                url: "{{ route('get.group') }}",
                data: {
                    groupId: groupId
                },
                type: "GET",
                success: function(response) {
                    var html = ``;

                    response.forEach( data=> {
                        html+=`<option value="` + data.ids + `">` + data.customer_group_code + `</option>`;
                    })

                    document.getElementById("group").innerHTML = html;
        
                    $('.select2').select2({
                        allowClear: false,
                        theme: 'bootstrap',
                        width: "100%",
                    });
                }
            });
        }

        // function tambahDoc() {
        //     var html = '';
            
        //     html +=     `<div class="form-group row" id="field">`;
        //     html +=         `<label class="label col-md-2">Document Name</label>`;
        //     html +=         `<div class="col-md-4">`;
        //     html +=             `<input class="form-control shadow-sm" type="text" name="docName[]">`;
        //     html +=         `</div>`;
        //     html +=         `<label class="label col-md-1">File</label>`;
        //     html +=         `<div class="col-md-4">`;
        //     html +=             `<input class="form-control shadow-sm" type="file" name="file[]">`;
        //     html +=         `</div>`;
        //     html +=         `<div class="col-md-1">`;
        //     html +=             `<button type="button" id="hapusDoc" class="btn btn-danger hapus" title="Delete Document"><i class="fa fa-trash"></i></button>`;
        //     html +=         `</div>`;
        //     html +=     `</div>`;

        //     $('#docField').append(html);
        // }

        // $('body').on('click', '.hapus', function() {
        //     $(this).parents('#field').remove();
        // });

        $(function() {
        $('#only-number').on('keydown', '.number', function(e){
            -1!==$
            .inArray(e.keyCode,[46,8,9,27,13,110,190]) || /65|67|86|88/
            .test(e.keyCode) && (!0 === e.ctrlKey || !0 === e.metaKey)
            || 35 <= e.keyCode && 40 >= e.keyCode || (e.shiftKey|| 48 > e.keyCode || 57 < e.keyCode)
            && (96 > e.keyCode || 105 < e.keyCode) && e.preventDefault()
        });
        })
    </script>
    </body>
</html>

