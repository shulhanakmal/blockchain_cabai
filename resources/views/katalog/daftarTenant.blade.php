<!DOCTYPE html>
<html lang="en">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
    <title>B2B Platform</title>
    <!-- Icons-->
    <link href="{{ asset('assets/node_modules/@coreui/icons/css/coreui-icons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/node_modules/flag-icon-css/css/flag-icon.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/node_modules/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/node_modules/simple-line-icons/css/simple-line-icons.css') }}" rel="stylesheet">
    <!-- Main styles for this application-->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/pace-progress/css/pace.min.css') }}" rel="stylesheet">
    {{-- select2 --}}
    <link href="{{ asset('assets/node_modules/select2/dist/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/node_modules/select2/dist/css/select-2.min.css') }}" rel="stylesheet">
  </head>
  <body class="app flex-row align-items-center" style="background-color: white">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-12">
          <div class="card mx-8 shadow-lg bg-light rounded">
            <form action="{{route('daftar.tenant.store')}}" method="POST" enctype="multipart/form-data">@csrf
                <div class="card-body">
                    <h1>B2B Platform Tenant</h1>
                    <p class="text-muted">Please fill in the registration form below</p>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label class="label"><i style="color: red;">*</i>Tenant</label>
                            <input class="form-control shadow-sm" type="text" name="tenant" required>
                        </div>
                        <div class="col-md-6">
                            <label class="label"><i style="color: red;">*</i>Email</label>
                            <input class="form-control shadow-sm" type="email" name="email" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label class="label">Contact Person</label>
                            <input class="form-control shadow-sm" type="text" name="firstName">
                        </div>
                        <div class="col-md-6">
                            <label class="label">Company Name</label>
                            <input class="form-control shadow-sm" type="text" name="LastName">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label class="label"><i style="color: red;">*</i>Currency</label>
                            <select name="currency" id="currency" class="form-control select2" onchange="CurrencyChange()" required>
                                <option value="">Choose Currency</option>
                                @foreach($currency as $c)
                                    <option value="{{$c->code}}">{{$c->name}} - {{$c->code}}</option>
                                @endforeach
                                <option value="Otr">Other</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label class="col-md-2 col-form-label" for="currencyOtr"></label>
                            <input type="text" name="currencyOtr" id="fieldCurr" style="display: none; text-transform:uppercase;" maxlength="5" class="form-control" value="" placeholder="Masukan Kode Currency">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label class="label"><i style="color: red;">*</i>Round Price</label>
                            <select name="round_price" id="round_price" class="form-control select2"required>
                                <option></option>
                                <option value="10">10</option>
                                <option value="100">100</option>
                                <option value="1000">1000</option>
                                <option value="10000">10000</option>
                                <option value="100000">100000</option>
                                <option value="1000000">1000000</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label class="label"><i style="color: red;">*</i>Username</label>
                            <input class="form-control shadow-sm" type="text" name="username" required>
                        </div>
                        <div class="col-md-6">
                            <label class="label"><i style="color: red;">*</i>Logo</label>
                            <input class="form-control shadow-sm" type="file" name="file" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label class="label"><i style="color: red;">*</i>Password</label>
                            <input class="form-control shadow-sm" type="password" name="passowrd" id="p1" required>
                        </div>
                        <div class="col-md-6">
                            <label class="label"><i style="color: red;">*</i>Repeat Password</label>
                            <input class="form-control shadow-sm" type="password" id="p2" required>
                        </div>
                    </div>
                </div>
                <div class="card-footer bg-secondary">
                    <button class="btn btn-success float-right" type="submit">Create Account</button>
                    <a class="btn btn-light" href="{{route('guest.index')}}" type="button">Back To Home</a>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- CoreUI and necessary plugins-->
    <script src="{{ asset('assets/node_modules/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/node_modules/popper.js/dist/umd/popper.min.js') }}"></script>
    <script src="{{ asset('assets/node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/node_modules/pace-progress/pace.min.js') }}"></script>
    <script src="{{ asset('assets/node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js') }}"></script>
    <script src="{{ asset('assets/node_modules/@coreui/coreui-pro/dist/js/coreui.min.js') }}"></script>
  </body>
  <script src="{{ asset('assets/node_modules/select2/dist/js/select2.min.js') }}"></script>
  <script>
    $('#currency').select2({
        allowClear: false,
        theme: 'bootstrap',
        width: "100%",
        placeholder: "Choose Currency",
    });

    $('#round_price').select2({
        allowClear: false,
        theme: 'bootstrap',
        width: "100%",
        placeholder: "Choose Round Price",
    });

    function CurrencyChange() {
        currency = document.getElementById('currency').value;
        if(currency === "Otr"){
            $('#fieldCurr').prop('required',true);
            $('#fieldCurr').show();
        } else {
            $('#fieldCurr').prop('required',false);
            $('#fieldCurr').hide();
        }
    }
  </script>
</html>

