<footer>
    <div class="footer layout2 layout3">
        <div class="container">
            <div class="footer-note layout2">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8 left-content">
                        <div class="coppy-right">
                            <h3 class="content">© Powered by <span class="site-name"> <a href="http://www.inamart.co.id" target="_blank">PT Inamart Sukses Jaya</a></span></h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

{{-- <footer>
    <div class="footer layout1 ">
        <div class="container">
            <div class="main-footer">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8">
                        <div class="coppy-right">
                            <h3 class="content">© Powered by <span class="site-name"> <a href="www.inamart.co.id" target="_blank">PT Inamart Sukses Jaya</a></span></h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer> --}}


<div class="modal modal-loading fade bd-example-modal-lg" data-backdrop="static" data-keyboard="false" tabindex="-1">
	<div class="modal-dialog modal-sm"><center>
		<div class="modal-content" style="width:75px;color:#1d66ad;background-color:white;padding:15px">
			<span class="fa fa-refresh fa-spin fa-3x"></span>
		</div>
	</div>
</div>
<div class="modal fade" id="assign-modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-primary modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Detail</h4>
            </div>
			<div class="modal-body">
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-danger" type="button" data-dismiss="modal">Tutup</button>
			</div>
        </div>
    </div>
</div>
<div class="modal fade" id="keranjang-modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" tabindex="-1">
    <div class="modal-dialog modal-primary modal-lg">
        <div class="modal-content">
            <div class="modal-header">
            </div>
			<div class="modal-body">
			</div>
			<div class="modal-footer">
				{{-- <button class="btn btn-sm btn-danger" type="button" data-dismiss="modal">Lanjutkan Belanja</button>
				<a class="btn btn-sm btn-danger" href="{{ route('katalog.cart.index', [Auth::user()->group()->first()->code, Illuminate\Support\Facades\Crypt::encryptString('cart')]) }}">Beli</a> --}}
			</div>
        </div>
    </div>
</div>
<div class="modal fade" id="perbandingan-modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-primary modal-lg">
        <div class="modal-content">
            <div class="modal-header">
            </div>
			<div class="modal-body">
			</div>
			<div class="modal-footer">
				{{-- <button class="btn btn-sm btn-danger" type="button" data-dismiss="modal">Lanjutkan Belanja</button>
				<a class="btn btn-sm btn-danger" href="{{ route('katalog.comparison.index', [Auth::user()->group()->first()->code, Illuminate\Support\Facades\Crypt::encryptString('comparison')]) }}">Lihat</a> --}}
			</div>
        </div>
    </div>
</div>
<div class="modal fade" id="detail-checkout-modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-primary modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Detail Pemesanan</h4>
            </div>
			<div class="modal-body">
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-danger" type="button" data-dismiss="modal">Tutup</button>
			</div>
        </div>
    </div>
</div>
<div class="modal fade" id="detail-order-modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-primary modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Detail Pemesanan</h4>
            </div>
			<div class="modal-body">
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-danger" type="button" data-dismiss="modal">Tutup</button>
				<a class="btn btn-sm btn-danger" onclick="checkout()">Checkout</a>
			</div>
        </div>
    </div>
</div>
<div class="modal fade" id="catatan-customer-modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-primary modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Catatan</h4>
            </div>
            <div class="modal-body" >
                <div class="form-group row">
                    <input type="hidden" name="order_number" id="order_number"/>
                </div>
                <div id="body-catatan">
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-danger pull-left" type="button" data-dismiss="modal">Tutup</button>
                <button class="btn btn-sm btn-primary pull-right" type="button" onclick="checkCatatanCustomer()">Kirim</button>
            </div>
        </div>
    </div>
</div>
