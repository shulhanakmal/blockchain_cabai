<html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>B2B Platform</title>
        <link href="{{ asset('techone/images/inamartbar.png') }}" rel="icon">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link href="{{ asset('assets/ample/plugins/bower_components/bootstrap/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('techone/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('assets/ample/plugins/bower_components/ionicons.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/ample/plugins/bower_components/AdminLTE.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/ample/plugins/bower_components/blue.css') }}" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
        @toastr_css
    </head>

    <body class="hold-transition login-page" style="background-color: white;">
        <div class="login-box-body" style="height:525px; width:553; background-image: url('{{ asset('images/background/login-form06.png') }}'); background-size: cover;">
            <br>
            <br>
            <br>
            <br>
            <br>
            <h3 class="login-box-msg">Login {{isset($url) ? $url : 'customer'}}</h3>
            <br>
            <br>
            @if ($message = Session::get('error'))
                <div class="flex-sb-m w-full p-t-3 p-b-32">
                    <p style="color:red;">{{ $message }}</p>
                </div>
            @endif
            @isset($url)
                <form method="POST" action="{{ route("login.post.$url") }}">
            @else
                <form method="POST" action="{{ route('login.post.customer') }}">
            @endisset
                {{ csrf_field() }}
                <div class="form-group has-feedback">
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    <input id="email" type="text" placeholder="Email" class="form-control" name="email" oninvalid="this.setCustomValidity('Masukkan Username')" required >
                </div>
                <div class="form-group has-feedback">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    <input id="password" type="password" placeholder="Password" class="form-control" name="password" oninvalid="this.setCustomValidity('Masukkan Password')" required >
                </div>
                <br><br>
                <br><br>
                <div class="row">
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">
                            Sign In
                        </button>
                        @if(!isset($url))
                            <a href="{{route('regcode')}}" class="btn btn-info btn-block btn-flat">
                                sign in with registration code
                            </a>
                        @endif
                        <a href="{{route('guest.index')}}" class="btn btn-block btn-flat">
                            Back To Home
                        </a>
                    </div>
                </div>
            </form>
            <br><br>
        </div>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
        @toastr_js
        @toastr_render
        <script>
            @foreach ($errors->all() as $error)
                toastr.error("{{$error}}")
            @endforeach
        </script>
    </body>
</html>
