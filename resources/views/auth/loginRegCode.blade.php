<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>B2B Platform</title>
        <link href="{{ asset('techone/images/inamartbar.png') }}" rel="icon">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link href="{{ asset('assets/ample/plugins/bower_components/bootstrap/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('techone/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('assets/ample/plugins/bower_components/ionicons.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/ample/plugins/bower_components/AdminLTE.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/ample/plugins/bower_components/blue.css') }}" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css" rel="stylesheet">

        {{-- <link href="{{ asset('assets/node_modules/@coreui/icons/css/coreui-icons.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/node_modules/flag-icon-css/css/flag-icon.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/node_modules/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/node_modules/simple-line-icons/css/simple-line-icons.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/vendors/pace-progress/css/pace.min.css') }}" rel="stylesheet"> --}}

        <link href="{{ asset('assets/node_modules/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
        @toastr_css
        <style>
            .swal-wide-register {
                width: 600px;
                background-color: transparent;
                background: transparent;
            }
        </style>
    </head>

    <body class="hold-transition login-page" style="background-color: white;">
        <div class="login-box-body" style="height:525px; width:553; background-image: url('{{ asset('images/background/login-form06.png') }}'); background-size: cover;">
            <br>
            <br>
            <br>
            <br>
            <br>
            <h3 class="login-box-msg">Sign in with registration code</h3>
            <br>
            <br>
            @if ($message = Session::get('error'))
                <div class="flex-sb-m w-full p-t-3 p-b-32">
                    <p style="color:red;">{{ $message }}</p>
                </div>
            @endif
                <form method="POST" action="{{ route("login.regcode") }}">
                {{ csrf_field() }}
                <div class="form-group has-feedback">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    <input id="code" type="password" placeholder="Enter Registration Code" class="form-control" name="code" oninvalid="this.setCustomValidity('Enter Registration Code')" required >
                </div>
                <br><br>
                <br><br>
                <div class="row">
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">
                            Sign In
                        </button>
                        <a href="{{route('guest.index')}}" class="btn btn-block btn-flat">
                            Back To Home
                        </a>
                    </div>
                </div>
            </form>
            <br><br>
        </div>

        <script src="{{ asset('assets/node_modules/jquery/dist/jquery.min.js') }}"></script>
        <script src="{{ asset('assets/node_modules/popper.js/dist/umd/popper.min.js') }}"></script>
        <script src="{{ asset('assets/node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/node_modules/pace-progress/pace.min.js') }}"></script>
        <script src="{{ asset('assets/node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js') }}"></script>
        <script src="{{ asset('assets/node_modules/@coreui/coreui-pro/dist/js/coreui.min.js') }}"></script>
        <script src="{{ asset('assets/node_modules/sweetalert2/dist/sweetalert2.min.js') }}"></script>
        @toastr_js
        @toastr_render
        <script>
            @foreach ($errors->all() as $error)
                toastr.error("{{$error}}")
            @endforeach

            @if(session()->has('message'))
                Swal.fire({
                    customClass: 'swal-wide-register',
                    html: `	<div style="z-index:9999999;padding:0;margin:0;">
                                <div class="modal-dialog modal-secondary" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Attention</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>
                                                {{ session()->get('message') }}
                                            </p>
                                        </div>
                                        <div class="modal-footer">
                                            <button id="tutupSwal" class="btn btn-success float-right" style="color:white;">Continue</button>
                                        </div>
                                    </div>
                                </div>
                            </div>`,
                    onOpen: function() {
                        $('#tutupSwal').click(function(){
                            swal.close();
                            window.location.href="{{ route('index.regcode', Illuminate\Support\Facades\Crypt::encryptString(session()->get('reg_code'))) }}"
                        });
                    },
                    showCancelButton: false,
                    showConfirmButton: false
                })
            @endif
        </script>
    </body>
</html>
