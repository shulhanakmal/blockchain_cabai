<!DOCTYPE html>
<html lang="en">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
    <title>B2B Platform</title>
    <!-- Icons-->
    <link href="{{ asset('assets/node_modules/@coreui/icons/css/coreui-icons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/node_modules/flag-icon-css/css/flag-icon.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/node_modules/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/node_modules/simple-line-icons/css/simple-line-icons.css') }}" rel="stylesheet">
    <!-- Main styles for this application-->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/pace-progress/css/pace.min.css') }}" rel="stylesheet">
    {{-- select2 --}}
    <link href="{{ asset('assets/node_modules/select2/dist/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/node_modules/select2/dist/css/select-2.min.css') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
    @toastr_css
    <style>
        .label{
            padding-top: 7px;
        }
    </style>
  </head>
  <body class="app flex-row align-items-center" style="background-color: white">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-12"><br><br>

            <div class="card shadow-lg bg-light rounded">
                <form action="{{route('store.regcode')}}" method="POST" enctype="multipart/form-data">@csrf
                    <input type="hidden" name="id_customer" value="{{$data->id}}">
                    <div class="card-body">
                        <h1>B2B Platform Buyer</h1>
                        <p class="text-muted">Please fill in the registration form below</p>
                        <hr>
                        <div class="form-group row">
                            <div class="col-md-6" id="only-number">
                                <label class="label">Roasting Capacity per Month<i class="text-danger">(*)</i></label>
                                <input class="form-control shadow-sm number" type="text" name="roastingCapacity" placeholder="-Kg" required>
                            </div>
                        </div>
                        <hr>
                        <label class="label"><h5>Legal Document(Optional)</h5></label>
                        <div id="docField">
                            <div class="form-group row" id="field">
                                <label class="label col-md-2">Document Name</label>
                                <div class="col-md-4">
                                    <input class="form-control shadow-sm" type="text" name="docName[]">
                                </div>
                                <label class="label col-md-1">File</label>
                                <div class="col-md-4">
                                    <input class="form-control shadow-sm" type="file" name="file[]">
                                </div>
                                <div class="col-md-1">
                                    <button type="button" id="tambahDokumen" onclick="tambahDoc()" class="btn btn-primary" title="Add Document"><i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer">
                        <button class="btn btn-primary float-right" type="submit">
                            Save
                        </button>
                        <a class="btn btn-danger" href="{{ route('guest.index') }}">Logout</a>
                    </div>
                </form>
            </div>
        </div>
      </div>
    </div>
    <!-- CoreUI and necessary plugins-->
    <script src="{{ asset('assets/node_modules/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/node_modules/popper.js/dist/umd/popper.min.js') }}"></script>
    <script src="{{ asset('assets/node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/node_modules/pace-progress/pace.min.js') }}"></script>
    <script src="{{ asset('assets/node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js') }}"></script>
    <script src="{{ asset('assets/node_modules/@coreui/coreui-pro/dist/js/coreui.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    @toastr_js
    @toastr_render
    <script src="{{ asset('assets/node_modules/select2/dist/js/select2.min.js') }}"></script>
    <script>
        @foreach ($errors->all() as $error)
            toastr.error("{{$error}}")
        @endforeach

        function tambahDoc() {
            var html = '';
            
            html +=     `<div class="form-group row" id="field">`;
            html +=         `<label class="label col-md-2">Document Name</label>`;
            html +=         `<div class="col-md-4">`;
            html +=             `<input class="form-control shadow-sm" type="text" name="docName[]">`;
            html +=         `</div>`;
            html +=         `<label class="label col-md-1">File</label>`;
            html +=         `<div class="col-md-4">`;
            html +=             `<input class="form-control shadow-sm" type="file" name="file[]">`;
            html +=         `</div>`;
            html +=         `<div class="col-md-1">`;
            html +=             `<button type="button" id="hapusDoc" class="btn btn-danger hapus" title="Delete Document"><i class="fa fa-trash"></i></button>`;
            html +=         `</div>`;
            html +=     `</div>`;

            $('#docField').append(html);
        }

        $('body').on('click', '.hapus', function() {
            $(this).parents('#field').remove();
        });
    </script>
    </body>
</html>

