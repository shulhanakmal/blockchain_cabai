<?php

namespace Modules\Vendor\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Vendor\Http\Controllers\LayoutController;
use Illuminate\Http\Request;
use Response;
use Auth;
use DB;
use App\Category\Category;
use App\Product\Product;
use App\Vendor\vendor;
use App\Vendor\VendorAddress;
use App\Vendor\VendorMaster;
use App\Vendor\VendorAddressMaster;
use App\Vendor\VendorCodeMaster;
use App\Vendor\VendorContractMaster;
use App\Product\ProductVendor;
use App\Product\ProductContract;
use App\Product\ProductShipping;
use App\Category\CategoryProduct;
use App\Lokasi\Location;
use App\Product\ProductGroup;
use App\Product\ProductGroupRule;
use App\Group\group;
use App\Style\style;
use App\MCurrencies;
use App\Batch;
use App\Unit;
use QrCode;
use App\ImageProduct;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Input;
use File;

class ProductContractController extends LayoutController {
    function __construct() {
        $this->middleware('permission:vendor', ['only' => ['index']]);
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function contract(Request $request, $code, $hash) {
        $contracts = Crypt::decryptString($hash);
        if($contracts == 'product_contract')
        DB::setDefaultConnection('mysql');
        $vendor_contract_master = VendorContractMaster::with(['vendor_master' => function($vm) {
                $vm->where('id', $this->user->id)
                ->orderBy('created_at', 'desc');
            }])
            ->has('vendor_master')
            ->whereHas('vendor_master', function ($vm){
                $vm->where('id', $this->user->id)
                ->orderBy('created_at', 'desc');
            })->orderBy('created_at', 'desc');
        if(isset($request->showAll)) {
            $pagePaginate = $vendor_contract_master->count();
            return view('vendor::product.contract.index',[
                'vendor_contract_master' => $vendor_contract_master->paginate($pagePaginate),
                'style' => $this->user->group->style
            ])
            ->with('i', ($request->input('page', 1) - 1) * $pagePaginate);
        } else {
            return view('vendor::product.contract.index',[
                'vendor_contract_master' => $vendor_contract_master->paginate(10),
                'style' => $this->user->group->style
            ])
            ->with('i', ($request->input('page', 1) - 1) * 10);
        }
    }

    public function detail(Request $request, $code, $hash) {
        $id = Crypt::decryptString($hash);
        DB::setDefaultConnection('mysql');
        $vendor_contract_master = VendorContractMaster::with(['vendor_master' => function($vm) {
                $vm->where('id', $this->user->id)
                ->orderBy('created_at', 'desc');
            }])
            ->has('vendor_master')
            ->whereHas('vendor_master', function ($vm){
                $vm->where('id', $this->user->id)
                ->orderBy('created_at', 'desc');
            })
            ->where('id', $id)
            ->first();

        return view('vendor::product.contract.detail',[
                'vendor_contract_master' => $vendor_contract_master,
                'style' => $this->user->group->style
            ]);

    }

    public function product_create(Request $request, $code, $hash) {

        $id = Crypt::decryptString($hash);

        DB::setDefaultConnection('mysql');
        $vendor_contract_master = VendorContractMaster::find($id);

        $product_group_rules = new ProductGroupRule();
        $product_group_rules = $product_group_rules->setConnection($this->user->group->katalog);
        $product_group_rules = $product_group_rules->get();
        $currency = MCurrencies::where('status', 1)->where('flag', 1)->orderBy('code', 'asc')->get();
        $defaultCurrency = $this->user->group->currency;

        $batch = new Batch;
        $batch = $batch->setConnection($this->user->group->katalog);
        $batch = $batch->get();

        $unit = new Unit;
        $unit = $unit->setConnection($this->user->group->katalog);
        $unit = $unit->get();

        return view('vendor::product.contract.add', [
            'vendor_contract_master' => $vendor_contract_master,
            'category' => $this->get_category($this->user->group->katalog, $vendor_contract_master->category),
            'product_group_rules' => $product_group_rules,
            'defaultCurrency' => $defaultCurrency,
            'currency' => $currency,
            'batch' => $batch,
            'unit' => $unit,
            'style' => $this->user->group->style
        ]);
    }

    public function product_create_post(Request $request, $code, $id) {
        $this->validateForm($request, 'create');
        $product = new Product();
        $product = $product->setConnection($this->user->group->katalog);
        $product = $this->saveProduct($request, $product);

        // insert product group
        if($request->product_group){
            foreach($request->product_group as $group){
                $pg = new ProductGroup();
                $pg->setConnection($this->user->group->katalog);
                $pg->rule_id = $group;
                $pg->product_id = $product->id;
                $pg->save();
            }
        }

        $product_contract = new ProductContract();
        $product_contract = $product_contract->setConnection($this->user->group->katalog);
        $product_contract = $this->saveProductContract($request, $product_contract, $product);

        return redirect()->route('vendor.product.contract',[$this->user->group->code, Crypt::encryptString('product_contract')]);
    }

    public function validateForm($request, $type) {

        $messages = [
            'batch.required' => 'Choose batch!',
            'product_group.required' => 'Choose product group!',
            'name.required' => 'Enter name!',
            'description.required' => 'Enter description!',
            'short_description.required' => 'Enter short description!',
            'currency.required' => 'Choose currency!',
            'price.required' => 'Enter price!',
            'stock.required' => 'Enter stock!',
            'unit.required' => 'Choose unit!',
            'sku.required' => 'Enter SKU!',
            'vendor_sku.required' => 'Enter Seller SKU!',
            'subcategory.required' => 'Choose unit!',
            'weight.required' => 'Enter weight!',
            'tgl_produksi.required' => 'Enter production date!',
        ];

        $this->validate($request,[
            'batch' => 'required',
            'product_group' => 'required',
            'name' => 'required',
            'description' => 'required',
            'short_description' => 'required',
            'currency' => 'required',
            'price' => 'required',
            'stock' => 'required',
            'unit' => 'required',
            'sku' => 'required',
            'vendor_sku' => 'required',
            'subcategory' => 'required',
            'weight' => 'required',
            'tgl_produksi' => 'required',
        ],$messages);

        if($type == 'create') {
            return view('vendor::product.contract.add', [
                'data' => $request
            ]);
        }
    }

    public function saveProduct($request, $product) {

        if($request->currency == 'Otr') {
            $simpanCurr = new MCurrencies;
            $simpanCurr->name = "Lainnya";
            $simpanCurr->code = $request->currencyOtr;
            $simpanCurr->status = 1;
            $simpanCurr->flag = 1;
            $simpanCurr->save();
        }

        $product->name              = $request->name;
        $product->description       = $request->description;
        $product->short_description = $request->short_description;
        $product->price             = str_replace(',', '', $request->price);
        if($request->currency == 'Otr') {
            $product->currency          = $request->simpanCurr;
        } else {
            $product->currency          = $request->currency;
        }
        $product->unit              = $request->unit;
        $product->sku               = $request->sku;
        $product->vendor_sku        = $request->vendor_sku;

        if ($request->hasFile('files')) {
            $files = $request->file('files');
            foreach($files as $file) {
                $name=$file->getClientOriginalName();
                $file->move(public_path("images/product/") . $product->id . '/', $name);
                $image            = new ImageProduct();
                $image            = $image->setConnection($this->user->group->katalog);
                $image->images    = "images/product/" . $product->id . '/' . $name;
                $image->productID = $product->id;
                $image->save();
            }
        }

        $product->status       = 1;
        $product->batch_id     = $request->batch;
        $product->tgl_produksi = $request->tgl_produksi;
        $product->weight       = $request->weight;
        $product->save();

        if(!file_exists(public_path("images/product/") . $this->user->group->katalog . '/' . $this->user->id . '/image/' . $product->id))
        File::makeDirectory(public_path("images/product/") . $this->user->group->katalog . '/' . $this->user->id . '/image/' . $product->id,0777,true);
        QrCode::generate(env('REACT_URL') . '/detailProduk/' . $product->id, public_path("images/product/") . $this->user->group->katalog . '/' . $this->user->id . '/image/' . $product->id . '/' . 'qrcode' . $product->id . '.svg');
        $product->image = public_path("images/product/") . $this->user->group->katalog . '/' . $this->user->id . '/image/' . $product->id . '/' . 'qrcode' . $product->id . '.svg';
        $product->qrcode  = "images/product/" . $this->user->group->katalog . '/' . $this->user->id . '/image/' . $product->id . '/' . 'qrcode' . $product->id . '.svg';

        if ($request->hasFile('files_gambar')) {
            $product->small_image = $request->file('files_gambar')->store('public/files');
            $files                = $request->file('files_gambar');
            $files->move(public_path("images/product/") . $this->user->group->katalog . '/' . $this->user->id . '/image/' . $product->id, $files->getClientOriginalName() . '.' . $files->getClientOriginalExtension());
            $product->thumbnail   = "images/product/" . $this->user->group->katalog . '/' . $this->user->id . '/image/' . $product->id . '/' . $files->getClientOriginalName() . '.' . $files->getClientOriginalExtension();
        }
        $product->save();

        $product_vendor = new ProductVendor();
        $product_vendor = $product_vendor->setConnection($this->user->group->katalog);
        $product_vendor->vendor_id = $this->user->vendor_katalog_id;
        $product_vendor->product_id = $product->id;
        $product_vendor->price = str_replace(',', '', $product->price);
        $product_vendor->stock = str_replace(',', '', $request->stock);
        $product_vendor->status = 1;
        if($request->currency == 'Otr') {
            $product_vendor->currency = $request->currencyOtr;
        } else {
            $product_vendor->currency = $request->currency;
        }
        $product_vendor->save();

        return $product;
    }

    public function saveProductContract($request, $product_contract, $product) {

        $newcontract_start = date("Y-m-d H:i:s", strtotime($request->contract_start));
        $newcontract_end = date("Y-m-d H:i:s", strtotime($request->contract_end));

        $product_contract->vendor_id = $this->user->vendor_katalog_id;
        $product_contract->vendor_master_id = $request->vendor_master_id;
        $product_contract->vendor_contract_master_id = $request->vendor_contract_master_id;
        $product_contract->product_id = $product->id;
        $product_contract->contract_name = $request->contract_name;
        $product_contract->contract_no = $request->contract_no;
        $product_contract->contract_price = str_replace(',', '', $request->contract_price);
        $product_contract->contract_start = $newcontract_start;
        $product_contract->contract_end = $newcontract_end;
        $product_contract->category = $request->category;
        $product_contract->status = 0;
        $product_contract->created_at = date("Y-m-d H:i:s");
        $product_contract->updated_at = date("Y-m-d H:i:s");
        $product_contract->save();

        if($request->has('name_biaya') && $request->has('price_biaya')) {
            foreach($request->name_biaya as $index => $nb) {
                $product_shipping = new ProductShipping();
                $product_shipping = $product_shipping->setConnection($this->user->group->katalog);
                $product_shipping->product_id = $product->id;
                $product_shipping->name =  $nb;
                $product_shipping->price =  str_replace(',', '', $request->price_biaya[$index]);
                $product_shipping->created_at = date("Y-m-d H:i:s");
                $product_shipping->updated_at = date("Y-m-d H:i:s");
                $product_shipping->save();
            }
        }
        $category_product = new CategoryProduct();
        $category_product = $category_product->setConnection($this->user->group->katalog);
        $category_product->category_id = $request->category;
        $category_product->product_id = $product->id;
        $category_product->save();

        $category_product = new CategoryProduct();
        $category_product = $category_product->setConnection($this->user->group->katalog);
        $category_product->category_id = $request->subcategory;
        $category_product->product_id = $product->id;
        $category_product->save();

        if(isset($request->subsubcategory)){
            $category_product = new CategoryProduct();
            $category_product = $category_product->setConnection($this->user->group->katalog);
            $category_product->category_id = $request->subsubcategory;
            $category_product->product_id = $product->id;
            $category_product->save();
        }

        return $product_contract;
    }

    public function subcategory(Request $request, $code) {
        $sub_sub_category = new \App\Category\Category;
        $sub_sub_category = $sub_sub_category->setConnection($this->user->group->katalog);
        $sub_sub_category = $sub_sub_category->where("parent_id", $request->id)->where("level", 4)->where("status", 1)->get();
        return json_encode($sub_sub_category);
    }
}
