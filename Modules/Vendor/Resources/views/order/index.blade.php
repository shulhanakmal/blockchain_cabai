@extends('vendor::layouts.app')
@section('content')
<ol class="breadcrumb">
    <li class="breadcrumb-item">Order List</li>
</ol>
<div class="container-fluid" style="margin-top:16px;">
    <div class="animated fadeIn">
        <div class="card">
            <div class="card-body">
                <div class="white-box">
                    <div class="row">
                        <div class="col-md-12">
                            <form class="form-inline pull-right" method="get">
                                <input class="form-control mr-sm-2" type="text" name="q" placeholder="Search..">
                                <button class="btn btn-primary my-2 my-sm-0" type="submit">Search</button>
                            </form>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="tabel-order" class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center" style="vertical-align:middle">#</th>
                                    <th class="text-center" style="vertical-align:middle">Order Number</th>
                                    <th class="text-center" style="vertical-align:middle">Order Name</th>
                                    <th class="text-center" style="vertical-align:middle">Order From</th>
                                    <th class="text-center" style="vertical-align:middle">Status</th>
                                    <th class="text-center" style="vertical-align:middle">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($order as $o)
                                <tr>
                                    <td class="text-center">{{ ++$i }}</td>
                                    <td class="text-left">{{ $o->order_number }}</td>
                                    <td class="text-left">{{ $o->customer->first_name }} {{ $o->customer->last_name }}</td>
                                    <td class="text-left">{{ $o->group->name }}</td>
                                    <td class="text-left">
                                        {{-- {{ $o->order_status->name }} --}}
                                        {{ get_current_state($o) }}
                                    </td>
                                    <td class="text-center" style="vertical-align:middle">
                                        <a class="btn btn-outline-primary" title="Detail" href="{{ route('vendor.order.detail', [Auth()->user()->group->code, Illuminate\Support\Facades\Crypt::encryptString($o->id)]) }}">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        {{-- <a class="btn btn-sm btn-danger hover">
                                            <i class="fa fa-trash-o ihover" style="color:white"><span class='span'>&nbsp;Hapus</span></i>
                                        </a> --}}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {!! $order->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    @foreach ($errors->all() as $error)
        toastr.error("{{$error}}")
    @endforeach
    $(document).ready(function() {
        $('#tabel-order .btn').tooltip();
    })

    var table_order = null;
    table_order =  $('#tabel-order').DataTable({
        lengthChange: false,
        searching: false,
        paging:   false,
        ordering: true,
        info:     false
    })
</script>
@endpush
