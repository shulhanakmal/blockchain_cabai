@extends('vendor::layouts.app')
@section('content')
<ol class="breadcrumb">
    <li class="breadcrumb-item">Product List</li>
</ol>
<div class="container-fluid" style="margin-top:16px;">
    <div class="animated fadeIn">
        <div class="card">
            <div class="card-body">
                <div class="white-box">
                    <div class="row">
                        <div class="col-md-12">
                            <form class="form-inline pull-right" method="get">
                                <input class="form-control mr-sm-2" type="text" name="q" placeholder="Search..">
                                <button class="btn btn-primary my-2 my-sm-0" type="submit">Search</button>
                            </form>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="tabel-product" class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center" style="vertical-align:middle">No</th>
                                    <th class="text-center" style="vertical-align:middle">Name</th>
                                    <th class="text-center" style="vertical-align:middle">Price</th>
                                    <th class="text-center" style="vertical-align:middle">Stock</th>
                                    <th class="text-center" style="vertical-align:middle">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($product as $key => $p)
                                <tr>
                                    <td class="text-center">{{ ++$i }}</td>
                                    <td class="text-left">{{ $p->product->name }}</td>
                                    <td class="text-right">{{ number_format($p->price, 0) }}</td>
                                    <td class="text-center">{{ number_format($p->stock, 0) }}</td>
                                    <td class="text-center" style="vertical-align:middle">
                                        {{-- <a class="btn btn-outline-primary" title="Detail" href="{{ route('vendor.product.detail', [Auth::user()->group->code, Crypt::encryptString($p->product_id)]) }}">
                                            <i class="fa fa-eye"></i>
                                        </a> --}}
                                        <a class="btn btn-outline-primary" title="Detail" href="{{ route('vendor.product.edit', [Auth::user()->group->code, Crypt::encryptString($p->product_id)]) }}">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        {{-- <a class="btn btn-sm btn-danger hover">
                                            <i class="fa fa-trash-o ihover" style="color:white"><span class='span'>&nbsp;Hapus</span></i>
                                        </a> --}}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {!! $product->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    @foreach ($errors->all() as $error)
        toastr.error("{{$error}}")
    @endforeach
    $(document).ready(function() {
        $('#tabel-product .btn').tooltip();
    })

    var table_product = null;
    table_product =  $('#tabel-product').DataTable({
        lengthChange: false,
        searching: false,
        paging:   false,
        ordering: true,
        info:     false
    })
</script>
@endpush
