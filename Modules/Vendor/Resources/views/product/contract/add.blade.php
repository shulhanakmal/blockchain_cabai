@extends('vendor::layouts.app')
@section('content')
<ol class="breadcrumb">
    <li class="breadcrumb-item">Add Product</li>
</ol>
<div class="container-fluid" style="margin-top:16px;">
    <div class="animated fadeIn">
        <form id="forms" method="post" action="{{ route('vendor.product.contract.add_product_post', [Auth::user()->group->code, $vendor_contract_master->id]) }}" enctype="multipart/form-data" role="form">
            <div class="card">
                {{ csrf_field() }}
                <input class="form-control" id="vendor_master_id" name="vendor_master_id" type="hidden" value="{{ $vendor_contract_master->vendor_master_id }}">
                <input class="form-control" id="vendor_contract_master_id" name="vendor_contract_master_id" type="hidden" value="{{ $vendor_contract_master->id }}">
                <input class="form-control" id="category" name="category" type="hidden" value="{{ $vendor_contract_master->category }}">
                <div class="card-header">
                    <h4>Detail Contract</h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label" for="contract_name">Contract Name</label>
                        <div class="col-md-10">
                            <input class="form-control" id="contract_name" name="contract_name" type="text" value="{{ $vendor_contract_master->contract_name }}" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label" for="contract_no">Contract Number</label>
                        <div class="col-md-4">
                            <input class="form-control" id="contract_no" name="contract_no" type="text" value="{{ $vendor_contract_master->contract_no }}" readonly>
                        </div>
                        <label class="col-md-2 col-form-label text-right" for="contract_price">Contract Price</label>
                        <div class="col-md-4">
                            <input class="form-control" id="contract_price" name="contract_price" type="text" value="{{ number_format($vendor_contract_master->contract_price, 0) }}" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label" for="contract_start">Start Date</label>
                        <div class="col-md-4">
                            <input class="form-control" id="contract_start" name="contract_start" type="text" value="{{ date('d-M-Y', strtotime($vendor_contract_master->contract_start)) }}" readonly>
                        </div>
                        <label class="col-md-2 col-form-label text-right" for="contract_end">End Date</label>
                        <div class="col-md-4">
                            <input class="form-control" id="contract_end" name="contract_end" type="text" value="{{ date('d-M-Y', strtotime($vendor_contract_master->contract_end)) }}" readonly>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h4>
                        Detail Product
                    </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label style="font-weight: bold; font-size: 14px;">Bacth <span style="color: red;">(*)</span></label>
                                <select class="custom-select select2 val-custom form-control @if($errors->has('batch')) is-invalid @endif" name="batch" id="batch" data-placeholder="Choose Batch">
                                    <option></option>
                                    @foreach($batch as $b)
                                    <option value="{{ $b->id }}"
                                        @if(Input::old('batch') != null)
                                            @if(Input::old('batch') == $b->id)
                                                selected
                                            @endif
                                        @endif
                                    >{{ $b->batch_id }}</option>
                                    @endforeach
                                </select>
                                <em id="firstname-error" class="error invalid-feedback">Choose Batch</em>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label style="font-weight: bold; font-size: 14px;">Product Group <span style="color: red;">(*)</span></label>
                                <select class="form-control select2-multiple select2-hidden-accessible @if($errors->has('product_group')) is-invalid @endif" data-placeholder="Choose Product Group" name="product_group[]" id="product_group" multiple="" data-select2-id="select2-2" tabindex="-1" aria-hidden="true">
                                    <option></option>
                                    @foreach($product_group_rules as $pgr)
                                    <option
                                        value="{{ $pgr->id }}""
                                        @if(old("product_group"))
                                            {{ (in_array($pgr->id, old("product_group")) ? "selected":"") }}
                                        @endif
                                    >{{ $pgr->rule_name }}</option>
                                    @endforeach
                                </select>
                                <em id="firstname-error" class="error invalid-feedback">Choose Product Group</em>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label style="font-weight: bold; font-size: 14px;">Product Name <span style="color: red;">(*)</span></label>
                        </div>
                        <div class="form-group col-md-12">
                            <input class="form-control @if($errors->has('name')) is-invalid @endif" id="name" type="text" name="name" placeholder="Product Name" value="{{ old('name') }}">
                            <em id="name-error" class="error invalid-feedback">Enter Product Name</em>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label style="font-weight: bold; font-size: 14px;">Description <span style="color: red;">(*)</span></label>
                        </div>
                        <div class="form-group col-md-12">
                            <input class="form-control @if($errors->has('description')) is-invalid @endif" id="descriptions" type="text" name="description" placeholder="Description" value="{{ old('description') }}">
                            <em id="name-error" class="error invalid-feedback">Enter Description</em>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label style="font-weight: bold; font-size: 14px;">Short Description <span style="color: red;">(*)</span></label>
                        </div>
                        <div class="form-group col-md-12">
                            <input class="form-control @if($errors->has('short_description')) is-invalid @endif" id="short_descriptions" type="text" name="short_description" placeholder="Short Description" value="{{ old('short_description') }}">
                            <em id="name-error" class="error invalid-feedback">Enter Short Description</em>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label style="font-weight: bold; font-size: 14px;">Currency <span style="color: red;">(*)</span></label>
                                <select class="custom-select select2 val-custom form-control @if($errors->has('currency')) is-invalid @endif" name="currency" id="currency" data-placeholder="Choose Currency" onchange="CurrencyChange()">
                                    <option></option>
                                    @foreach($currency as $c)
                                    <option value="{{ $c->id }}"
                                        @if(Input::old('currency') != null)
                                            @if(Input::old('currency') == $c->id)
                                                selected
                                            @endif
                                        @else
                                            @if($c->code == $defaultCurrency) selected @endif
                                        @endif

                                    >{{$c->name}} - {{$c->code}}</option>
                                    @endforeach
                                    <option value="Otr">Other</option>
                                </select>
                                <em id="firstname-error" class="error invalid-feedback">Choose Currency</em>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Price <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input class="form-control @if($errors->has('price')) is-invalid @endif" id="price" type="text" name="price" placeholder="Price" value="{{ old('price') }}" data-type="currency">
                                <em id="name-error" class="error invalid-feedback">Enter Price</em>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" name="currencyOtr" id="fieldCurr" style="display: none; text-transform:uppercase;" maxlength="5" class="form-control" value="" placeholder="Enter Currency Code">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Stock <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input class="form-control @if($errors->has('stock')) is-invalid @endif" id="stock" type="text" name="stock" placeholder="Stock" value="{{ old('stock') }}" data-type="currency">
                                <em id="name-error" class="error invalid-feedback">Enter Stock</em>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label style="font-weight: bold; font-size: 14px;">Unit <span style="color: red;">(*)</span></label>
                                <select class="custom-select select2 val-custom form-control @if($errors->has('unit')) is-invalid @endif" name="unit" id="unit" data-placeholder="Choose Unit">
                                    <option></option>
                                    @foreach($unit as $u)
                                    <option value="{{ $u->id }}"
                                        @if(Input::old('unit') != null)
                                            @if(Input::old('unit') == $u->id)
                                                selected
                                            @endif
                                        @endif
                                    >{{ $u->nama_unit }}</option>
                                    @endforeach
                                </select>
                                <em id="firstname-error" class="error invalid-feedback">Choose Unit</em>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">SKU <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input class="form-control @if($errors->has('sku')) is-invalid @endif" id="sku" type="text" name="sku" placeholder="SKU" value="{{ old('sku') }}">
                                <em id="name-error" class="error invalid-feedback">Enter SKU</em>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Seller SKU <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input class="form-control @if($errors->has('vendor_sku')) is-invalid @endif" id="vendor_sku" type="text" name="vendor_sku" placeholder="Seller SKU" value="{{ old('vendor_sku') }}">
                                <em id="name-error" class="error invalid-feedback">Enter Seller SKU</em>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        @php
                            $sub_category = new \App\Category\Category;
                            $sub_category = $sub_category->setConnection(Auth()->user()->group->katalog);
                            $sub_category = $sub_category->where("parent_id", $category->id)->where("level", 3)->where("status", 1)->get();
                        @endphp
                        <div class="col-md-6">
                            <div class="form-group">
                                <label style="font-weight: bold; font-size: 14px;">Category <span style="color: red;">(*)</span></label>
                                <select class="custom-select select2 val-custom form-control @if($errors->has('category')) is-invalid @endif" name="category" id="category" data-placeholder="Choose Category">
                                    <option selected="" disabled value="{{ $category->id }}">{{ $category->name }}</option>
                                </select>
                                <em id="firstname-error" class="error invalid-feedback">Choose Category</em>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label style="font-weight: bold; font-size: 14px;">Sub Category <span style="color: red;">(*)</span></label>
                                <select class="custom-select select2 val-custom form-control @if($errors->has('subcategory')) is-invalid @endif" name="subcategory" id="subcategory" data-placeholder="Choose Sub Category" onchange="myFunctionSubCategory()">
                                    <option></option>
                                    @foreach($sub_category as $sc)
                                    <option value="{{ $sc->id }}"
                                        @if(Input::old('subcategory') != null)
                                            @if(Input::old('subcategory') == $sc->id)
                                                selected
                                            @endif
                                        @endif
                                    >{{ $sc->name }}</option>
                                    @endforeach
                                </select>
                                <em id="firstname-error" class="error invalid-feedback">Choose Sub Category</em>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Weight <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input class="form-control @if($errors->has('weight')) is-invalid @endif" id="weight" type="text" name="weight" placeholder="Weight" value="{{ old('weight') }}">
                                <em id="name-error" class="error invalid-feedback">Enter Weight</em>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Production Date <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input type="text" autocomplete="off" class="form-control js-datepicker" name="tgl_produksi" id="tgl_produksi">
                                <em id="name-error" class="error invalid-feedback">Enter Production Date</em>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        {{-- <div class="col-md-6">
                            <div class="form-group">
                                <label style="font-weight: bold; font-size: 14px;">Sub Sub Category</label>
                                <div id="idsubsubcategory">
                                    <select class="custom-select select2 val-custom form-control @if($errors->has('subsubcategory')) is-invalid @endif" name="subsubcategory" id="subsubcategory" data-placeholder="Choose Sub Sub Category">
                                        <option></option>
                                    </select>
                                    <em id="firstname-error" class="error invalid-feedback">Choose Sub Sub Category</em>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Process Image <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input class="form-control" id="files" type="file" multiple name="files" placeholder="files" value="{{ old('files') }}" accept="jpg, jpeg, png">
                                <em id="name-error" class="error invalid-feedback">Enter Stock</em>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Product Image <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input class="form-control" id="files_gambar" type="file" name="files_gambar" placeholder="Files" value="{{ old('files_gambar') }}" accept="jpg, jpeg, png">
                                <em id="name-error" class="error invalid-feedback">Enter Stock</em>
                            </div>
                        </div>
                    </div>
                    <br><br>
                    <div class="row">
                        <label class="col-md-2 col-form-label" for="stock">Status <i class="text-danger">*</i></label>
                        <div class="col-xs-10">
                            <div class="input-group">
                                <label class="col-form-label switch switch-md switch-label switch-pill switch-primary">
                                    <input class="switch-input" type="checkbox" id="status_product" name="status_product" onclick="checkBox()">
                                    <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
                                </label>
                                &nbsp;&nbsp;&nbsp;<label id="status_name" name="status_name" class="col-form-label">Inactive</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h4>
                        Shipment
                        <button type="button" id="add_product_shipping" class="btn btn-outline-primary pull-right">
                            <i class="fa fa-plus-square"></i>
                        </button>
                    </h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                        <fieldset class="form-group col-md-12">
                            <table class="table table-bordered col-md-12" id="table_product_shipping">
                                <tbody>
                                </tbody>
                            </table>
                        </fieldset>
                    </div>
                </div>
                <div class="card-footer">
                    <button class="btn btn-primary float-right" type="submit">
                        Save
                    </button>
                    <a class="btn btn-danger" href="{{ route('vendor.product.contract', [Auth()->user()->group->code, Illuminate\Support\Facades\Crypt::encryptString('product_contract')]) }}">Back</a>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/webrtc-adapter/3.3.3/adapter.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.1.10/vue.min.js"></script>
<script type="text/javascript" src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
<script>
    // @foreach ($errors->all() as $error)
    //     toastr.error("{{$error}}")
    // @endforeach

    var description = document.getElementById("description");
    CKEDITOR.replace(description,{
        language:'en-gb'
    });
    var short_description = document.getElementById("short_description");
    CKEDITOR.replace(short_description,{
        language:'en-gb'
    });
    CKEDITOR.config.allowedContent = true;
    CKEDITOR.config.width = '100%';

    function CurrencyChange() {
        currency = document.getElementById('currency').value;
        if(currency === "Otr"){
            $('#fieldCurr').prop('required',true);
            $('#fieldCurr').show();
        } else {
            $('#fieldCurr').prop('required',false);
            $('#fieldCurr').hide();
        }
    }

    function myFunctionSubCategory() {
        subcategory_id = document.getElementById('subcategory').value;
        $.ajax({
            type : 'GET',
            url : '{{ route('vendor.product.subcategory', [Auth::user()->group->code]) }}',
            data : {
                id : subcategory_id
            },
            success:function(response){
                response = JSON.parse(response);
                var html = `<select class="custom-select val-custom form-control select2" data-placeholder="Sub Sub Kategori" name="subsubcategory" id="subsubcategory">
                                <option></option>`;
                for(var i = 0; i < response.length; i++) {
                    html+=`     <option value="` + response[i].id + `">` + response[i].name + `</option>`;
                }
                html+=`     </select>`;

                document.getElementById("idsubsubcategory").innerHTML = html;

                $('.select2').select2({
                    allowClear: false,
                    theme: 'bootstrap',
                    width: "100%"
                });
            }
        });
    }

    function checkBox() {
        if (document.getElementById('status_product').checked) {
            document.getElementById('status_name').innerHTML = "Active";
        document.getElementById('status_product').value = 1;
        } else {
            document.getElementById('status_name').innerHTML = "Inactive";
            document.getElementById('status_product').value = 0;
        }
    }

    $("#add_product_shipping").click(function() {
        var html = '';
        html += '<tr class="tr">';
        html +=     '<td width="47.5%"><input type="text" class="form-control" id="name_biaya[]" name="name_biaya[]" placeholder="Place"></td>';
        html +=     '<td width="47.5%"><input type="text" class="form-control" data-type="currency" id="price_biaya[]" name="price_biaya[]" placeholder="Price"></td>';
        html +=     '<td width="5%"><button type="button" class="btn btn-danger hapus"><i class="fa fa-trash"></i></button></td>';
        html += '</tr>';
        $("#table_product_shipping tbody").append(html);

        $("input[data-type='currency']").on({
            keyup: function() {
                formatCurrency($(this));
            }
        });
    });

    $('body').on('click', '.hapus', function() {
        $(this).parents('.tr').remove();
    });

    $(document).ready(function () {
        $('.js-datepicker').daterangepicker({
            "singleDatePicker": true,
            "showDropdowns": true,
            "autoUpdateInput": false,
            locale: {
                format: 'YYYY-MM-DD'
            },
        });

        var myCalendar = $('.js-datepicker');
        var isClick = 0;

        $(window).on('click',function(){
            isClick = 0;
        });

        $(myCalendar).on('apply.daterangepicker',function(ev, picker){
            isClick = 0;
            $(this).val(picker.startDate.format('YYYY-MM-DD'));

        });

        $('.js-btn-calendar').on('click',function(e){
            e.stopPropagation();

            if(isClick === 1) isClick = 0;
            else if(isClick === 0) isClick = 1;

            if (isClick === 1) {
                myCalendar.focus();
            }
        });

        $(myCalendar).on('click',function(e){
            e.stopPropagation();
            isClick = 1;
        });

        $('.daterangepicker').on('click',function(e){
            e.stopPropagation();
        });

        $('#product_group').select2({
            allowClear: false,
            theme: 'bootstrap',
            width: "100%",
            placeholder: "Pilih Group",
        });
        $(document).on('change', '.btn-file :file', function() {
		var input = $(this),
			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		input.trigger('fileselect', [label]);
		});

		$('.btn-file :file').on('fileselect', function(event, label) {

		    var input = $(this).parents('.input-group').find(':text'),
		        log = label;

		    if( input.length ) {
		        input.val(log);
		    } else {
		        if( log ) alert(log);
		    }

		});
		function readURL(input) {
		    if (input.files && input.files[0]) {
		        var reader = new FileReader();

		        reader.onload = function (e) {
		            $('#img-upload').attr('src', e.target.result);
		        }

		        reader.readAsDataURL(input.files[0]);
		    }
		}

		$("#imgInp").change(function(){
		    readURL(this);
		});

        $(document).on('change', '.btn-file-2 :file', function() {
            var input = $(this), label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [label]);
		});

		$('.btn-file-2 :file').on('fileselect', function(event, label) {

		    var input = $(this).parents('.input-group').find(':text'),
		        log = label;

		    if( input.length ) {
		        input.val(log);
		    } else {
		        if( log ) alert(log);
		    }

		});
		function readURL2(input) {
		    if (input.files && input.files[0]) {
		        var reader = new FileReader();

		        reader.onload = function (e) {
		            $('#img-upload-2').attr('src', e.target.result);
		        }

		        reader.readAsDataURL(input.files[0]);
		    }
		}

		$("#imgInp2").change(function(){
		    readURL2(this);
		});

        $(document).on('change', '.btn-file-3 :file', function() {
            var input = $(this), label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [label]);
		});

		$('.btn-file-3 :file').on('fileselect', function(event, label) {

		    var input = $(this).parents('.input-group').find(':text'),
		        log = label;

		    if( input.length ) {
		        input.val(log);
		    } else {
		        if( log ) alert(log);
		    }

		});
		function readURL3(input) {
		    if (input.files && input.files[0]) {
		        var reader = new FileReader();

		        reader.onload = function (e) {
		            $('#img-upload-3').attr('src', e.target.result);
		        }

		        reader.readAsDataURL(input.files[0]);
		    }
		}

		$("#imgInp3").change(function(){
		    readURL3(this);
		});
    });

    $('.select2').select2({
        allowClear: false,
        theme: 'bootstrap',
        width: "100%",
        placeholder: "Pilih Wilayah",
    });

    $('#currency').select2({
        allowClear: false,
        theme: 'bootstrap',
        width: "100%",
        placeholder: "Pilih Currency",
    });

    $("input[data-type='currency']").on({
        keyup: function() {
            formatCurrency($(this));
        }
    });

    function formatNumber(n) {
        return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }

    function formatCurrency(input, blur) {

        var input_val = input.val();
        if (input_val === "") { return; }
        var original_len = input_val.length;
        var caret_pos = input.prop("selectionStart");

        if (input_val.indexOf(".") >= 0) {

            var decimal_pos = input_val.indexOf(".");
            var left_side = input_val.substring(0, decimal_pos);
            left_side = formatNumber(left_side);
            input_val = left_side;

        } else {

            input_val = formatNumber(input_val);
            input_val = input_val;

            if (blur === "blur") {
                input_val;
            }
        }

        input.val(input_val);

        var updated_len = input_val.length;
        caret_pos = updated_len - original_len + caret_pos;
        input[0].setSelectionRange(caret_pos, caret_pos);
    }
</script>
@endpush
