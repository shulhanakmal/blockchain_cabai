<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Admin\Http\Controllers\LayoutController;
use Illuminate\Http\Request;
use Response;
use Auth;
use DB;
use App\Customer\Customer;
use App\Customer\CustomerGroup;
use Illuminate\Support\Facades\Crypt;
use Modules\Admin\Repositories\CustomerRepository;

class GroupController extends LayoutController {
    protected $customerRepository;

    public function __construct(CustomerRepository $customerRepository) {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
        $this->customerRepository = $customerRepository;
    }

    public function index(Request $request, $code, $hash) {
        $customers = Crypt::decryptString($hash);

        if($customers == 'group')
        DB::setDefaultConnection('mysql');
        $group = CustomerGroup::where('group_id', $this->user->group->id)
        ->when($request->keyword, function ($query) use ($request) {
            $query->where(function ($q) use ($request) {
                $q->where('customer_group_code', 'like', "%{$request->keyword}%");
            });
        })
        ->orderBy('created_at', 'desc')->paginate(10);
        $group->appends($request->only('keyword'));

        return view('admin::group.daftar_group', [
            'group'   => $group,
            'style'   => $this->user->group->style,
            'keyword' => $request->keyword
        ])
        ->with('i', ($request->input('page', 1) - 1) * 10);
    }

    public function edit(Request $request, $code, $hash) {
        $id = Crypt::decryptString($hash);

        DB::setDefaultConnection('mysql');
        $group = CustomerGroup::find($id);

        return view('admin::group.edit_group',[
            'group' => $group,
            'style' => $this->user->group->style
        ]);
    }

    public function create(Request $request, $code, $hash) {
        $groups = Crypt::decryptString($hash);

        if($groups == 'group')
        DB::setDefaultConnection('mysql');
        $group = CustomerGroup::find($groups);

        return view('admin::group.tambah_group', [
            'style' => $this->user->group->style,
            'group' => $group
        ]);
    }

    public function create_post(Request $request) {

        $this->validation($request);

        $groups = new CustomerGroup;
        $groups->setConnection('mysql');
        $last_ids = $groups->where('group_id', $this->user->group->id)->orderBy('ids', 'desc')->first();

        $group = new CustomerGroup;
        $group->setConnection('mysql');
        $group->ids = $last_ids->ids + 1;
        $group->group_id = $this->user->group->id;
        $group->customer_group_code = $request->customer_group_code;
        $group->tax_class_id = 3;
        $group->save();

        toastr()->success('Data berhasil disimpan');
        return redirect()->route('admin.group',[$this->user->group->code, Crypt::encryptString('group')]);
    }

    public function edit_post(Request $request) {

        $this->validation($request);

        $group = CustomerGroup:: find($request->id);
        $group->setConnection('mysql');
        $group->customer_group_code = $request->customer_group_code;
        $group->save();

        toastr()->success('Data berhasil disimpan');
        return redirect()->route('admin.group',[$this->user->group->code, Crypt::encryptString('group')]);
    }

    public function validation($request) {
        $this->validate($request,[
            'customer_group_code' => 'required'
        ]);
    }
}
