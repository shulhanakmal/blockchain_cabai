<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Admin\Http\Controllers\LayoutController;
use Illuminate\Http\Request;
use Response;
use Auth;
use DB;
use App\Category\Category;
use App\Product\Product;
use App\Vendor\vendor;
use App\Product\ProductVendor;
use App\Product\ProductGroupRule;
use App\Category\CategoryProduct;
use App\Lokasi\Location;
use App\Style\style;

class AdminController extends LayoutController {

    function __construct() {
        $this->middleware('permission:administrator', ['only' => ['index']]);
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function index() {
        $styles = new style();
        $styles->setConnection($this->user->group->katalog);
        $styles = $styles->count();

        if($styles == 0) {
            $newStyles = new style();
            $newStyles->setConnection($this->user->group->katalog);
            $newStyles->group_id = $this->user->group->style->group_id;
            $newStyles->logo = $this->user->group->style->logo;
            $newStyles->small_logo = $this->user->group->style->small_logo;
            $newStyles->color = $this->user->group->style->color;
            $newStyles->url_image = $this->user->group->style->url_image;
            $newStyles->save();
        }

        $productGroupRule = new ProductGroupRule;
        $productGroupRule->setConnection($this->user->group->katalog);
        $productGroupRule = $productGroupRule->where('rule_name', 'General')->first();
        if(!isset($productGroupRule)) {
            $newProductGroupRule = new ProductGroupRule;
            $newProductGroupRule->setConnection($this->user->group->katalog);
            $newProductGroupRule->rule_name = 'General';
            $newProductGroupRule->enable = 3;
            $newProductGroupRule->cust_groups = 1;
            $newProductGroupRule->save();
        }

        DB::setDefaultConnection('mysql');
        return view('admin::index', [
            'style' => $this->user->group->style
        ]);
    }
}
