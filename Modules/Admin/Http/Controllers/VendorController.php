<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Admin\Http\Controllers\LayoutController;
use Illuminate\Http\Request;
use Response;
use Auth;
use DB;
use App\Vendor\vendor;
use App\Vendor\VendorMaster;
use App\Vendor\VendorContractMaster;
use Illuminate\Support\Facades\Crypt;
use Hash;
use Modules\Admin\Repositories\VendorRepository;
use Spatie\Permission\Models\Role;
use Input;

class VendorController extends LayoutController {
    protected $vendorRepository;

    public function __construct(VendorRepository $vendorRepository) {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
        $this->vendorRepository = $vendorRepository;
    }

    public function index(Request $request, $code, $hash) {
        $vendors = Crypt::decryptString($hash);

        if($vendors == 'adminDaftarVendor')
        DB::setDefaultConnection('mysql');
        $vm = $this->vendorRepository->adminGetDaftarVendor($request, $this->user->group->id)->paginate(10);
        $vm->appends($request->only('keyword'));

        return view('admin::vendor.daftar_vendor', [
            'vendor'  => $vm,
            'style'   => $this->user->group->style,
            'keyword' => $request->keyword
        ])
        ->with('i', ($request->input('page', 1) - 1) * 10);
    }

    public function create(Request $request, $code, $hash) {
        $vendors = Crypt::decryptString($hash);

        if($vendors == 'adminVendorCreate')
        DB::setDefaultConnection('mysql');
        $checkCode = $this->checkCode();
        $roles = Role::where('guard_name', 'vendor')->where('name', '!=', 'Vendor')->get();

        return view('admin::vendor.tambah_vendor', [
            'style' => $this->user->group->style,
            'inamart_code' => $checkCode,
            'roles' => $roles
        ]);
    }

    public function create_post(Request $request) {
        dd($request);
        // $this->validation($request, 'create');
        if($request->konfirm_katasandi != $request->katasandi) {
            return redirect()->back()->withInput(Input::all())->withErrors(['konfirm_katasandi' => 'Wrong confirmation password!']);
        }

        $cekEmail = new VendorMaster;
        $cekEmail->setConnection('mysql');
        $cekEmail = $cekEmail->where('email', $request->email)->first();
        if(isset($cekEmail)) {
            return redirect()->back()->withInput(Input::all())->withErrors(['email' => 'Email already exist!']);
        }

        $cekVendorNo  = new vendor;
        $cekVendorNo->setConnection($this->user->group->katalog);
        $cekVendorNo = $cekVendorNo->where('vendor_number', $request->vendor_number)->first();
        if(isset($cekVendorNo)) {
            return redirect()->back()->withInput(Input::all())->withErrors(['vendor_number' => 'Vendor number already exist!']);
        }

        $v  = new vendor;
        $v->setConnection($this->user->group->katalog);
        $v  = $this->vendorRepository->saveVendor($request, $v, $this->user->group->katalog);

        $vm = new VendorMaster;
        $vm->setConnection('mysql');
        $vm = $this->vendorRepository->saveVendorMaster($request, $v, $vm, $this->user->group->id);

        toastr()->success('Successfully added new seller');
        return redirect()->route('admin.vendor',[$this->user->group->code, Crypt::encryptString('adminDaftarVendor')]);
    }

    public function edit(Request $request, $code, $hash) {
        $id = Crypt::decryptString($hash);
        $vm = VendorMaster::find($id);
        $roles = Role::where('guard_name', 'vendor')->where('name', '!=', 'Vendor')->get();

        return view('admin::vendor.edit_vendor', [
            'style' => $this->user->group->style,
            'vendor_master' => $vm,
            'roles' => $roles
        ]);
    }

    public function edit_post(Request $request) {
        $this->validation($request, 'edit');

        $vm = new VendorMaster;
        $vm->setConnection('mysql');
        $vm = $vm->find($request->vendor_master_id);

        $v = new vendor;
        $v->setConnection($this->user->group->katalog);
        $v = $v->find($vm->vendor_katalog_id);

        $cekEmail = new VendorMaster;
        $cekEmail->setConnection('mysql');
        $cekEmail = $cekEmail->where('email', $request->email)->where('id', '!=', $vm->id)->first();
        if(isset($cekEmail)) {
            return redirect()->back()->withInput(Input::all())->withErrors(['email' => 'Email already exist!']);
        }

        $cekVendorNo  = new vendor;
        $cekVendorNo->setConnection($this->user->group->katalog);
        $cekVendorNo = $cekVendorNo->where('vendor_number', $request->vendor_number)->where('id', '!=', $vm->vendor_katalog_id)->first();
        if(isset($cekVendorNo)) {
            return redirect()->back()->withInput(Input::all())->withErrors(['vendor_number' => 'Vendor number already exist!']);
        }

        $v = $this->vendorRepository->saveVendor($request, $v, $this->user->group->katalog);
        $vm = $this->vendorRepository->saveVendorMaster($request, $v, $vm, $this->user->group->id);

        toastr()->success('Successfully update seller');
        return redirect()->back();
    }

    public function edit_ubah_password(Request $request) {
        $password     = Hash::make($request->modal_password);

        $vm           = new VendorMaster;
        $vm->setConnection('mysql');
        $vm           = $vm->find($request->vendor_master_id);
        $vm->password = $password;
        $vm->save();

        toastr()->success('Password successfully update');
        return redirect()->back();
    }

    public function delete(Request $request) {
        $vm        = VendorMaster::find($request->vendorMasterId);
        $vContract = $vm->vendor_contract_master;
        $vCode     = $vm->vendor_code_master;
        $vAddress  = $vm->vendor_address_master;
        if($vContract->count() > 0) {
            $vContract->delete();
        }
        if(isset($vCode)) {
            $vCode->delete();
        }
        if(isset($vAddress)) {
            $vAddress->delete();
        }
        toastr()->success('Vendor berhasil dihapus');
        return redirect()->back();
    }

    public function validation($request, $type) {
        if($type == 'create') {
            $messages = [
                'vendor_name.required' => 'Enter seller name!',
                'vendor_number.required' => 'Enter seller number!',
                'email.required' => 'Enter email!',
                'katasandi.required' => 'Enter password!',
                'konfirm_katasandi.required' => 'Enter confirmation password!',
                'company_code.required' => 'Enter company code!',
                'address.required' => 'Enter address!',
                'city.required' => 'Enter city!',
                'region.required' => 'Enter region!',
                'postcode.required' => 'Enter zip code!',
                'telephone.required' => 'Enter phone number!',
                'roles.required' => 'Choose role!',
            ];
            $this->validate($request,[
                'vendor_name'       => 'required',
                'vendor_number'     => 'required',
                'email'             => 'required|email',
                'katasandi'         => 'required',
                'konfirm_katasandi' => 'required',
                'company_code'      => 'required',
                'address'           => 'required',
                'city'              => 'required',
                'region'            => 'required',
                'postcode'          => 'required',
                'telephone'         => 'required',
                'roles'             => 'required',
            ], $messages);
            return view('admin::vendor.tambah_vendor', [
                'data' => $request
            ]);
        } else {
            $messages = [
                'vendor_name.required' => 'Enter seller name!',
                'vendor_number.required' => 'Enter seller number!',
                'email.required' => 'Enter email!',
                'company_code.required' => 'Enter company code!',
                'address.required' => 'Enter address!',
                'city.required' => 'Enter city!',
                'region.required' => 'Enter region!',
                'postcode.required' => 'Enter zip code!',
                'telephone.required' => 'Enter phone number!',
                'roles.required' => 'Choose role!',
            ];
            $this->validate($request,[
                'vendor_name'   => 'required',
                'vendor_number' => 'required',
                'email'         => 'required|email',
                'company_code'  => 'required',
                'address'       => 'required',
                'city'          => 'required',
                'region'        => 'required',
                'postcode'      => 'required',
                'telephone'     => 'required',
                'roles'         => 'required',
            ], $messages);
            return view('admin::vendor.edit_vendor', [
                'data' => $request
            ]);
        }
    }
}
