<?php

namespace Modules\Admin\Http\Controllers;

use Modules\Admin\Http\Controllers\LayoutController;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use QrCode;
use Auth;
use DB;

class PetaniController extends LayoutController {

    public function __construct() {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
        $this->middleware('auth');
    }
}
