<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Admin\Http\Controllers\LayoutController;
use Illuminate\Http\Request;
use Response;
use Auth;
use DB;
use App\Customer\Customer;
use App\Customer\CustomerGroup;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Crypt;
use Modules\Admin\Repositories\CustomerRepository;
use Input;

class CustomerController extends LayoutController {
    protected $vendorRepository;

    public function __construct(CustomerRepository $customerRepository) {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
        $this->customerRepository = $customerRepository;
    }

    public function index(Request $request, $code, $hash) {
        $customers = Crypt::decryptString($hash);

        if($customers == 'customer')
        DB::setDefaultConnection('mysql');
        $customer = Customer::where('group_id', $this->user->group->id)
        ->when($request->keyword, function ($query) use ($request) {
            $query->where(function ($q) use ($request) {
                $q->where('first_name', 'like', "%{$request->keyword}%")
                  ->orWhere('middle_name', 'like', "%{$request->keyword}%")
                  ->orWhere('last_name', 'like', "%{$request->keyword}%")
                  ->orWhere('email', 'like', "%{$request->keyword}%");
            });
        })
        ->orderBy('created_at', 'desc')->paginate(10);
        $customer->appends($request->only('keyword'));

        return view('admin::customer.daftar_customer', [
            'customer' => $customer,
            'style'    => $this->user->group->style,
            'keyword'  => $request->keyword
        ])
        ->with('i', ($request->input('page', 1) - 1) * 10);
    }

    public function create(Request $request, $code, $hash) {
        $customers = Crypt::decryptString($hash);

        if($customers == 'customer')
        DB::setDefaultConnection('mysql');
        $customer_group = CustomerGroup::where('group_id', $this->user->group->id)->get();
        $roles = Role::where('guard_name', 'customer')->where('name', '!=', 'Customer')->get();

        return view('admin::customer.tambah_customer', [
            'style'          => $this->user->group->style,
            'customer_group' => $customer_group,
            'roles'          => $roles
        ]);
    }

    public function edit(Request $request, $code, $hash) {
        $id = Crypt::decryptString($hash);

        DB::setDefaultConnection('mysql');
        $customer       = Customer::find($id);
        $customer_group = CustomerGroup::where('group_id', $this->user->group->id)->get();
        $roles = Role::where('guard_name', 'customer')->where('name', '!=', 'Customer')->get();

        return view('admin::customer.edit_customer',[
            'customer'       => $customer,
            'roles'          => $roles,
            'customer_group' => $customer_group,
            'style'          => $this->user->group->style
        ]);
    }

    public function create_post(Request $request) {
        $this->validation($request, 'create');
        if($request->konfirm_katasandi != $request->katasandi) {
            return redirect()->back()->withInput(Input::all())->withErrors(['konfirm_katasandi' => 'Wrong confirmation password!']);
        }

        $cekEmail = new Customer;
        $cekEmail->setConnection('mysql');
        $cekEmail = $cekEmail->where('email', $request->email)->first();
        if(isset($cekEmail)) {
            return redirect()->back()->withInput(Input::all())->withErrors(['email' => 'Email already exist!']);
        }

        $cust = new Customer;
        $cust->setConnection('mysql');
        $cust = $this->customerRepository->saveCustomer($request, $cust, $this->user->group->id);

        toastr()->success('Successfully create buyer');
        return redirect()->route('admin.customer',[$this->user->group->code, Crypt::encryptString('customer')]);
    }

    public function edit_post(Request $request) {
        $this->validation($request, 'edit');

        $cust = new Customer;
        $cust->setConnection('mysql');
        $cust = $cust->find($request->customer_id);

        $cekEmail = new Customer;
        $cekEmail->setConnection('mysql');
        $cekEmail = $cekEmail->where('email', $request->email)->where('id', '!=', $cust->id)->first();
        if(isset($cekEmail)) {
            return redirect()->back()->withInput(Input::all())->withErrors(['email' => 'Email already exist!']);
        }

        $cust = $this->customerRepository->saveCustomer($request, $cust, $this->user->group->id);

        toastr()->success('Successfully update buyer');
        return redirect()->route('admin.customer',[$this->user->group->code, Crypt::encryptString('customer')]);
    }

    public function validation($request, $type) {
        if($type == 'create') {
            $messages = [
                'first_name.required' => 'Enter first name!',
                'last_name.required' => 'Enter last name!',
                'email.required' => 'Enter email!',
                'gender.required' => 'Choose gender!',
                'katasandi.required' => 'Enter password!',
                'konfirm_katasandi.required' => 'Enter confirmation password!',
                'group.required' => 'Choose group!',
                'street.required' => 'Enter address!',
                'city.required' => 'Enter city!',
                'region.required' => 'Enter region!',
                'postcode.required' => 'Enter postcode!',
                'telephone.required' => 'Enter telephone!',
            ];
            $this->validate($request,[
                'first_name'        => 'required',
                'last_name'         => 'required',
                'email'             => 'required',
                'gender'            => 'required',
                'katasandi'         => 'required',
                'konfirm_katasandi' => 'required',
                'group'             => 'required',
                'street'            => 'required',
                'city'              => 'required',
                'region'            => 'required',
                'postcode'          => 'required',
                'telephone'         => 'required'
            ], $messages);
            return view('admin::customer.tambah_customer', [
                'data' => $request
            ]);
        } else {
            $messages = [
                'first_name.required' => 'Enter first name!',
                'last_name.required' => 'Enter last name!',
                'email.required' => 'Enter email!',
                'gender.required' => 'Choose gender!',
                'group.required' => 'Choose group!',
                'street.required' => 'Enter address!',
                'city.required' => 'Enter city!',
                'region.required' => 'Enter region!',
                'postcode.required' => 'Enter postcode!',
                'telephone.required' => 'Enter telephone!',
            ];
            $this->validate($request,[
                'first_name'        => 'required',
                'last_name'         => 'required',
                'email'             => 'required',
                'gender'            => 'required',
                'group'             => 'required',
                'street'            => 'required',
                'city'              => 'required',
                'region'            => 'required',
                'postcode'          => 'required',
                'telephone'         => 'required'
            ], $messages);
            return view('admin::customer.tambah_customer', [
                'data' => $request
            ]);
        }
    }
}
