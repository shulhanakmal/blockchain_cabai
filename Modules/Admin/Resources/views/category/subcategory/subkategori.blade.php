@extends('admin::layouts.app')
@section('content')
<ol class="breadcrumb">
    <li class="breadcrumb-item">List Sub Category</li>
</ol>
<div class="container-fluid" style="margin-top:16px;">
    <div class="animated fadeIn">
        <div class="card">
            <div class="card-body">
                <div class="pull-left">
                    <form class="form-inline pull-right" action="{{ url()->current() }}">
                        <a href="{{ route('admin.subcategory', [Auth()->user()->group->code, Crypt::encryptString('subcategory')]) }}" class="btn btn-secondary" style='color:#fff'> Reset</a>&nbsp;
                        <input class="form-control" type="text" name="keyword" placeholder="Search.." value="{{ $keyword }}">&nbsp;
                        <button class="btn btn-primary" type="submit">Search</button>&nbsp;
                    </form>
                </div>
                <div class="pull-right">
                    <a href="{{ route('admin.subcategory.create', [Auth::user()->group->code, Crypt::encryptString('subcategory')]) }}" class="btn btn-primary">
                        <span class="span">&nbsp;Add New</span></i>
                    </a>
                </div>
                <div class="white-box">
                    <div class="table-responsive">
                        <table id="tabel-subcategory" class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center" style="vertical-align:middle">No</th>
                                    <th class="text-center" style="vertical-align:middle">Category</th>
                                    <th class="text-center" style="vertical-align:middle">Sub Category</th>
                                    <th class="text-center" style="vertical-align:middle">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($subcategory as $sc)
                                <tr>
                                    <td class="text-center">{{ ++$i }}</td>
                                    <td>{{ $sc->category->name }}</td>
                                    <td>{{ $sc->name }}</td>
                                    <td style="vertical-align:middle" align="center">
                                        <a class="btn btn-outline-primary" title="Detail" href="{{ route('admin.subcategory_edit', [Auth::user()->group->code, Crypt::encryptString($sc->id)]) }}">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        {{-- <a class="btn btn-outline-primary" title="Delete">
                                            <i class="fa fa-trash-o"></i>
                                        </a> --}}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {!! $subcategory->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    @foreach ($errors->all() as $error)
        toastr.error("{{$error}}")
    @endforeach

    $(document).ready(function() {
        $('#tabel-subcategory .btn').tooltip();
    }

    var table_subcategory = null;
    table_subcategory =  $('#tabel-subcategory').DataTable({
        lengthChange: false,
        searching: false,
        paging:   false,
        ordering: true,
        info:     false
    })
</script>
@endpush
