@extends('admin::layouts.app')
@section('content')
<ol class="breadcrumb">
    <li class="breadcrumb-item">Edit Buyer</li>
</ol>
<div class="container-fluid" style="margin-top:16px;">
    <div class="animated fadeIn">
        <form id="forms" method="post" action="{{ route('admin.customer.edit_post', [Auth::user()->group->code]) }}" enctype="multipart/form-data" role="form">
            {{ csrf_field() }}
            <div class="card">
                <input id="customer_id" name="customer_id" type="hidden" value="{{ $customer->id }}">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label style="font-weight: bold; font-size: 14px;">First Name <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input class="form-control @if($errors->has('first_name')) is-invalid @endif" id="first_name" type="text" name="first_name" placeholder="First Name" value="{{ old('first_name') == null ? $customer->first_name : old('first_name') }}" autocomplete="off">
                                <em id="name-error" class="error invalid-feedback">@error('first_name'){{ $message }}@enderror</em>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Middle Name</label>
                            <div class="form-group">
                                <input class="form-control @if($errors->has('middle_name')) is-invalid @endif" id="middle_name" type="text" name="middle_name" placeholder="Middle Name" value="{{ old('middle_name') == null ? $customer->middle_name : old('middle_name') }}" autocomplete="off">
                                <em id="name-error" class="error invalid-feedback">@error('middle_name'){{ $message }}@enderror</em>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Last Name <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input class="form-control @if($errors->has('last_name')) is-invalid @endif" id="last_name" type="text" name="last_name" placeholder="Last Name" value="{{ old('last_name') == null ? $customer->last_name : old('last_name') }}" autocomplete="off">
                                <em id="name-error" class="error invalid-feedback">@error('last_name'){{ $message }}@enderror</em>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label style="font-weight: bold; font-size: 14px;">Email <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input class="form-control @if($errors->has('email')) is-invalid @endif" id="email" type="email" name="email" placeholder="Email" value="{{ old('email') == null ? $customer->email : old('email') }}" autocomplete="off">
                                <em id="name-error" class="error invalid-feedback">@error('email'){{ $message }}@enderror</em>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label style="font-weight: bold; font-size: 14px;">Gender <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <select class="custom-select val-custom form-control select2 @if($errors->has('gender')) is-invalid @endif" data-placeholder="Choose Gender" name="gender" id="gender">
                                    <option></option>
                                    <option value="1"
                                        @if(old('gender') != null)
                                            @if(old('gender') == 1)
                                            selected
                                            @endif
                                        @else
                                            @if($customer->gender == 1)
                                            selected
                                            @endif
                                        @endif
                                    >Laki-laki</option>
                                    <option value="2"
                                        @if(old('gender') != null)
                                            @if(old('gender') == 2)
                                            selected
                                            @endif
                                        @else
                                            @if($customer->gender == 2)
                                            selected
                                            @endif
                                        @endif
                                    >Perempuan</option>
                                </select>
                                <em id="name-error" class="error invalid-feedback">@error('gender'){{ $message }}@enderror</em>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Address<span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <textarea class="form-control @if($errors->has('street')) is-invalid @endif" id="street" rows="9" name="street" placeholder="Address" autocomplete="off">{{ old('street') == null ? $customer->customer_address->street : old('street') }}</textarea>
                                <em id="name-error" class="error invalid-feedback">@error('street'){{ $message }}@enderror</em>
                            </div>
                        </div>
                        <div class="col-md-6 row">
                            <div class="col-md-12">
                                <label style="font-weight: bold; font-size: 14px;">Region <span style="color: red;">(*)</span></label>
                                <div class="form-group">
                                    <input class="form-control @if($errors->has('region')) is-invalid @endif" id="region" type="text" name="region" placeholder="Region" value="{{ old('region') == null ? $customer->customer_address->region : old('region') }}" autocomplete="off">
                                    <em id="name-error" class="error invalid-feedback">@error('region'){{ $message }}@enderror</em>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label style="font-weight: bold; font-size: 14px;">City <span style="color: red;">(*)</span></label>
                                <div class="form-group">
                                    <input class="form-control @if($errors->has('city')) is-invalid @endif" id="city" type="text" name="city" placeholder="City" value="{{ old('city') == null ? $customer->customer_address->city : old('city') }}" autocomplete="off">
                                    <em id="name-error" class="error invalid-feedback">@error('city'){{ $message }}@enderror</em>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label style="font-weight: bold; font-size: 14px;">Zip Code <span style="color: red;">(*)</span></label>
                                <div class="form-group">
                                    <input class="form-control @if($errors->has('postcode')) is-invalid @endif" id="postcode" type="text" name="postcode" placeholder="Zip Code" value="{{ old('postcode') == null ? $customer->customer_address->postcode : old('postcode') }}" autocomplete="off">
                                    <em id="name-error" class="error invalid-feedback">@error('postcode'){{ $message }}@enderror</em>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Phone <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input class="form-control @if($errors->has('telephone')) is-invalid @endif" id="telephone" type="text" name="telephone" placeholder="Phone" value="{{ old('telephone') == null ? $customer->customer_address->telephone : old('telephone') }}" autocomplete="off">
                                <em id="name-error" class="error invalid-feedback">@error('telephone'){{ $message }}@enderror</em>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Fax</label>
                            <div class="form-group">
                                <input class="form-control @if($errors->has('fax')) is-invalid @endif" id="fax" type="text" name="fax" placeholder="Fax" value="{{ old('fax') == null ? $customer->customer_address->fax : old('fax') }}" autocomplete="off">
                                <em id="name-error" class="error invalid-feedback">@error('fax'){{ $message }}@enderror</em>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label style="font-weight: bold; font-size: 14px;">Group <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <select class="form-control select2 select2-multiple select2-hidden-accessible @if($errors->has('group')) is-invalid @endif" data-placeholder="Choose Group" name="group[]" id="group" multiple="" data-select2-id="select2-3" tabindex="-1" aria-hidden="true">
                                    <option></option>
                                    @foreach($customer_group as $cg)
                                        <option value="{{ $cg->ids }}"
                                            @if(old("group"))
                                                {{ (in_array($cg->ids, old("group")) ? "selected":"") }}
                                            @else
                                                {{ (in_array($cg->ids, $customer->customer_grouping->pluck('customer_group_id')->all()) ? "selected":"") }}
                                            @endif
                                        >{{ $cg->customer_group_code }}</option>
                                    @endforeach
                                </select>
                                <em id="name-error" class="error invalid-feedback">@error('group'){{ $message }}@enderror</em>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label style="font-weight: bold; font-size: 14px;">Role <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <select class="form-control select2 select2-multiple select2-hidden-accessible @if($errors->has('roles')) is-invalid @endif" data-placeholder="Choose Role" name="roles[]" id="roles" multiple="" data-select2-id="select2-2" tabindex="-1" aria-hidden="true">
                                    <option></option>
                                    @foreach($roles as $r)
                                        <option value="{{ $r->id }}"
                                            @if(old("roles"))
                                                {{ (in_array($r->id, old("roles")) ? "selected":"") }}
                                            @else
                                                {{ (in_array($r->id, $customer->roles->pluck('id')->all()) ? "selected":"") }}
                                            @endif
                                        >{{ $r->name }}</option>
                                    @endforeach
                                </select>
                                <em id="name-error" class="error invalid-feedback">@error('group'){{ $message }}@enderror</em>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Status <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <div class="input-group">
                                    <label class="col-form-label switch switch-md switch-label switch-pill switch-primary">
                                        <input class="switch-input" type="checkbox" id="status_customer" name="status_customer" onclick="checkBox()" @if($customer->status == 1) value="1" checked @else value="0" @endif>
                                        <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
                                    </label>
                                    &nbsp;&nbsp;&nbsp;<label id="status_name" name="status_name" class="col-form-label">@if($customer->status == 1) Active @else Inactive @endif</label>
                                </div>
                                <em id="firstname-error" class="error invalid-feedback">@error('status_vendor'){{ $message }}@enderror</em>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button class="btn btn-primary float-right" type="submit">
                        Save
                    </button>
                    <a class="btn btn-secondary" href="{{ route('admin.customer', [Auth::user()->group->code, Crypt::encryptString('customer')]) }}">Back</a>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@push('scripts')
<script>


    $('#gender').select2({
        allowClear: false,
        theme: 'bootstrap',
        width: "100%"
    });

    $('#group').select2({
        allowClear: false,
        theme: 'bootstrap',
        width: "100%"
    });

    $('#roles').select2({
        allowClear: false,
        theme: 'bootstrap',
        width: "100%"
    });

    function checkBox() {
        if (document.getElementById('status_customer').checked) {
            document.getElementById('status_name').innerHTML = "Aktif";
            document.getElementById('status_customer').value = 1;
        } else {
            document.getElementById('status_name').innerHTML = "Tidak Aktif";
            document.getElementById('status_customer').value = 0;
        }
    }
</script>
@endpush
