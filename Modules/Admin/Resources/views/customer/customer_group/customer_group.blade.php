@extends('admin::layouts.app')
@section('content')
<ol class="breadcrumb row">
    <li class="col-md-12 breadcrumb-item">
        List Buyer Group
    </li>
</ol>
<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="card">
            <div class="card-body">
                <div class="pull-left">
                    <form class="form-inline pull-right" action="{{ url()->current() }}">
                        <a href="{{ route('admin.customer_group', [Auth()->user()->group->code, Crypt::encryptString('customer_group')]) }}" class="btn btn-secondary"> Reset</a>&nbsp;
                        <input class="form-control" type="text" name="keyword" placeholder="Search.." value="{{ $keyword }}">&nbsp;
                        <button class="btn btn-primary" type="submit">Search</button>&nbsp;
                    </form>
                </div>
                <div class="pull-right">
                    <a href="{{ route('admin.customer_group.create', [Auth::user()->group->code, Crypt::encryptString('customer_group')]) }}" class="btn btn-primary">
                        <span class="span">&nbsp;Add New</span></i>
                    </a>
                </div>
                <div class="white-box">
                    <div class="table-responsive">
                        <table id="tabel-customer_group" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th class="text-center" style="vertical-align:middle">No</th>
                                    <th class="text-center" style="vertical-align:middle">Name</th>
                                    <th class="text-center" style="vertical-align:middle">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($customer_group as $cg)
                                <tr>
                                    <td class="text-center">{{ ++$i }}</td>
                                    <td>{{ $cg->customer_group_code }}</td>
                                    <td style="vertical-align:middle" align="center">
                                        <a class="btn btn-outline-primary" title="Detail" href="{{ route('admin.customer_group_show', [Auth::user()->group->code, Crypt::encryptString($cg->id)]) }}">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        {{-- <a class="btn btn-danger" data-placement="right" title="Hapus">
                                            <i class="fa fa-trash-o" style="color:white"></i>
                                        </a> --}}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {!! $customer_group->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    @foreach ($errors->all() as $error)
        toastr.error("{{$error}}")
    @endforeach

    $(document).ready(function() {
        $('#tabel-customer_group .btn').tooltip();
    })
</script>
@endpush
