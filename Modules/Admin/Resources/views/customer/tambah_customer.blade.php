@extends('admin::layouts.app')
@section('content')
<ol class="breadcrumb">
    <li class="breadcrumb-item">Add New Buyer</li>
</ol>
<div class="container-fluid">
    <div class="animated fadeIn">
        <form id="forms" method="post" action="{{ route('admin.customer.create_post', [Auth::user()->group->code]) }}" enctype="multipart/form-data" role="form">
            {{ csrf_field() }}
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label style="font-weight: bold; font-size: 14px;">First Name <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input class="form-control @if($errors->has('first_name')) is-invalid @endif" id="first_name" type="text" name="first_name" placeholder="First Name" value="{{ old('first_name') }}" autocomplete="off">
                                <em id="name-error" class="error invalid-feedback">@error('first_name'){{ $message }}@enderror</em>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Middle Name</label>
                            <div class="form-group">
                                <input class="form-control @if($errors->has('middle_name')) is-invalid @endif" id="middle_name" type="text" name="middle_name" placeholder="Middle Name" value="{{ old('middle_name') }}" autocomplete="off">
                                <em id="name-error" class="error invalid-feedback">@error('middle_name'){{ $message }}@enderror</em>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Last Name <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input class="form-control @if($errors->has('last_name')) is-invalid @endif" id="last_name" type="text" name="last_name" placeholder="Last Name" value="{{ old('last_name') }}" autocomplete="off">
                                <em id="name-error" class="error invalid-feedback">@error('last_name'){{ $message }}@enderror</em>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label style="font-weight: bold; font-size: 14px;">Email <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input class="form-control @if($errors->has('email')) is-invalid @endif" id="email" type="email" name="email" placeholder="Email" value="{{ old('email') }}" autocomplete="off">
                                <em id="name-error" class="error invalid-feedback">@error('email'){{ $message }}@enderror</em>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label style="font-weight: bold; font-size: 14px;">Gender <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <select class="custom-select val-custom form-control select2 @if($errors->has('gender')) is-invalid @endif" data-placeholder="Choose Gender" name="gender" id="gender">
                                    <option></option>
                                    <option value="1"
                                        @if(old('gender') != null)
                                            @if(old('gender') == 1)
                                            selected
                                            @endif
                                        @endif
                                    >Laki-laki</option>
                                    <option value="2"
                                        @if(old('gender') != null)
                                            @if(old('gender') == 2)
                                            selected
                                            @endif
                                        @endif
                                    >Perempuan</option>
                                </select>
                                <em id="name-error" class="error invalid-feedback">@error('gender'){{ $message }}@enderror</em>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Address<span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <textarea class="form-control @if($errors->has('street')) is-invalid @endif" id="street" rows="9" name="street" placeholder="Address" autocomplete="off">{{ old('street') }}</textarea>
                                <em id="name-error" class="error invalid-feedback">@error('street'){{ $message }}@enderror</em>
                            </div>
                        </div>
                        <div class="col-md-6 row">
                            <div class="col-md-12">
                                <label style="font-weight: bold; font-size: 14px;">Region <span style="color: red;">(*)</span></label>
                                <div class="form-group">
                                    <input class="form-control @if($errors->has('region')) is-invalid @endif" id="region" type="text" name="region" placeholder="Region" value="{{ old('region') }}" autocomplete="off">
                                    <em id="name-error" class="error invalid-feedback">@error('region'){{ $message }}@enderror</em>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label style="font-weight: bold; font-size: 14px;">City <span style="color: red;">(*)</span></label>
                                <div class="form-group">
                                    <input class="form-control @if($errors->has('city')) is-invalid @endif" id="city" type="text" name="city" placeholder="City" value="{{ old('city') }}" autocomplete="off">
                                    <em id="name-error" class="error invalid-feedback">@error('city'){{ $message }}@enderror</em>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label style="font-weight: bold; font-size: 14px;">Zip Code <span style="color: red;">(*)</span></label>
                                <div class="form-group">
                                    <input class="form-control @if($errors->has('postcode')) is-invalid @endif" id="postcode" type="text" name="postcode" placeholder="Zip Code" value="{{ old('postcode') }}" autocomplete="off">
                                    <em id="name-error" class="error invalid-feedback">@error('postcode'){{ $message }}@enderror</em>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Phone <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input class="form-control @if($errors->has('telephone')) is-invalid @endif" id="telephone" type="text" name="telephone" placeholder="Phone" value="{{ old('telephone') }}" autocomplete="off">
                                <em id="name-error" class="error invalid-feedback">@error('telephone'){{ $message }}@enderror</em>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Fax</label>
                            <div class="form-group">
                                <input class="form-control @if($errors->has('fax')) is-invalid @endif" id="fax" type="text" name="fax" placeholder="Fax" value="{{ old('fax') }}" autocomplete="off">
                                <em id="name-error" class="error invalid-feedback">@error('fax'){{ $message }}@enderror</em>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <label style="font-weight: bold; font-size: 14px;">Group <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <select class="form-control select2 select2-multiple select2-hidden-accessible @if($errors->has('group')) is-invalid @endif" data-placeholder="Choose Group" name="group[]" id="group" multiple="" data-select2-id="select2-3" tabindex="-1" aria-hidden="true">
                                    <option></option>
                                    @foreach($customer_group as $cg)
                                        <option value="{{ $cg->ids }}"
                                            @if(old("group"))
                                                {{ (in_array($cg->ids, old("group")) ? "selected":"") }}
                                            @endif
                                        >{{ $cg->customer_group_code }}</option>
                                    @endforeach
                                </select>
                                <em id="name-error" class="error invalid-feedback">@error('group'){{ $message }}@enderror</em>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label style="font-weight: bold; font-size: 14px;">Role <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <select class="form-control select2 select2-multiple select2-hidden-accessible @if($errors->has('roles')) is-invalid @endif" data-placeholder="Choose Role" name="roles[]" id="roles" multiple="" data-select2-id="select2-2" tabindex="-1" aria-hidden="true">
                                    <option></option>
                                    @foreach($roles as $r)
                                        <option value="{{ $r->id }}"
                                            @if(old("roles"))
                                                {{ (in_array($r->id, old("roles")) ? "selected":"") }}
                                            @endif
                                        >{{ $r->name }}</option>
                                    @endforeach
                                </select>
                                <em id="name-error" class="error invalid-feedback">@error('group'){{ $message }}@enderror</em>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Password <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input class="form-control @if($errors->has('katasandi')) is-invalid @endif" id="katasandi" type="password" name="katasandi" placeholder="Password" value="{{ old('katasandi') }}" autocomplete="new-password">
                                <em id="name-error" class="error invalid-feedback">@error('katasandi'){{ $message }}@enderror</em>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Confirmation Password <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input class="form-control @if($errors->has('konfirm_katasandi')) is-invalid @endif" id="konfirm_katasandi" type="password" name="konfirm_katasandi" placeholder="Confirmation Password" value="{{ old('konfirm_katasandi') }}" autocomplete="new-password">
                                <em id="name-error" class="error invalid-feedback">@error('konfirm_katasandi'){{ $message }}@enderror</em>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Status <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <div class="input-group">
                                    <label class="col-form-label switch switch-md switch-label switch-pill switch-primary">
                                        <input class="switch-input @if($errors->has('status_customer')) is-invalid @endif" type="checkbox" id="status_customer" name="status_customer" onclick="checkBox()">
                                        <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
                                    </label>
                                    &nbsp;&nbsp;&nbsp;<label id="status_name" name="status_name" class="col-form-label">Inactive</label>
                                </div>
                                <em id="firstname-error" class="error invalid-feedback">@error('status_vendor'){{ $message }}@enderror</em>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button class="btn btn-primary float-right" type="submit">
                        Save
                    </button>
                    <a class="btn btn-secondary" href="{{ route('admin.customer', [Auth::user()->group->code, Crypt::encryptString('customer')]) }}">Back</a>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@push('scripts')
<script>

    $('#gender').select2({
        allowClear: false,
        theme: 'bootstrap',
        width: "100%"
    });

    $('#group').select2({
        allowClear: false,
        theme: 'bootstrap',
        width: "100%"
    });

    $('#roles').select2({
        allowClear: false,
        theme: 'bootstrap',
        width: "100%"
    });

    $(document).ready(function() {
        document.getElementById('status_customer').value = 0;
    });

    function checkBox() {
        if (document.getElementById('status_customer').checked) {
            document.getElementById('status_name').innerHTML = "Active";
            document.getElementById('status_customer').value = 1;
        } else {
            document.getElementById('status_name').innerHTML = "Inactive";
            document.getElementById('status_customer').value = 0;
        }
    }
</script>
@endpush
