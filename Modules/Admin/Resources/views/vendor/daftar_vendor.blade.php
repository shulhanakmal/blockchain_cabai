@extends('admin::layouts.app')
@section('content')
<ol class="breadcrumb row">
    <li class="col-md-12 breadcrumb-item">
        List Petani
    </li>
</ol>
<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="white-box">
            <div class="card">
                <div class="card-body">
                    <div class="pull-left">
                        <form class="form-inline" action="{{ url()->current() }}">
                            <a href="{{ route('admin.vendor',[Auth::user()->group->code, Crypt::encryptString('adminDaftarVendor')]) }}" class="btn btn-secondary"> Reset</a>&nbsp;
                            <input class="form-control" type="text" name="keyword" placeholder="Search.." value="{{ $keyword }}">&nbsp;
                            <button class="btn btn-primary" type="submit">Search</button>
                        </form>
                    </div>
                    <div class="pull-right">
                        <a href="{{ route('admin.vendor.create', [Auth::user()->group->code, Crypt::encryptString('adminVendorCreate')]) }}" class="btn btn-primary">
                            <span class="span">&nbsp;Add New</span></i>
                        </a>
                    </div>
                    <div class="table-responsive">
                        <table id="tabel-vendor" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="10%" class="text-center">No</th>
                                    <th class="text-center">Nama Petani</th>
                                    <th class="text-center">Status</th>
                                    <th width="15%" class="text-center">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($vendor as $v)
                                <tr>
                                    <td class="text-center">{{ ++$i }}</td>
                                    <td>{{ $v->vendor_name }}</td>
                                    <td class="text-center">
                                        @if($v->status == 0)
                                        <label style="vertical-align:middle;color:white;" class="badge badge-warning text-center">
                                            Inactive
                                        </label>
                                        @else
                                        <label style="vertical-align:middle" class="badge badge-primary text-center">
                                            Active
                                        </label>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        <a class="btn btn-outline-primary" title="Detail" href="{{ route('admin.vendor.edit', [Auth::user()->group->code, Crypt::encryptString($v->id)]) }}">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        {{-- <a class="btn btn-danger" data-placement="right" title="Hapus vendor" type="button" id="hapusVendor" data-id="{{ $v->id }}" data-status="{{ $v->status }}">
                                            <i class="fa fa-trash-o" style="color: white;"></i>
                                        </a> --}}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {!! $vendor->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalHapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-danger modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Hapus Petani</h4>
            </div>
            <div class="modal-body">
                <p>
                    Are you sure?
                </p>
            </div>
            <div class="modal-footer">
                <form id="hapusModalForm" action="" method="POST">
                    @method('POST')
                    @csrf
                    <input type="hidden" name="vendorMasterId" id="vendorMasterId">
                    <button class="btn btn-secondary" style="color:white;" type="button" data-dismiss="modal">No</button>
                    <button class="btn btn-danger" type="submit">Yes</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalWarning" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-warning modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Attention</h4>
            </div>
            <div class="modal-body">
                <p>
                    Petani masih active, tolong Nonaktifkan terlebih dahulu!
                </p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-warning" style="color:white;" type="button" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    @foreach ($errors->all() as $error)
        toastr.error("{{$error}}")
    @endforeach

    $(document).ready(function() {
        $('#tabel-vendor .btn').tooltip();
        $("#tabel-vendor").on("click", "#hapusVendor", function () {
            if($(this).attr("data-status") == 1) {
                $('#modalWarning').modal('show');
            } else {
                $('#modalHapus').modal('show');
                $("#modalHapus .modal-dialog .modal-content #vendorMasterId").val($(this).attr("data-id"))
                var formAction = "{{ url('admin/' . Auth::user()->group->code . '/vendor/delete') }}";
                $("#hapusModalForm").attr("action", formAction);
            }
        });
    })
</script>
@endpush
