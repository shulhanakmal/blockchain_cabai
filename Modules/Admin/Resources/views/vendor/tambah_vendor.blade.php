@extends('admin::layouts.app')
@section('content')
<ol class="breadcrumb">
    <li class="breadcrumb-item">Tambah Petani Baru</li>
</ol>
<div class="container-fluid" style="margin-top:16px;">
    <div class="animated fadeIn">
        <form id="forms" method="post" action="{{ route('admin.vendor.create_post', [Auth::user()->group->code]) }}" enctype="multipart/form-data" role="form">
            <div class="card">
                {{ csrf_field() }}
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label style="font-weight: bold; font-size: 14px;">Nama Petani <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input class="form-control @if($errors->has('vendor_name')) is-invalid @endif" id="vendor_name" type="text" name="vendor_name" placeholder="Petani Name" value="{{ old('vendor_name') }}" autocomplete="off">
                                <em id="name-error" class="error invalid-feedback">@error('vendor_name'){{ $message }}@enderror</em>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12">
                            <label style="font-weight: bold; font-size: 14px;">Email <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input class="form-control @if($errors->has('email')) is-invalid @endif" id="email" type="email" name="email" placeholder="Email" value="{{ old('email') }}" autocomplete="off">
                                <em id="name-error" class="error invalid-feedback">@error('email'){{ $message }}@enderror</em>
                            </div>
                        </div>
                    </div>

                    <div class="row" style="display: none;">
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Inamart Code <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input class="form-control @if($errors->has('inamart_code')) is-invalid @endif" id="inamart_code" type="text" name="inamart_code" placeholder="Inamart Code" value="{{ $inamart_code }}" readonly>
                                <em id="name-error" class="error invalid-feedback">Enter Inamart Code</em>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Alamat<span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <textarea class="form-control @if($errors->has('address')) is-invalid @endif" id="address" rows="9" name="address" placeholder="Address" autocomplete="off">{{ old('address') }}</textarea>
                                <em id="name-error" class="error invalid-feedback">@error('address'){{ $message }}@enderror</em>
                            </div>
                        </div>
                        <div class="col-md-6 row">
                            <div class="col-md-12">
                                <label style="font-weight: bold; font-size: 14px;">Provinsi <span style="color: red;">(*)</span></label>
                                <div class="form-group">
                                    <input class="form-control @if($errors->has('region')) is-invalid @endif" id="region" type="text" name="region" placeholder="Region" value="{{ old('region') }}" autocomplete="off">
                                    <em id="name-error" class="error invalid-feedback">@error('region'){{ $message }}@enderror</em>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label style="font-weight: bold; font-size: 14px;">Kota <span style="color: red;">(*)</span></label>
                                <div class="form-group">
                                    <input class="form-control @if($errors->has('city')) is-invalid @endif" id="city" type="text" name="city" placeholder="City" value="{{ old('city') }}" autocomplete="off">
                                    <em id="name-error" class="error invalid-feedback">@error('city'){{ $message }}@enderror</em>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label style="font-weight: bold; font-size: 14px;">Kode Pos <span style="color: red;">(*)</span></label>
                                <div class="form-group">
                                    <input class="form-control @if($errors->has('postcode')) is-invalid @endif" id="postcode" type="text" name="postcode" placeholder="Zip Code" value="{{ old('postcode') }}" autocomplete="off">
                                    <em id="name-error" class="error invalid-feedback">@error('postcode'){{ $message }}@enderror</em>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Kontak <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input class="form-control @if($errors->has('telephone')) is-invalid @endif" id="telephone" type="text" name="telephone" placeholder="Phone" value="{{ old('telephone') }}" autocomplete="off">
                                <em id="name-error" class="error invalid-feedback">@error('telephone'){{ $message }}@enderror</em>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Password <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input class="form-control @if($errors->has('katasandi')) is-invalid @endif" id="katasandi" type="password" name="katasandi" placeholder="Password" value="{{ old('katasandi') }}" autocomplete="new-password">
                                <em id="name-error" class="error invalid-feedback">@error('katasandi'){{ $message }}@enderror</em>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Confirmation Password <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input class="form-control @if($errors->has('konfirm_katasandi')) is-invalid @endif" id="konfirm_katasandi" type="password" name="konfirm_katasandi" placeholder="konfirm_katasandi" value="{{ old('konfirm_katasandi') }}" autocomplete="new-password">
                                <em id="name-error" class="error invalid-feedback">@error('konfirm_katasandi'){{ $message }}@enderror</em>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Status <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <div class="input-group">
                                    <label class="col-form-label switch switch-md switch-label switch-pill switch-primary">
                                        <input class="switch-input @if($errors->has('status_vendor')) is-invalid @endif" type="checkbox" id="status_vendor" name="status_vendor" onclick="checkBox()">
                                        <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
                                    </label>
                                    &nbsp;&nbsp;&nbsp;<label id="status_name" name="status_name" class="col-form-label">Inactive</label>
                                </div>
                                <em id="firstname-error" class="error invalid-feedback">@error('status_vendor'){{ $message }}@enderror</em>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button class="btn btn-primary float-right" type="submit">
                        Save
                    </button>
                    <a class="btn btn-secondary" href="{{ route('admin.vendor', [Auth()->user()->group->code, Crypt::encryptString('adminDaftarVendor')]) }}">Back</a>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $(document).ready(function() {
        document.getElementById('status_vendor').value = 0;
        $('#roles').select2({
            allowClear: false,
            theme: 'bootstrap',
            width: "100%"
        });
    });

    function checkBox() {
        if (document.getElementById('status_vendor').checked) {
            document.getElementById('status_name').innerHTML = "Active";
        document.getElementById('status_vendor').value = 1;
        } else {
            document.getElementById('status_name').innerHTML = "Inactive";
            document.getElementById('status_vendor').value = 0;
        }
    }
</script>
@endpush
