@extends('admin::layouts.app')
@section('content')
<ol class="breadcrumb">
    <li class="breadcrumb-item">Edit Contract</li>
</ol>
<div class="container-fluid" style="margin-top:16px;">
    <div class="animated fadeIn">
        <form id="forms" method="post" action="{{ route('admin.vendor.contract_edit_post', [Auth::user()->group->code]) }}" enctype="multipart/form-data" role="form">
            <div class="card">
                {{ csrf_field() }}
                <input id="vendor_master_id" name="vendor_master_id" type="hidden" value="{{ $vendor_contract_master->id }}">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label style="font-weight: bold; font-size: 14px;">Seller <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <select class="custom-select val-custom form-control select2 @if($errors->has('vendor_name')) is-invalid @endif" data-placeholder="Choose Exporter" name="vendor_name" id="vendor_name" disabled>
                                    <option></option>
                                    @foreach($vendor_master as $vm)
                                    <option value="{{ $vm->id }}"
                                        @if(old('vendor_name') != null)
                                            @if(old('vendor_name') == $vm->id)
                                            selected
                                            @endif
                                        @else
                                            @if($vm->id == $vendor_contract_master->vendor_master_id)
                                            selected
                                            @endif
                                        @endif
                                    >{{ $vm->vendor_name }}</option>
                                    @endforeach
                                </select>
                                <em id="name-error" class="error invalid-feedback">@error('vendor_name'){{ $message }}@enderror</em>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label style="font-weight: bold; font-size: 14px;">Category <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <select class="custom-select val-custom form-control select2 @if($errors->has('category')) is-invalid @endif" data-placeholder="Choose Category" name="category" id="category" disabled>
                                    <option></option>
                                    @foreach($category as $c)
                                    <option value="{{ $c->id }}"
                                        @if(old('category') != null)
                                            @if(old('category') == $c->id)
                                            selected
                                            @endif
                                        @else
                                            @if($c->id == $vendor_contract_master->category)
                                            selected
                                            @endif
                                        @endif
                                    >{{ $c->name }}</option>
                                    @endforeach
                                </select>
                                <em id="name-error" class="error invalid-feedback">@error('category'){{ $message }}@enderror</em>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label style="font-weight: bold; font-size: 14px;">Contract Name <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input class="form-control @if($errors->has('vendor_name')) is-invalid @endif" id="contract_name" type="text" name="contract_name" placeholder="Contract Name" value="{{ old('contract_name') == null ? $vendor_contract_master->contract_name : old('contract_name') }}" autocomplete="off">
                                <em id="name-error" class="error invalid-feedback">@error('contract_name'){{ $message }}@enderror</em>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Contract Number <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input class="form-control @if($errors->has('contract_no')) is-invalid @endif" id="contract_no" type="text" name="contract_no" placeholder="Contract Number" value="{{ old('contract_no') == null ? $vendor_contract_master->contract_no : old('contract_no') }}" autocomplete="off">
                                <em id="name-error" class="error invalid-feedback">@error('contract_no'){{ $message }}@enderror</em>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Contract Price <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input class="form-control @if($errors->has('contract_price')) is-invalid @endif" id="contract_price" type="text" name="contract_price" placeholder="Contract Price" data-type="currency" value="{{ old('contract_price') == null ? number_format($vendor_contract_master->contract_price, 0) : old('contract_price') }}" autocomplete="off">
                                <em id="name-error" class="error invalid-feedback">@error('contract_price'){{ $message }}@enderror</em>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Start Date <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input class="form-control js-datepicker @if($errors->has('contract_start')) is-invalid @endif" id="contract_start" type="text" name="contract_start" placeholder="Start Date" value="{{ old('contract_start') == null ? date('d-M-Y', strtotime($vendor_contract_master->contract_start)) : old('contract_start') }}" autocomplete="off">
                                <em id="name-error" class="error invalid-feedback">@error('contract_start'){{ $message }}@enderror</em>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">End Date <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input class="form-control js-datepicker @if($errors->has('contract_end')) is-invalid @endif" id="contract_end" type="text" name="contract_end" placeholder="End Date" value="{{ old('contract_end') == null ? date('d-M-Y', strtotime($vendor_contract_master->contract_end)) : old('contract_end') }}" autocomplete="off">
                                <em id="name-error" class="error invalid-feedback">@error('contract_end'){{ $message }}@enderror</em>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button class="btn btn-primary float-right" type="submit">
                        Save
                    </button>
                    <a class="btn btn-secondary" href="{{ route('admin.vendor.contract', [Auth::user()->group->code, Crypt::encryptString('contract')]) }}">Back</a>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $('.select2').select2({
        allowClear: false,
        theme: 'bootstrap',
        width: "100%"
    });

    $("input[data-type='currency']").on({
        keyup: function() {
            formatCurrency($(this));
        }
    });

    function formatNumber(n) {
        return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }


    function formatCurrency(input, blur) {

        var input_val = input.val();
        if (input_val === "") { return; }
        var original_len = input_val.length;
        var caret_pos = input.prop("selectionStart");

        if (input_val.indexOf(".") >= 0) {

            var decimal_pos = input_val.indexOf(".");
            var left_side = input_val.substring(0, decimal_pos);
            left_side = formatNumber(left_side);
            input_val = left_side;

        } else {

            input_val = formatNumber(input_val);
            input_val = input_val;

            if (blur === "blur") {
                input_val;
            }
        }

        input.val(input_val);

        var updated_len = input_val.length;
        caret_pos = updated_len - original_len + caret_pos;
        input[0].setSelectionRange(caret_pos, caret_pos);
    }

    $('.js-datepicker').daterangepicker({
        "singleDatePicker": true,
        "showDropdowns": true,
        "autoUpdateInput": false,
        locale: {
            format: 'DD-MMM-Y'
        },
    });

    var myCalendar = $('.js-datepicker');
    var isClick = 0;

    $(window).on('click',function(){
        isClick = 0;
    });

    $(myCalendar).on('apply.daterangepicker',function(ev, picker){
        isClick = 0;
        $(this).val(picker.startDate.format('DD-MMM-Y'));

    });

    $('.js-btn-calendar').on('click',function(e){
        e.stopPropagation();

        if(isClick === 1) isClick = 0;
        else if(isClick === 0) isClick = 1;

        if (isClick === 1) {
            myCalendar.focus();
        }
    });

    $(myCalendar).on('click',function(e){
        e.stopPropagation();
        isClick = 1;
    });

    $('.daterangepicker').on('click',function(e){
        e.stopPropagation();
    });
</script>
@endpush
