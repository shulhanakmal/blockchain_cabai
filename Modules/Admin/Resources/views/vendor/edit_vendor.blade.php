@extends('admin::layouts.app')
@section('content')
<ol class="breadcrumb">
    <li class="breadcrumb-item">Edit Seller</li>
</ol>
<div class="container-fluid" style="margin-top:16px;">
    <div class="animated fadeIn">
        <form id="forms" method="post" action="{{ route('admin.vendor.edit_post', [Auth::user()->group->code]) }}" enctype="multipart/form-data" role="form">
            <div class="card">
                {{ csrf_field() }}
                <input id="vendor_master_id" name="vendor_master_id" type="hidden" value="{{ $vendor_master->id }}">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label style="font-weight: bold; font-size: 14px;">Seller Name <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input class="form-control @if($errors->has('vendor_name')) is-invalid @endif" id="vendor_name" type="text" name="vendor_name" placeholder="Seller Name" value="{{ old('vendor_name') == null ? isset($vendor_master->vendor_name) ? $vendor_master->vendor_name : '' : old('vendor_name') }}" autocomplete="off">
                                <em id="name-error" class="error invalid-feedback">@error('vendor_name'){{ $message }}@enderror</em>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label style="font-weight: bold; font-size: 14px;">Email <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input class="form-control @if($errors->has('email')) is-invalid @endif" id="vendor_name" type="email" name="email" placeholder="Email" value="{{ old('email') == null ? isset($vendor_master->email) ? $vendor_master->email : '' : old('email') }}" autocomplete="off">
                                <em id="name-error" class="error invalid-feedback">@error('email'){{ $message }}@enderror</em>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Seller Number <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input class="form-control @if($errors->has('vendor_number')) is-invalid @endif" id="vendor_number" type="text" name="vendor_number" placeholder="Seller Number" value="{{ old('vendor_number') == null ? isset($vendor_master->vendor_number) ? $vendor_master->vendor_number : '' : old('vendor_number') }}" autocomplete="off">
                                <em id="name-error" class="error invalid-feedback">@error('vendor_number'){{ $message }}@enderror</em>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Company Code <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input class="form-control @if($errors->has('company_code')) is-invalid @endif" id="company_code" type="text" name="company_code" placeholder="Company Code" value="{{ old('company_code') == null ? isset($vendor_master->vendor_code_master) ? $vendor_master->vendor_code_master->company_code : '' : old('company_code') }}" autocomplete="off">
                                <em id="name-error" class="error invalid-feedback">@error('company_code'){{ $message }}@enderror</em>
                            </div>
                        </div>
                    </div>

                    <div class="row" style="display: none;">
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Inamart Code <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input class="form-control @if($errors->has('inamart_code')) is-invalid @endif" id="inamart_code" type="text" name="inamart_code" placeholder="Inamart Code" value="{{ $vendor_master->vendor_code_master->inamart_code }}" readonly>
                                <em id="name-error" class="error invalid-feedback">Enter Inamart Code</em>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Address<span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <textarea class="form-control @if($errors->has('address')) is-invalid @endif" id="address" rows="9" name="address" placeholder="Address" autocomplete="off">{{ old('address') == null ? isset($vendor_master->vendor_address_master->street) ? $vendor_master->vendor_address_master->street : '' : old('address') }}</textarea>
                                <em id="name-error" class="error invalid-feedback">@error('address'){{ $message }}@enderror</em>
                            </div>
                        </div>
                        <div class="col-md-6 row">
                            <div class="col-md-12">
                                <label style="font-weight: bold; font-size: 14px;">Region <span style="color: red;">(*)</span></label>
                                <div class="form-group">
                                    <input class="form-control @if($errors->has('region')) is-invalid @endif" id="region" type="text" name="region" placeholder="Region" value="{{ old('region') == null ? isset($vendor_master->vendor_address_master->region) ? $vendor_master->vendor_address_master->region : '' : old('region') }}" autocomplete="off">
                                    <em id="name-error" class="error invalid-feedback">@error('region'){{ $message }}@enderror</em>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label style="font-weight: bold; font-size: 14px;">City <span style="color: red;">(*)</span></label>
                                <div class="form-group">
                                    <input class="form-control @if($errors->has('city')) is-invalid @endif" id="city" type="text" name="city" placeholder="City" value="{{ old('city') == null ? isset($vendor_master->vendor_address_master->city) ? $vendor_master->vendor_address_master->city : '' : old('city') }}" autocomplete="off">
                                    <em id="name-error" class="error invalid-feedback">@error('city'){{ $message }}@enderror</em>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label style="font-weight: bold; font-size: 14px;">Zip Code <span style="color: red;">(*)</span></label>
                                <div class="form-group">
                                    <input class="form-control @if($errors->has('postcode')) is-invalid @endif" id="postcode" type="text" name="postcode" placeholder="Zip Code" value="{{ old('postcode') == null ? isset($vendor_master->vendor_address_master->postcode) ? $vendor_master->vendor_address_master->postcode : '' : old('postcode') }}" autocomplete="off">
                                    <em id="name-error" class="error invalid-feedback">@error('postcode'){{ $message }}@enderror</em>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Phone <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input class="form-control @if($errors->has('telephone')) is-invalid @endif" id="telephone" type="text" name="telephone" placeholder="Phone" value="{{ old('telephone') == null ? isset($vendor_master->vendor_address_master->telephone) ? $vendor_master->vendor_address_master->telephone : '' : old('telephone') }}" autocomplete="off">
                                <em id="name-error" class="error invalid-feedback">@error('telephone'){{ $message }}@enderror</em>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Fax</label>
                            <div class="form-group">
                                <input class="form-control @if($errors->has('fax')) is-invalid @endif" id="fax" type="text" name="fax" placeholder="Fax" value="{{ old('fax') == null ? isset($vendor_master->vendor_address_master->fax) ? $vendor_master->vendor_address_master->fax : '' : old('fax') }}" autocomplete="off">
                                <em id="name-error" class="error invalid-feedback">@error('fax'){{ $message }}@enderror</em>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label style="font-weight: bold; font-size: 14px;">Role <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <select class="form-control select2-multiple select2-hidden-accessible @if($errors->has('roles')) is-invalid @endif" data-placeholder="Choose Role" name="roles[]" id="roles" multiple="" data-select2-id="select2-2" tabindex="-1" aria-hidden="true">
                                    <option></option>
                                    @foreach($roles as $r)
                                    @if($r->name != 'Masjid')
                                    <option
                                        value="{{ $r->id }}" data-id="{{ $r->name }}"
                                        @if(old("roles"))
                                            {{ (in_array($r->id, old("roles")) ? "selected":"") }}
                                        @else
                                            {{ (in_array($r->id, $vendor_master->roles->pluck('id')->all()) ? "selected":"") }}
                                        @endif
                                    >{{ $r->name }}</option>
                                    @endif
                                    @endforeach
                                </select>
                                <em id="name-error" class="error invalid-feedback">@error('roles'){{ $message }}@enderror</em>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Status <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <div class="input-group">
                                    <label class="col-form-label switch switch-md switch-label switch-pill switch-primary">
                                        <input class="switch-input" type="checkbox" id="status_vendor" name="status_vendor" onclick="checkBox()" @if($vendor_master->status == 1) value="1" checked @else value="0" @endif>
                                        <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
                                    </label>
                                    &nbsp;&nbsp;&nbsp;<label id="status_name" name="status_name" class="col-form-label">@if($vendor_master->status == 1) Active @else Inactive @endif</label>
                                </div>
                                <em id="firstname-error" class="error invalid-feedback">@error('status_vendor'){{ $message }}@enderror</em>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button class="btn btn-primary float-right" type="submit" style="margin-left:10px;">
                        Save
                    </button>
                    <a class="btn btn-primary float-right" data-toggle="modal" data-target="#password-modal" style="color: white;">
                        Update Password
                    </a>
                    <a class="btn btn-secondary" href="{{ route('admin.vendor', [Auth::user()->group->code, Crypt::encryptString('adminDaftarVendor')]) }}">Back</a>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="password-modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-primary modal-md">
        <div class="modal-content">
            <form id="form-ubah-password" method="post" action="{{ route('admin.vendor.edit.ubah_password', [Auth::user()->group->code]) }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-header">
                    <h4 class="modal-title">Update Password</h4>
                </div>
                <div class="modal-body">
                    <input id="vendor_master_id" name="vendor_master_id" type="hidden" value="{{ $vendor_master->id }}">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="telephone">Password <i class="text-danger">(*)</i></label>
                        <div class="col-md-7">
                            <input class="form-control" id="modal_password" name="modal_password" type="password" autocomplete="false">
                        </div>
                        <div class="col-md-1">
                            <a class="btn btn-secondary pull-left" style="margin-top:5px;" id="showPassword">
                                <i class="fa fa-eye"></i>
                            </a>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="telephone">Confirmation Password <i class="text-danger">(*)</i></label>
                        <div class="col-md-7">
                            <input class="form-control" id="modal_konfirmasi_password" name="modal_konfirmasi_password" type="password" autocomplete="false">
                        </div>
                        <div class="col-md-1">
                            <a class="btn btn-secondary pull-left" style="margin-top:5px;" id="showKonfirmasiPassword">
                                <i class="fa fa-eye"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary pull-left" type="button" data-dismiss="modal" style="color: white;">Close</button>
                    <button class="btn btn-primary pull-right" type="button" onclick="simpanUbahPassword()">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $('#roles').select2({
        allowClear: false,
        theme: 'bootstrap',
        width: "100%"
    });

    function checkBox() {
        if (document.getElementById('status_vendor').checked) {
            document.getElementById('status_name').innerHTML = "Active";
            document.getElementById('status_vendor').value = 1;
        } else {
            document.getElementById('status_name').innerHTML = "Inactive";
            document.getElementById('status_vendor').value = 0;
        }
    }

    function simpanUbahPassword() {
        if($('#modal_password').val() == '') {
            toastr.error('Password wajib diisi', 'Informasi!')
        } else if($('#modal_konfirmasi_password').val() == '') {
            toastr.error('Konfirmasi password wajib diisi', 'Informasi!')
        } else if($('#modal_konfirmasi_password').val() != $('#modal_password').val()) {
            toastr.error('Konfirmasi password salah', 'Informasi!')
        } else {
            $('#form-ubah-password').submit();
        }
    }

    $('#password-modal').on('shown.bs.modal', function (e) {
        var click = false;
        $('#showPassword').on('click', function() {
            if(!click) {
                $('#modal_password').attr('type',"text");
                click = true;
            } else {
                $('#modal_password').attr('type',"password");
                click = false;
            }
        });
        $('#showKonfirmasiPassword').on('click', function() {
            if(!click) {
                $('#modal_konfirmasi_password').attr('type',"text");
                click = true;
            } else {
                $('#modal_konfirmasi_password').attr('type',"password");
                click = false;
            }
        });
    })
</script>
@endpush
