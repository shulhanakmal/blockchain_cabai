@extends('admin::layouts.app')
@section('content')
<ol class="breadcrumb">
    <li class="breadcrumb-item">Add New Group</li>
</ol>
<div class="container-fluid">
    <div class="animated fadeIn">
        <form id="forms" method="post" action="{{ route('admin.group.edit_post', [Auth::user()->group->code]) }}" enctype="multipart/form-data" role="form">
            {{ csrf_field() }}
            <input id="id" name="id" type="hidden" value="{{ $group->id }}">
            <div class="card">
                <div class="card-body">
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label" for="customer_group_code">Group Name <i class="text-danger">(*)</i></label>
                        <div class="col-md-10">
                            <input class="form-control @if($errors->has('customer_group_code')) is-invalid @endif" id="customer_group_code" name="customer_group_code" type="text" value="{{ $group->customer_group_code }}">
                            <em id="firstname-error" class="error invalid-feedback">Enter Group Name</em>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button class="btn btn-primary float-right" type="submit">
                        Save
                    </button>
                    <a class="btn btn-danger" href="{{ route('admin.group', [Auth::user()->group->code, Crypt::encryptString('group')]) }}">Back</a>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@push('scripts')
<script>

    $('.select2').select2({
        allowClear: false,
        theme: 'bootstrap',
        width: "100%",
        placeholder: "Pilih Wilayah",
    });

    $(document).ready(function() {
        document.getElementById('status_customer').value = 0;
    });

    function checkBox() {
        if (document.getElementById('status_customer').checked) {
            document.getElementById('status_name').innerHTML = "Active";
            document.getElementById('status_customer').value = 1;
        } else {
            document.getElementById('status_name').innerHTML = "Inactive";
            document.getElementById('status_customer').value = 0;
        }
    }
</script>
@endpush
