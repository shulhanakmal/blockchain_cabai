@extends('admin::layouts.app')
@section('content')
<ol class="breadcrumb row">
    <li class="col-md-12 breadcrumb-item">
        Group List
    </li>
</ol>
<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="card">
            <div class="card-body">
                <div class="pull-right">
                    <form class="form-inline pull-right" action="{{ url()->current() }}">
                        <a href="{{ route('admin.customer', [Auth()->user()->group->code, Crypt::encryptString('customer')]) }}" class="btn btn-danger" style='color:#fff'> Reset</a>&nbsp;
                        <input class="form-control" type="text" name="keyword" placeholder="Search.." value="{{ $keyword }}">&nbsp;
                        <button class="btn btn-primary" type="submit">Search</button>&nbsp;
                        <a href="{{ route('admin.group.create', [Auth::user()->group->code, Crypt::encryptString('group')]) }}" class="btn btn-primary">
                            <span class="span">&nbsp;Add New</span></i>
                        </a>
                    </form>
                </div>
                <div class="white-box">
                    <div class="table-responsive">
                        <table id="tabel-customer" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="10%" class="text-center" style="vertical-align:middle">No</th>
                                    <th class="text-center" style="vertical-align:middle">Name</th>
                                    <th width="15%" class="text-center" style="vertical-align:middle">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($group as $c)
                                <tr>
                                    <td class="text-center">{{ ++$i }}</td>
                                    <td>{{ $c->customer_group_code }}</td>

                                    <td style="vertical-align:middle" align="center">
                                        <a class="btn btn-outline-primary" title="Detail" href="{{ route('admin.group.edit', [Auth::user()->group->code, Crypt::encryptString($c->id)]) }}">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        {{-- <a class="btn btn-outline-primary" title="Hapus">
                                            <i class="fa fa-trash-o"></i>
                                        </a> --}}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {!! $group->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    @foreach ($errors->all() as $error)
        toastr.error("{{$error}}")
    @endforeach

    $(document).ready(function() {
        $('#tabel-customer .btn').tooltip();
    })
</script>
@endpush
