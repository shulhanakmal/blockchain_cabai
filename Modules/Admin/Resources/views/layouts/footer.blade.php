@if(!Auth::guest())
<footer class="app-footer">
@else
<footer class="app-footer" style="width:100%;margin:0;">
@endif
    <div>
        <a href="https://coreui.io/pro/">E-Catalogue</a>
        <span>&copy; 2021 Inamart.</span>
    </div>
</footer>
