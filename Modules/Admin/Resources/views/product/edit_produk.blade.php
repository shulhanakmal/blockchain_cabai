@extends('admin::layouts.app')
@section('content')
<ol class="breadcrumb">
    <li class="breadcrumb-item">Edit Product</li>
</ol>
<div class="container-fluid" style="margin-top:16px;">
    <div class="animated fadeIn">
        <form id="forms" method="post" action="{{ route('admin.product.edit_post', [Auth::user()->group->code]) }}" enctype="multipart/form-data" role="form">
            {{ csrf_field() }}
            <input id="product_id" name="product_id" type="hidden" value="{{ $product->product_id }}">
            <input id="id" name="id" type="hidden" value="{{ $product->id }}">
            <div class="card">
                <div class="card-header">
                    <h4>Detail Product</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label style="font-weight: bold; font-size: 14px;">Bacth <span style="color: red;">(*)</span></label>
                                <select class="custom-select select2 val-custom form-control @if($errors->has('batch')) is-invalid @endif" name="batch" id="batch" data-placeholder="Choose Batch">
                                    <option></option>
                                    @foreach($batch as $b)
                                    <option value="{{ $b->id }}"
                                        @if(Input::old('batch') != null)
                                            @if(Input::old('batch') == $b->id)
                                                selected
                                            @endif
                                        @else
                                            @if($product->product->batch_id == $b->id)
                                            selected
                                            @endif
                                        @endif
                                    >{{ $b->batch_id }}</option>
                                    @endforeach
                                </select>
                                <em id="firstname-error" class="error invalid-feedback">@error('batch'){{ $message }}@enderror</em>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label style="font-weight: bold; font-size: 14px;">Product Group <span style="color: red;">(*)</span></label>
                                <select class="form-control select2-multiple select2-hidden-accessible @if($errors->has('product_group')) is-invalid @endif" data-placeholder="Choose Product Group" name="product_group[]" id="product_group" multiple="" data-select2-id="select2-2" tabindex="-1" aria-hidden="true">
                                    <option></option>
                                    @foreach($product_group_rules as $pgr)
                                    <option
                                        value="{{ $pgr->id }}""
                                        @if(old("product_group"))
                                            {{ (in_array($pgr->id, old("product_group")) ? "selected":"") }}
                                        @else
                                            {{ (in_array($pgr->id, $product->product->product_group->pluck('rule_id')->all()) ? "selected":"") }}
                                        @endif
                                    >{{ $pgr->rule_name }}</option>
                                    @endforeach
                                </select>
                                <em id="firstname-error" class="error invalid-feedback">@error('product_group'){{ $message }}@enderror</em>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label style="font-weight: bold; font-size: 14px;">Product Name <span style="color: red;">(*)</span></label>
                        </div>
                        <div class="form-group col-md-12">
                            <input class="form-control @if($errors->has('name')) is-invalid @endif" id="name" type="text" name="name" placeholder="Product Name" value="{{ old('name') == null ? $product->product->name : old('name') }}">
                            <em id="name-error" class="error invalid-feedback">@error('name'){{ $message }}@enderror</em>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label style="font-weight: bold; font-size: 14px;">Description <span style="color: red;">(*)</span></label>
                        </div>
                        <div class="form-group col-md-12">
                            <input class="form-control @if($errors->has('description')) is-invalid @endif" id="descriptions" type="text" name="description" placeholder="Description" value="{{ old('description') == null ? $product->product->description : old('description') }}">
                            <em id="name-error" class="error invalid-feedback">@error('description'){{ $message }}@enderror</em>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label style="font-weight: bold; font-size: 14px;">Short Description <span style="color: red;">(*)</span></label>
                        </div>
                        <div class="form-group col-md-12">
                            <input class="form-control @if($errors->has('short_description')) is-invalid @endif" id="short_descriptions" type="text" name="short_description" placeholder="Short Description" value="{{ old('short_description') == null ? $product->product->short_description : old('short_description') }}">
                            <em id="name-error" class="error invalid-feedback">@error('short_description'){{ $message }}@enderror</em>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label style="font-weight: bold; font-size: 14px;">Currency <span style="color: red;">(*)</span></label>
                                <select class="custom-select select2 val-custom form-control @if($errors->has('currency')) is-invalid @endif" name="currency" id="currency" data-placeholder="Choose Currency" onchange="CurrencyChange()">
                                    <option></option>
                                    @foreach($currency as $c)
                                    <option value="{{ $c->id }}"
                                        @if(Input::old('currency') != null)
                                            @if(Input::old('currency') == $c->id)
                                                selected
                                            @endif
                                        @else
                                            @if($c->code == $defaultCurrency) selected @endif
                                        @endif

                                    >{{$c->name}} - {{$c->code}}</option>
                                    @endforeach
                                    <option value="Otr">Lainnya</option>
                                </select>
                                <em id="firstname-error" class="error invalid-feedback">@error('currency'){{ $message }}@enderror</em>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Price <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input class="form-control @if($errors->has('price')) is-invalid @endif" id="price" type="text" name="price" placeholder="Price" value="{{ old('price') == null ? number_format($product->price, 0) : old('price') }}" data-type="currency">
                                <em id="name-error" class="error invalid-feedback">@error('price'){{ $message }}@enderror</em>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" name="currencyOtr" id="fieldCurr" style="display: none; text-transform:uppercase;" maxlength="5" class="form-control" value="" placeholder="Enter Currency Code">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Stock <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input class="form-control @if($errors->has('stock')) is-invalid @endif" id="stock" type="text" name="stock" placeholder="Stock" value="{{ old('stock') == null ? $product->stock : old('stock') }}" data-type="currency">
                                <em id="name-error" class="error invalid-feedback">@error('stock'){{ $message }}@enderror</em>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label style="font-weight: bold; font-size: 14px;">Unit <span style="color: red;">(*)</span></label>
                                <select class="custom-select select2 val-custom form-control @if($errors->has('unit')) is-invalid @endif" name="unit" id="unit" data-placeholder="Choose Unit">
                                    <option></option>
                                    @foreach($unit as $u)
                                    <option value="{{ $u->id }}"
                                        @if(Input::old('unit') != null)
                                            @if(Input::old('unit') == $u->id)
                                                selected
                                            @endif
                                        @else
                                            @if($product->product->unit == $u->id)
                                            selected
                                            @endif
                                        @endif
                                    >{{ $u->nama_unit }}</option>
                                    @endforeach
                                </select>
                                <em id="firstname-error" class="error invalid-feedback">@error('unit'){{ $message }}@enderror</em>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">SKU <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input class="form-control @if($errors->has('sku')) is-invalid @endif" id="sku" type="text" name="sku" placeholder="SKU" value="{{ old('sku') == null ? $product->product->sku : old('sku') }}">
                                <em id="name-error" class="error invalid-feedback">@error('sku'){{ $message }}@enderror</em>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Seller SKU <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input class="form-control @if($errors->has('vendor_sku')) is-invalid @endif" id="vendor_sku" type="text" name="vendor_sku" placeholder="Exporter SKU" value="{{ old('vendor_sku') == null ? $product->product->vendor_sku : old('vendor_sku') }}">
                                <em id="name-error" class="error invalid-feedback">@error('vendor_sku'){{ $message }}@enderror</em>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        @php
                            $cat = new \App\Category\Category;
                            $cat = $cat->setConnection(Auth::user()->group->katalog);
                            $category = $cat->where('level', 2)->whereIn('id', $category_product)->get();
                            $subcategory = $cat->where('level', 3)->whereIn('id', $category_product)->get();
                            $subsubcategory = $cat->where('level', 4)->whereIn('id', $category_product)->get();
                        @endphp
                        <div class="col-md-6">
                            <div class="form-group">
                                <label style="font-weight: bold; font-size: 14px;">Category <span style="color: red;">(*)</span></label>
                                <select class="custom-select select2 val-custom form-control @if($errors->has('category')) is-invalid @endif" name="category" id="category" data-placeholder="Choose Category" disabled>
                                    @if(!$category->isEmpty()) <option selected="" disabled value="{{ $category[0]->id }}">{{ $category[0]->name }}</option> @endif
                                </select>
                                <em id="firstname-error" class="error invalid-feedback">@error('category'){{ $message }}@enderror</em>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label style="font-weight: bold; font-size: 14px;">Sub Category <span style="color: red;">(*)</span></label>
                                <select class="custom-select select2 val-custom form-control @if($errors->has('subcategory')) is-invalid @endif" name="subcategory" id="subcategory" data-placeholder="Choose Sub Category" onchange="myFunctionSubCategory()">
                                    <option></option>
                                    @foreach($subcategory as $sc)
                                    <option value="{{ $sc->id }}"
                                        @if(Input::old('subcategory') != null)
                                            @if(Input::old('subcategory') == $sc->id)
                                                selected
                                            @endif
                                        @else
                                            @if($subcategory[0]->id == $sc->id)
                                                selected
                                            @endif
                                        @endif
                                    >{{ $sc->name }}</option>
                                    @endforeach
                                </select>
                                <em id="firstname-error" class="error invalid-feedback">@error('subcategory'){{ $message }}@enderror</em>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Weight <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input class="form-control @if($errors->has('weight')) is-invalid @endif" id="weight" type="text" name="weight" placeholder="Weight" value="{{ old('weight') == null ? $product->product->weight : old('weight') }}">
                                <em id="name-error" class="error invalid-feedback">@error('weight'){{ $message }}@enderror</em>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Production Date <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <input type="text" autocomplete="off" class="form-control js-datepicker @if($errors->has('tgl_produksi')) is-invalid @endif" name="tgl_produksi" id="tgl_produksi" value="{{ old('tgl_produksi') == null ? date('Y-m-d', strtotime($product->product->tgl_produksi)) : old('tgl_produksi') }}">
                                <em id="name-error" class="error invalid-feedback">@error('tgl_produksi'){{ $message }}@enderror</em>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label style="font-weight: bold; font-size: 14px;">Status <span style="color: red;">(*)</span></label>
                            <div class="form-group">
                                <div class="input-group">
                                    <label class="col-form-label switch switch-md switch-label switch-pill switch-primary">
                                        <input class="switch-input" type="checkbox" id="status_product" name="status_product" onclick="checkBox()" @if($product->status == 1) value="1" checked @else value="0" @endif>
                                        <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
                                    </label>
                                    &nbsp;&nbsp;&nbsp;<label id="status_name" name="status_name" class="col-form-label">@if($product->status == 1) Active @else Inactive @endif</label>
                                </div>
                                <em id="firstname-error" class="error invalid-feedback">@error('status_name'){{ $message }}@enderror</em>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h4>
                        Shipment
                        <button type="button" id="add_product_shipping" class="btn btn-outline-primary pull-right">
                            <i class="fa fa-plus-square"></i>
                        </button>
                    </h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                        <fieldset class="form-group col-md-12">
                            <table class="table table-bordered col-md-12" id="table_product_shipping">
                                <tbody>
                                    <?php
                                        $product_shipping = new \App\Product\ProductShipping;
                                        $product_shipping = $product_shipping->where('product_id', $product->product_id)->get();
                                    ?>
                                    @foreach($product_shipping as $ps)
                                    <tr class="tr">
                                        <td width="47.5%">
                                            <input type="text" class="form-control" id="name_biaya[]" name="name_biaya[]" value="{{ $ps->name }}">
                                        </td>
                                        <td width="47.5%">
                                            <input type="text" class="form-control" data-type="currency" id="price_biaya[]" name="price_biaya[]" value="{{ number_format($ps->price, 0) }}">
                                        </td>
                                        <td width="5%">
                                            <button type="button" class="btn btn-danger hapus"><i class="fa fa-trash"></i></button>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </fieldset>
                    </div>
                </div>
                <div class="card-footer">
                    <button class="btn btn-primary float-right" type="submit">
                        Save
                    </button>
                    <a class="btn btn-secondary" href="{{ route('admin.product', [Auth()->user()->group->code, Crypt::encryptString('product')]) }}">Back</a>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/webrtc-adapter/3.3.3/adapter.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.1.10/vue.min.js"></script>
<script type="text/javascript" src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
<script>
    // @foreach ($errors->all() as $error)
    //     toastr.error("{{$error}}")
    // @endforeach

    function CurrencyChange() {
        currency = document.getElementById('currency').value;
        if(currency === "Otr"){
            $('#fieldCurr').prop('required',true);
            $('#fieldCurr').show();
        } else {
            $('#fieldCurr').prop('required',false);
            $('#fieldCurr').hide();
        }
    }

    function myFunctionSubCategory() {
        subcategory_id = document.getElementById('subcategory').value;
        $.ajax({
            type : 'GET',
            url : '{{ route('vendor.product.subcategory', [Auth::user()->group->code]) }}',
            data : {
                id : subcategory_id
            },
            success:function(response){
                response = JSON.parse(response);
                var html = `<select class="custom-select val-custom form-control select2" data-placeholder="Sub Sub Kategori" name="subsubcategory" id="subsubcategory">
                                <option></option>`;
                for(var i = 0; i < response.length; i++) {
                    html+=`     <option value="` + response[i].id + `">` + response[i].name + `</option>`;
                }
                html+=`     </select>`;

                document.getElementById("idsubsubcategory").innerHTML = html;

                $('.select2').select2({
                    allowClear: false,
                    theme: 'bootstrap',
                    width: "100%"
                });
            }
        });
    }

    function checkBox() {
        if (document.getElementById('status_product').checked) {
            document.getElementById('status_name').innerHTML = "Active";
        document.getElementById('status_product').value = 1;
        } else {
            document.getElementById('status_name').innerHTML = "Inactive";
            document.getElementById('status_product').value = 0;
        }
    }

    $("#add_product_shipping").click(function() {
        var html = '';
        html += '<tr class="tr">';
        html +=     '<td width="47.5%"><input type="text" class="form-control" id="name_biaya[]" name="name_biaya[]" placeholder="Place"></td>';
        html +=     '<td width="47.5%"><input type="text" class="form-control" data-type="currency" id="price_biaya[]" name="price_biaya[]" placeholder="Price"></td>';
        html +=     '<td width="5%"><button type="button" class="btn btn-danger hapus"><i class="fa fa-trash"></i></button></td>';
        html += '</tr>';
        $("#table_product_shipping tbody").append(html);

        $("input[data-type='currency']").on({
            keyup: function() {
                formatCurrency($(this));
            }
        });
    });

    $('body').on('click', '.hapus', function() {
        $(this).parents('.tr').remove();
    });

    $(document).ready(function () {
        $('.js-datepicker').daterangepicker({
            "singleDatePicker": true,
            "showDropdowns": true,
            "autoUpdateInput": false,
            locale: {
                format: 'YYYY-MM-DD'
            },
        });

        var myCalendar = $('.js-datepicker');
        var isClick = 0;

        $(window).on('click',function(){
            isClick = 0;
        });

        $(myCalendar).on('apply.daterangepicker',function(ev, picker){
            isClick = 0;
            $(this).val(picker.startDate.format('YYYY-MM-DD'));

        });

        $('.js-btn-calendar').on('click',function(e){
            e.stopPropagation();

            if(isClick === 1) isClick = 0;
            else if(isClick === 0) isClick = 1;

            if (isClick === 1) {
                myCalendar.focus();
            }
        });

        $(myCalendar).on('click',function(e){
            e.stopPropagation();
            isClick = 1;
        });

        $('.daterangepicker').on('click',function(e){
            e.stopPropagation();
        });

        $('#product_group').select2({
            allowClear: false,
            theme: 'bootstrap',
            width: "100%",
            placeholder: "Pilih Group",
        });
        $(document).on('change', '.btn-file :file', function() {
		var input = $(this),
			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		input.trigger('fileselect', [label]);
		});

		$('.btn-file :file').on('fileselect', function(event, label) {

		    var input = $(this).parents('.input-group').find(':text'),
		        log = label;

		    if( input.length ) {
		        input.val(log);
		    } else {
		        if( log ) alert(log);
		    }

		});
		function readURL(input) {
		    if (input.files && input.files[0]) {
		        var reader = new FileReader();

		        reader.onload = function (e) {
		            $('#img-upload').attr('src', e.target.result);
		        }

		        reader.readAsDataURL(input.files[0]);
		    }
		}

		$("#imgInp").change(function(){
		    readURL(this);
		});

        $(document).on('change', '.btn-file-2 :file', function() {
            var input = $(this), label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [label]);
		});

		$('.btn-file-2 :file').on('fileselect', function(event, label) {

		    var input = $(this).parents('.input-group').find(':text'),
		        log = label;

		    if( input.length ) {
		        input.val(log);
		    } else {
		        if( log ) alert(log);
		    }

		});
		function readURL2(input) {
		    if (input.files && input.files[0]) {
		        var reader = new FileReader();

		        reader.onload = function (e) {
		            $('#img-upload-2').attr('src', e.target.result);
		        }

		        reader.readAsDataURL(input.files[0]);
		    }
		}

		$("#imgInp2").change(function(){
		    readURL2(this);
		});

        $(document).on('change', '.btn-file-3 :file', function() {
            var input = $(this), label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [label]);
		});

		$('.btn-file-3 :file').on('fileselect', function(event, label) {

		    var input = $(this).parents('.input-group').find(':text'),
		        log = label;

		    if( input.length ) {
		        input.val(log);
		    } else {
		        if( log ) alert(log);
		    }

		});
		function readURL3(input) {
		    if (input.files && input.files[0]) {
		        var reader = new FileReader();

		        reader.onload = function (e) {
		            $('#img-upload-3').attr('src', e.target.result);
		        }

		        reader.readAsDataURL(input.files[0]);
		    }
		}

		$("#imgInp3").change(function(){
		    readURL3(this);
		});
    });

    $('.select2').select2({
        allowClear: false,
        theme: 'bootstrap',
        width: "100%",
        placeholder: "Pilih Wilayah",
    });

    $('#currency').select2({
        allowClear: false,
        theme: 'bootstrap',
        width: "100%",
        placeholder: "Pilih Currency",
    });

    $("input[data-type='currency']").on({
        keyup: function() {
            formatCurrency($(this));
        }
    });

    function formatNumber(n) {
        return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }

    function formatCurrency(input, blur) {

        var input_val = input.val();
        if (input_val === "") { return; }
        var original_len = input_val.length;
        var caret_pos = input.prop("selectionStart");

        if (input_val.indexOf(".") >= 0) {

            var decimal_pos = input_val.indexOf(".");
            var left_side = input_val.substring(0, decimal_pos);
            left_side = formatNumber(left_side);
            input_val = left_side;

        } else {

            input_val = formatNumber(input_val);
            input_val = input_val;

            if (blur === "blur") {
                input_val;
            }
        }

        input.val(input_val);

        var updated_len = input_val.length;
        caret_pos = updated_len - original_len + caret_pos;
        input[0].setSelectionRange(caret_pos, caret_pos);
    }
</script>
@endpush
