@extends('admin::layouts.app')
@section('content')
<ol class="breadcrumb row">
    <li class="col-md-12 breadcrumb-item">
        Product List
    </li>
</ol>
<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="card">
            <div class="card-body">
                <div class="pull-left">
                    <form class="form-inline pull-right" action="{{ url()->current() }}">
                        <a href="{{ route('admin.product', [Auth::user()->group->code, Crypt::encryptString('product')]) }}" class="btn btn-secondary"> Reset</a>&nbsp;
                        <input class="form-control" type="text" name="keyword" placeholder="Search.." value="{{ $keyword }}">&nbsp;
                        <button class="btn btn-primary" type="submit">Search</button>
                    </form>
                </div>
                <div class="white-box">
                    <div class="table-responsive">
                        <table id="tabel-product" class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th width="10%" class="text-center" style="vertical-align:middle">No</th>
                                    <th class="text-center" style="vertical-align:middle">Product Name</th>
                                    <th class="text-center" style="vertical-align:middle">Seller</th>
                                    <th class="text-center" style="vertical-align:middle">Status</th>
                                    <th width="15%" class="text-center" style="vertical-align:middle">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($product as $key => $p)
                                <tr>
                                    <td class="text-center">{{ ++$i }}</td>
                                    <td>{{ $p->product->name }}</td>
                                    <td>{{ $p->vendor->vendor_name }}</td>
                                    <td class="text-center" style="vertical-align:middle">
                                        @if($p->status == 0)
                                            <label style="vertical-align:middle;color:white;" class="badge badge-danger text-center">Inactive</label>
                                        @else
                                            <label style="vertical-align:middle" class="badge badge-primary text-center">Active</label>
                                        @endif
                                    </td>
                                    <td style="vertical-align:middle" align="center">
                                        <input type="hidden" name="description{{$key}}" id="description{{$key}}" value="{!! $p->product->short_description !!}"/>
                                        <a class="btn btn-outline-primary" title="Detail" href="{{ route('admin.product.edit', [Auth::user()->group->code, Crypt::encryptString($p->product_id)]) }}">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {!! $product->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    @foreach ($errors->all() as $error)
        toastr.error("{{$error}}")
    @endforeach

    $(document).ready(function() {
        $('#tabel-product .btn').tooltip();
    })
</script>
@endpush
