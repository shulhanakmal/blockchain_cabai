@extends('admin::layouts.app')
@section('content')
<ol class="breadcrumb">
    <li class="breadcrumb-item">Add New Product Group</li>
</ol>
<div class="container-fluid" style="margin-top:16px;">
    <div class="animated fadeIn">
        <form id="forms" method="post" action="{{ route('admin.product_group_create_post', [Auth::user()->group->code]) }}" enctype="multipart/form-data" role="form">
            {{ csrf_field() }}
            <div class="card">
                <div class="card-body">
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label" for="first_name">Name</label>
                        <div class="col-md-10">
                            <input class="form-control" id="first_name" name="first_name" type="text">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label" for="email">Buyer Group</label>
                        <div class="col-md-10">
                            <select class="form-control select2" data-placeholder="Choose Customer Group" name="customer_group" id="customer_group">
                                <option></option>
                                @foreach($customer_group as $cg)
                                    <option value="{{ $cg->ids }}">{{ $cg->customer_group_code }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button class="btn btn-primary float-right" type="submit">
                        Save
                    </button>
                    <a class="btn btn-secondary" href="{{ route('admin.product_group', [Auth::user()->group->code, Crypt::encryptString('product_group')]) }}">Back</a>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@push('scripts')
<script>
    @foreach ($errors->all() as $error)
        toastr.error("{{$error}}")
    @endforeach

    $('.select2').select2({
        allowClear: false,
        theme: 'bootstrap',
        width: "100%",
        placeholder: "Pilih Wilayah",
    });

    function checkBox() {
        if (document.getElementById('status_product_group').checked) {
            document.getElementById('status_name').innerHTML = "Active";
            document.getElementById('status_product_group').value = 1;
        } else {
            document.getElementById('status_name').innerHTML = "Inactive";
            document.getElementById('status_product_group').value = 0;
        }
    }
</script>
@endpush
