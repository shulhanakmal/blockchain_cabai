@extends('admin::layouts.app')
@section('content')
<ol class="breadcrumb">
    <li class="breadcrumb-item">Edit Product Group</li>
</ol>
<div class="container-fluid" style="margin-top:16px;">
    <div class="animated fadeIn">
        <div class="card">
            <input id="product_group_rules_id" name="product_group_rules_id" type="hidden" value="{{ $product_group_rules->id }}">
            <div class="card-body">
                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="first_name">Name</label>
                    <div class="col-md-10">
                        <input class="form-control" id="first_name" name="first_name" type="text" value="{{ $product_group_rules->rule_name }}">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-2 col-form-label" for="email">Buyer Group</label>
                    <div class="col-md-10">
                        <select class="form-control select2" data-placeholder="Choose Customer Group" name="customer_group" id="customer_group">
                            <option></option>
                            @foreach($customer_group as $cg)
                                <option value="{{ $cg->ids }}"
                                    @if($product_group_rules->cust_groups == $cg->ids)
                                        selected
                                    @endif
                                >{{ $cg->customer_group_code }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-primary float-right" type="submit">
                    Save
                </button>
                <a class="btn btn-secondary" href="{{ route('admin.product_group', [Auth::user()->group->code, Crypt::encryptString('product_group')]) }}">Back</a>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    @foreach ($errors->all() as $error)
        toastr.error("{{$error}}")
    @endforeach

    $('.select2').select2({
        allowClear: false,
        theme: 'bootstrap',
        width: "100%",
        placeholder: "Pilih Wilayah",
    });

    function checkBox() {
        if (document.getElementById('status_product_group').checked) {
            document.getElementById('status_name').innerHTML = "Aktif";
            document.getElementById('status_product_group').value = 1;
        } else {
            document.getElementById('status_name').innerHTML = "Tidak Aktif";
            document.getElementById('status_product_group').value = 0;
        }
    }
</script>
@endpush
