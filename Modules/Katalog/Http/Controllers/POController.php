<?php

namespace Modules\Katalog\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Katalog\Http\Controllers\LayoutController;
use Illuminate\Http\Request;
use Response;
use Auth;
use DB;
use App\Category\Category;
use App\Product\Product;
use App\Vendor\vendor;
use App\Product\ProductVendor;
use App\Category\CategoryProduct;
use App\Lokasi\Location;
use App\Cart\Cart;
use App\Style\style;
use App\Product\ProductShipping;
use App\Cart\CartShipping;
use App\Order\Order;
use App\Order\OrderDetail;
use App\Order\OrderShipping;
use App\Order\OrderAddress;
use App\Notification\notification;
use App\PO\po;
use App\PO\PoDetail;
use App\PO\PoDetailBiaya;
use App\PO\PoTotalJumlahTanpaPpn;
use App\PO\PoTotalHargaSatuanFinalTanpaPpn;
use App\PO\PoTotalHargaTotalFinalTanpaPpn;
use App\Vendor\VendorMaster;
use PDF;
use Illuminate\Support\Facades\Crypt;
use Modules\Katalog\Repositories\MonitoringRepository;
use App\Batch;
use App\RoasterProduct;
use App\RoasterBiji;
use App\Biji;
use App\Jenis;
use App\RoasterJenis;
use App\Proses;
use App\RoasterProses;
use App\Supplier;
use App\RoasterSupplier;
use App\RoasterInventory;
use App\RoasterUnit;
use App\Unit;

class POController extends LayoutController {

    function __construct(MonitoringRepository $monitoringRepository) {
        $this->middleware('auth');
        $this->middleware('permission:customer', ['only' => ['create_po']]);
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
        $this->monitoringRepository = $monitoringRepository;
    }

    public function index(Request $request, $code, $hash) {
        $pos = Crypt::decryptString($hash);
        if($pos == 'po')
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        $po = new po;
        $po->setConnection('mysql');
        $po = $po->where('po_by', $this->user->id)->get();
        return view('katalog::monitoring.po.index',[
			'po' => $po,
            'category' => $this->get_ctg(),
            'style' => $this->get_style(),
            'code_url' => $this->user->group()->first()->code
		])->with('search', $request->input('search'));
    }

    public function detail_po(Request $request, $code, $hash_id) {
        $id = Crypt::decryptString($hash_id);
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        $po = new po;
        $po->setConnection('mysql');
        $po = $po->find($id);
        return view('katalog::monitoring.po.detail',[
			'po' => $po,
            'category' => $this->get_ctg(),
            'style' => $this->get_style(),
            'code_url' => $this->user->group()->first()->code
		])->with('search', $request->input('search'));
    }

    public function create_po(Request $request, $code, $hash) {
        $order_number = Crypt::decryptString($hash);
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        $order = new Order;
        $order->setConnection('mysql');
        $order = $this->monitoringRepository->get_order_by_order_number($order, $this->user, $order_number);
        $data = $this->monitoringRepository->get_order_detail($order);
        /*return view('katalog::monitoring.po.create_po',[*/
        return view('katalog::monitoring.po.create_po2',[
            'order' => $order,
			'data' => $data,
            'category' => $this->get_ctg(),
            'style' => $this->get_style(),
            'code_url' => $this->user->group()->first()->code
		])->with('search', $request->input('search'));
    }

    public function post_po(Request $request) {
        $save = false;
        $po = create_po($this->user, $request);
        if ($po) {
            $save = true;
        } else {
            $save = false;
        }


        $orders = Order::where('order_number', $request->order_number)->first();
        $carts = Cart::where('cart_number', $orders->cart_number)->get();
        foreach($carts as $cart) {
            $products = new Product;
            $products->setConnection($cart->product_froms->katalog);
            $products = $products->find($cart->product_id);

            $batchs = new Batch;
            $batchs->setConnection($cart->product_froms->katalog);
            $batchs = $batchs->find($products->batch->id);

            $bijis = new Biji;
            $bijis->setConnection($cart->product_froms->katalog);
            $bijis = $bijis->find($batchs->biji_id);

            $jenis = new Jenis;
            $jenis->setConnection($cart->product_froms->katalog);
            $jenis = $jenis->find($batchs->jenis_id);

            $proses = new Proses;
            $proses->setConnection($cart->product_froms->katalog);
            $proses = $proses->find($batchs->proses_id);

            $supplier = new Supplier;
            $supplier->setConnection($cart->product_froms->katalog);
            $supplier = $supplier->find($batchs->supplier_id);

            $unit = new Unit;
            $unit->setConnection($cart->product_froms->katalog);
            $unit = $unit->find($products->unit);

            $roasterBiji = new RoasterBiji;
            $roasterBiji->setConnection($cart->product_froms->katalog);
            $roasterBiji = $roasterBiji->where('nama_biji', $bijis->nama_biji)->where('deskripsi_biji', $bijis->deskripsi_biji)->first();
            if(!isset($roasterBiji)) {
                $roasterBiji = new RoasterBiji;
                $roasterBiji->setConnection($cart->product_froms->katalog);
                $roasterBiji->nama_biji = $bijis->nama_biji;
                $roasterBiji->deskripsi_biji = $bijis->deskripsi_biji;
                $roasterBiji->save();
            }

            $roasterJenis = new RoasterJenis;
            $roasterJenis->setConnection($cart->product_froms->katalog);
            $roasterJenis = $roasterJenis->where('nama_jenis', $jenis->nama_jenis)->where('deskripsi_jenis', $jenis->deskripsi_jenis)->first();
            if(!isset($roasterJenis)) {
                $roasterJenis = new RoasterJenis;
                $roasterJenis->setConnection($cart->product_froms->katalog);
                $roasterJenis->nama_jenis = $jenis->nama_jenis;
                $roasterJenis->deskripsi_jenis = $jenis->deskripsi_jenis;
                $roasterJenis->save();
            }

            $roasterProses = new RoasterProses;
            $roasterProses->setConnection($cart->product_froms->katalog);
            $roasterProses = $roasterProses->where('nama_proses', $proses->nama_proses)->where('deskripsi_proses', $proses->deskripsi_proses)->first();
            if(!isset($roasterProses)) {
                $roasterProses = new RoasterProses;
                $roasterProses->setConnection($cart->product_froms->katalog);
                $roasterProses->nama_proses = $proses->nama_proses;
                $roasterProses->deskripsi_proses = $proses->deskripsi_proses;
                $roasterProses->save();
            }

            $roasterSupplier = new RoasterSupplier;
            $roasterSupplier->setConnection($cart->product_froms->katalog);
            $roasterSupplier = $roasterSupplier->where('nama_supplier', $supplier->nama_supplier)->where('lokasi_supplier', $supplier->lokasi_supplier)->where('nama_petani', $supplier->nama_petani)->first();
            if(!isset($roasterSupplier)) {
                $roasterSupplier = new RoasterSupplier;
                $roasterSupplier->setConnection($cart->product_froms->katalog);
                $roasterSupplier->nama_supplier = $supplier->nama_supplier;
                $roasterSupplier->lokasi_supplier = $supplier->lokasi_supplier;
                $roasterSupplier->lat = $supplier->lat;
                $roasterSupplier->long = $supplier->long;
                $roasterSupplier->nama_petani = $supplier->nama_petani;
                $roasterSupplier->gambar_petani = $supplier->gambar_petani;
                $roasterSupplier->elevation = $supplier->elevation;
                $roasterSupplier->unit = $supplier->unit;
                $roasterSupplier->estimated_production = $supplier->estimated_production;
                $roasterSupplier->save();
            }

            $roasterUnit = new RoasterUnit;
            $roasterUnit->setConnection($cart->product_froms->katalog);
            $roasterUnit = $roasterUnit->where('nama_unit', $unit->nama_unit)->first();
            if(!isset($roasterUnit)) {
                $roasterUnit = new RoasterUnit;
                $roasterUnit->setConnection($cart->product_froms->katalog);
                $roasterUnit->nama_unit = $unit->nama_unit;
                $roasterUnit->save();
            }

            $roasterProducts = new RoasterProduct;
            $roasterProducts->setConnection($cart->product_froms->katalog);
            $roasterProducts->productID = $products->id;
            $roasterProducts->name = $products->name;
            $roasterProducts->biji_id = $roasterBiji->id;
            $roasterProducts->jenis_id = $roasterJenis->id;
            $roasterProducts->proses_id = $roasterProses->id;
            $roasterProducts->supplier_id = $roasterSupplier->id;
            $roasterProducts->save();

            $roasterInventory = new RoasterInventory;
            $roasterInventory->setConnection($cart->product_froms->katalog);
            $roasterInventory->bean_id = $roasterProducts->id;
            $roasterInventory->weight = $products->weight;
            $roasterInventory->unit = $roasterUnit->nama_unit;
            $roasterInventory->save();
        }

        $workOrder    = $orders->work_order;
        abort_unless($this->user->can('createpo', $orders), 403);
        abort_unless($workOrder, 404);

        $states       = $orders->work_order->state->name;
        $workOrder->submitAction($this->user, 'send');


        if ($save) {
            $post["status"] = true;
			return redirect()->route('katalog.monitoring.po.index', [$this->user->group()->first()->code, Crypt::encryptString('po')]);
		} else {
            $post["status"] = false;
			return redirect()->route('katalog.monitoring.po.index', [$this->user->group()->first()->code, Crypt::encryptString('po')]);
		}
    }

    public function cetakpdf(Request $request, $code, $po_id) {
        DB::setDefaultConnection($this->user->group()->first()->katalog);
        $po = new po;
        $po->setConnection('mysql');
        $po = $po->find($po_id);

        // return view('katalog::monitoring.po.pdf2',[
		// 	'po' => $po,
        //     'style' => $this->get_style(),
		// ]);

        $pdf = PDF::loadView('katalog::monitoring.po.pdf2', [
            'po' => $po,
            'style' => $this->get_style()
        ]);
        $pdf->save(storage_path().'/app/public/files/PO/'.'_' . $po->po_number . '.pdf');
        return $pdf->download($po->po_number . '.pdf');
    }
}
