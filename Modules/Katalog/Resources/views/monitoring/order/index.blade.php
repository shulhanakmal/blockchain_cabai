@extends('katalog::layouts.app')
@section('content')
<div class="main-content shop-page main-content-detail">
    <div class="container">
        <div class="breadcrumbs">
            <a>List Order</a>
        </div>
        <div class="row content-form">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 content-offset">
                <div class="table-responsive">
                    <table id="compare" class="shopping-cart-content table table-scroll small-first-col">
                        <thead style="background-color:#cc00006e; color:white; vertical-align:middle">
                            <tr>
                                <th class="text-center" style="color: white; font-size: 16px; font-weight:bold;">Order No.</td>
                                <th class="text-center" style="color: white; font-size: 16px; font-weight:bold;">Job Desc</td>
                                <th class="text-center" style="color: white; font-size: 16px; font-weight:bold;">To</td>
                                <th class="text-center" style="color: white; font-size: 16px; font-weight:bold;">Total</td>
                                <th class="text-center" style="color: white; font-size: 16px; font-weight:bold;">Date</td>
                                <th class="text-center" style="color: white; font-size: 16px; font-weight:bold;">Status</td>
                                <th class="text-center" style="color: white; font-size: 16px; font-weight:bold;"></td>
                            </tr>
                        </thead>
                        <tbody class="body-half-screen">
                            @foreach ($order as $key => $o)
                            <tr class="each-item" id="rowqty{{ $o->id }}">
                                <td style="text-align:left; vertical-align:middle" class="product-name" data-title="Product Name">
                                    <a class="product-name">
                                        {{ $o->order_number }}
                                    </a>
                                </td>
                                <td style="text-align:left;vertical-align:middle" class="product-name" data-title="Product Name">
                                    <a class="product-name">
                                        {{ $o->name }}
                                    </a>
                                </td>
                                <td style="text-align:left;vertical-align:middle" class="product-name" data-title="Product Name">
                                    <a class="product-name">
                                        {{ $o->order_address->first_name }} {{ $o->order_address->last_name }}
                                        <br>
                                        {{ $o->order_address->address }},  {{ $o->order_address->city }}, {{ $o->order_address->province }}
                                    </a>
                                </td>
                                <td style="text-align:right;vertical-align:middle; color: red;" class="price" data-title="Unit Price" id="price{{ $o->id }}">
                                    {{Auth::user()->group->currency}}. {{ number_format($o->order_total, 0) }}
                                </td>
                                <td style="text-align:center;vertical-align:middle" class="product-name" data-title="Product Name">
                                    <a class="product-name">
                                        {{ date('d-M-Y', strtotime($o->created_at)) }}
                                    </a>
                                </td>
                                <td style="text-align:left;vertical-align:middle" class="product-name" data-title="Product Name">
                                    <a class="product-name">
                                        {{-- {{ $o->order_status->name }} --}}
                                        {{ get_current_state($o) }}
                                    </a>
                                </td>
                                <td style="text-align:center;vertical-align:middle" class="icon">
                                    <a href="{{ route('katalog.monitoring.order.detail', [Auth::user()->group()->first()->code, Illuminate\Support\Facades\Crypt::encryptString($o->order_number)]) }}" class="btn btn-primary">
                                        <i class="fa fa-search" style="color:white"></i>
                                    </a>
                                    @can('createpo', $o)
                                    <a href="{{ route('katalog.monitoring.po.create', [Auth::user()->group()->first()->code, Illuminate\Support\Facades\Crypt::encryptString($o->order_number)]) }}" class="btn btn-primary">
                                        Create PO
                                    </a>
                                    @endcan
                                    {{-- @if($o->order_status->name == 'Telah Dikonfirmasi')
                                    <a href="{{ route('katalog.monitoring.finish', [Auth::user()->group()->first()->code, Illuminate\Support\Facades\Crypt::encryptString($o->order_number)]) }}" class="btn btn-primary">
                                        Complete Order
                                    </a>
                                    @elseif($o->order_status->name == 'Selesai')
                                    <a href="{{ route('katalog.monitoring.po.create', [Auth::user()->group()->first()->code, Illuminate\Support\Facades\Crypt::encryptString($o->order_number)]) }}" class="btn btn-primary">
                                        Create PO
                                    </a>
                                    @endif --}}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @if(count($order) == 0)
                <div class="main-content shop-page page-404">
                    <div>
                        <img src="{{ asset('techone/images/icon/noproduct.png') }}" style="width:250px;height:140px;margin: auto; top: 0; left: 0; right: 0; bottom: 0;">
                        <h2 class="title">Sorry, No Order Found </h2>
                    </div>
                </div>
                @endif
                <div class="table-responsive" style="border:none;">
                    <table class="shopping-cart-content">
                        <thead>
                        </thead>
                        <tbody>
                        <tr class="checkout-cart group-button">
                            <td colspan="6" class="left">
                                <div class="left">
                                    <a href="{{ route('katalog.index') }}" class="continue-shopping submit">Continue Shopping</a>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
</script>
@endpush
