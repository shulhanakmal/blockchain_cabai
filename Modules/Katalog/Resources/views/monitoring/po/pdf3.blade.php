<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> kunci.io</title>
    <link href="{{ asset('techone/images/pngap2.png') }}" rel="icon">
    <link href="{{ asset('techone/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('techone/css/owl.carousel.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('techone/css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('techone/css/magnific-popup.min.css') }}" rel="stylesheet">
    <link href="{{ asset('techone/css/jquery-ui.min.css') }}" rel="stylesheet">
    <link href="{{ asset('techone/css/jquery.scrollbar.min.css') }}" rel="stylesheet">
    <link href="{{ asset('techone/css/chosen.min.css') }}" rel="stylesheet">
    <link href="{{ asset('techone/css/ovic-mobile-menu.css') }}" rel="stylesheet">
    <link href="{{ asset('techone/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('techone/css/customs-css5.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/bootstrap-daterangepicker/css/daterangepicker.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('assets/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/node_modules/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet">
</head>
<body>
    <div class="main-content">
        <div class="container">
            <div style="width:100%;" class="content-form">
                <table style="border:none;" class="table col-xs-12" cellspacing="5">
                    <tr class="row" style="border:none;">
                        <td width="50%" style="border:none;">
                            <div class="breadcrumbs" style="margin-top: 20px;">
                                <h3>INVOICE</h3>
                                <span class="label-text" style="font-size:14px; text-transform: capitalize;">PO No. {{ $po->po_number }}</span><br>
                            </div>
                        </td>
                        <td width="50%" style="border: none;">
                            <div class="breadcrumbs" style="position: absolute; right: 0px;">
                                <img src="{{ public_path("techone/images/" . $style->logo) }}" width="150" height="70"/>
                                <br>
                                {{-- {{ dd(date('F d, Y', strtotime($po->order->invoice_date))) }} --}}
                                <span class="label-text" style="font-size:14px; text-transform: capitalize;">Date : {{ date('F d, Y', strtotime($po->order->invoice_date)) }}</span><br>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>

            <div style="width: 100%; margin-top: 30px;" class="content-form">
                <table style="border:none;" class="table col-xs-12" cellspacing="5">
                    <tr class="row" style="border:none;">
                        <td width="40%" style="border:none; vertical-align:top;">
                            <span class="label-text" style="font-size:14px;"><b>To : </b></span><br><br>
                            <span class="label-text" style="font-size:14px;"><b>{{ $po->order->order_address->first_name }} {{ $po->order->order_address->last_name }}</b></span><br>
                            <span class="label-text" style="font-size:14px;">{{ $po->order->order_address->address }}</span><br>
                            <span class="label-text" style="font-size:14px;">{{ $po->order->order_address->city }}, {{ $po->order->order_address->province }}</span><br>
                            <span class="label-text" style="font-size:14px;">{{ $po->order->order_address->postcode }}</span><br>
                        </td>
                        <td width="40%" style="border:none; vertical-align:top;">

                        </td>
                        <td width="20%" style="border:none; vertical-align:top;">

                        </td>
                    </tr>
                </table>
                <table style="border:none; width: 100%;" class="table col-xs-12" cellspacing="5">
                    <tr class="row" style="border:none;">
                        <td width="40%" style="border:none; vertical-align:top;">
                            <span class="label-text" style="font-size:14px; text-transform: capitalize;">Order No. {{ $po->order_number }}</span><br>
                        </td>
                        <td width="40%" style="border:none; vertical-align:top;">

                        </td>
                        <td width="20%" style="border:none; vertical-align:top;">

                        </td>
                    </tr>
                </table>
                <br>
                <table class="table table-striped" style="width: 100%; border: 1px solid black;">
                    <thead style="width: 100%; border-bottom: 1px solid black;">
                        <tr class="title" style="font-size: 14px; vertical-align:middle;">
                            <th style="border-right: 1px solid black;">Job Desc</td>
                            <th style="border-right: 1px solid black;">Vol.</td>
                            <th style="border-right: 1px solid black;">Unit.</td>
                            <th style="border-right: 1px solid black;">Price / Unit</td>
                            <th>Total Price</td>
                        </tr>
                    </thead>
                    <tbody style="font-size:12px;">
                        @foreach ($po->po_detail()->get() as $d)
                        <tr class="each-item">
                            <td style="text-align: left; border-right: 1px solid black; border-bottom: 1px solid black;" id="uraian_barang">{{ $d->uraian_pekerjaan }}</td>
                            <td style="text-align: center; border-right: 1px solid black; border-bottom: 1px solid black;" id="volume_barang">{{ $d->volume }}</td>
                            <td style="text-align: center; border-right: 1px solid black; border-bottom: 1px solid black;" id="satuan_barang">
                                @php
                                    $units = new \App\Unit;
                                    $units = $units->setConnection($d->po->order->group->katalog);
                                    $units = $units->find($d->satuan);
                                @endphp
                                {{ $units->nama_unit }}
                                {{-- {{ $d->satuan }} --}}
                            </td>
                            <td style="text-align: right; border-right: 1px solid black; border-bottom: 1px solid black;" id="harga_barang">
                                {{ $d->po->vendor_froms->currency }} {{ number_format($d->harga_ecatalogue_tanpa_ppn, 0) }}
                            </td>
                            <td style="text-align: right;  border-bottom: 1px solid black;" id="jumlah_harga_barang">
                                {{ $d->po->vendor_froms->currency }} {{ number_format($d->jumlah_tanpa_ppn, 0) }}
                            </td>
                        </tr>
                        @endforeach
                        @foreach ($po->po_detail_biaya_pengiriman()->get() as $os)
                        <tr class="each-item">
                            <td style="text-align: left; border-right: 1px solid black; border-bottom: 1px solid black;" id="uraian_barang">{{ $os->uraian_pekerjaan }}</td>
                            <td style="text-align: left; border-right: 1px solid black; border-bottom: 1px solid black;" id="volume_barang"></td>
                            <td style="text-align: left; border-right: 1px solid black; border-bottom: 1px solid black;" id="satuan_barang"></td>
                            <td style="text-align: right; border-right: 1px solid black; border-bottom: 1px solid black;" id="harga_barang">
                                {{ $d->po->vendor_froms->currency }} {{ number_format($os->harga_ecatalogue_tanpa_ppn, 0) }}
                            </td>
                            <td style="text-align: right; border-bottom: 1px solid black;" id="jumlah_harga_barang">
                                {{ $d->po->vendor_froms->currency }} {{ number_format($os->jumlah_tanpa_ppn, 0) }}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot style="font-size:12px;">
                        <tr>
                            <td style="text-align:right; border-right: 1px solid black;" colspan="4">
                                Grand Total
                            </td>
                            <td style="border-top:none;border-bottom:none;text-align:right;vertical-align:middle;" colspan="1">
                                {{ $d->po->vendor_froms->currency }} {{ number_format($po->po_total_jumlah_tanpa_ppn->subtotal, 0) }}
                            </td>
                        </tr>
                    </tfoot>
                </table>
                <br>
                <table style="border:none;" class="table col-xs-12" cellspacing="5">
                    <tr class="row" style="border:none;">
                        <?php
                            $f = new NumberFormatter('en', NumberFormatter::SPELLOUT);
                            $curr = new \App\MCurrencies;
                            $curr->setConnection('mysql');
                            $curr = $curr->where('code', $d->po->vendor_froms->currency)->first();
                        ?>
                        <td width="40%" style="border:none; vertical-align:top;">
                            <span class="label-text" style="font-size:14px;"><b>Amount : </b></span><br>
                            <span class="label-text" style="font-size:14px;"><b>{{ $d->po->vendor_froms->currency }} {{ number_format($po->po_total_jumlah_tanpa_ppn->subtotal, 0) }} ({{ $f->format($po->po_total_jumlah_tanpa_ppn->subtotal) }} {{ $curr->name }})</b></span><br>
                        </td>
                        <?php

                        ?>
                        <td width="40%" style="border:none; vertical-align:top;">

                        </td>
                        <td width="20%" style="border:none; vertical-align:top;">

                        </td>
                    </tr>
                </table>
                <table style="border:none;" class="table col-xs-12" cellspacing="5">
                    <tr class="row" style="border:none;">
                        <?php
                            $vendor = new \App\Vendor\VendorMaster;
                            $vendor_master = $vendor->setConnection('mysql');
                            $group = new \App\Group\group;
                            $group = $group->setConnection('mysql');
                            $vendor_master = $vendor_master->where('vendor_katalog_id', $po->vendor_id)->where('group_id', $group->select('id')->where('code',$po->vendor_from)->first()->id)->first();
                        ?>
                        <td width="40%" style="border:none; vertical-align:top;">
                            <span class="label-text" style="font-size:14px;"><b>To : </b></span><br><br>
                            <span class="label-text" style="font-size:14px;"><b>{{ $vendor_master->vendor_name }}</b></span><br>
                            <span class="label-text" style="font-size:14px;">{{ $vendor_master->vendor_address_master->street }}<br>{{ $vendor_master->vendor_address_master->city }}, {{ $vendor_master->vendor_address_master->region }}</span><br>
                        </td>
                        <?php

                        ?>
                        <td width="40%" style="border:none; vertical-align:top;">

                        </td>
                        <td width="20%" style="border:none; vertical-align:top;">

                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <script src="{{ asset('techone/js/jquery-2.1.4.min.js') }}"></script>
    <script src="{{ asset('techone/js/bootstrap.min.js') }}" ></script>
    <script src="{{ asset('techone/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('techone/js/owl.thumbs.min.js') }}"></script>
    <script src="{{ asset('techone/js/magnific-popup.min.js') }}"></script>
    <script src="{{ asset('techone/js/ovic-mobile-menu.js') }}"></script>
    <script src="{{ asset('techone/js/mobilemenu.min.js') }}"></script>
    <script src="{{ asset('techone/js/jquery.plugin-countdown.min.js') }}"></script>
    <script src="{{ asset('techone/js/jquery-countdown.min.js') }}"></script>
    <script src="{{ asset('techone/js/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('techone/js/jquery.scrollbar.min.js') }}"></script>
    <script src="{{ asset('techone/js/chosen.min.js') }}"></script>
    <script src="{{ asset('techone/js/frontend.js') }}"></script>
    <script src="{{ asset('assets/node_modules/sweetalert2/dist/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('assets/node_modules/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('assets/node_modules/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/node_modules/datatables.net/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/node_modules/datatables.net-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.bootstrap4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.colVis.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
    <script>
    </script>
</body>
</html>
