@extends('katalog::layouts.app')
@section('content')
<div class="main-content shop-page main-content-detail">
    <div class="container">
        <div class="breadcrumbs">
            <a>Product Details</a>
        </div>
        <div class="row">

            @include('katalog::product.detail.descriptions')

            @include('katalog::product.detail.related_products')

        </div>
        <a class="kembali" href="{{ url()->previous() }}">Back</a>
    </div>
</div>
@endsection
@push('scripts')
<script>
    function qty(stat, stock) {
        var inputid = "qty";
        var qty = parseInt(document.getElementById(inputid).value);

        if (stat == "plus") {
            if(qty+1 <= stock) {
                $("#qty").val(qty+1);
            }
        } else if (stat == "min") {
            if((qty-1) != 0) {
                $("#qty").val(qty-1);
                if (qty == 1) {
                    document.getElementById("minus").disabled = true;
                } else if (qty > 1) {

                }
            }
        }
    }
</script>
@endpush
