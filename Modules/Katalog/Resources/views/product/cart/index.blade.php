@extends('katalog::layouts.app')
@push('styles')
@endpush
@section('content')
<div class="main-content shop-page main-content-detail">
    <div class="container">
        <div class="breadcrumbs">
            <a>Cart</a>
        </div>
        <form method="post" action="{{ route('katalog.cart.update', [Auth::user()->group->code]) }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="datacart_id[]" id="datacart_id">
            @if (Session::has('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
            @endif
            @if (Session::has('error'))
            <div class="alert alert-danger" role="alert">
                {{ session('error') }}
            </div>
            @endif
            <div class="row content-form">
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-9 content-offset">
                    <div class="table-responsive">
                        <table class="shopping-cart-content table">
                            <thead style="font-size: 12px; background-color:#cc00006e; color:white; font-size: 16px; font-weight:bold; vertical-align:middle">
                                <tr>
                                    <th width="15%" class="text-center"></td>
                                    <th width="25%" class="text-center">Product Name</td>
                                    <th width="20%" class="text-center">Price</td>
                                    <th width="25%" class="text-center">Qty</td>
                                    <th width="20%" class="text-center">Sub Total</td>
                                    <th width="5%" class="delete-item"></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $total = 0;
                                    $totalPervendor = 0;
                                    $totals = 0;
                                ?>
                                @foreach ($data['vendor']->distinct()->get() as $index1 => $v)
                                    <td colspan="3" style="text-align:left;vertical-align:middle;font-weight:bold;font-size:16px;background-color:#cc00006e;" class="product-name" data-title="Product Name">
                                        <span style="color:white">
                                            {{ $v->vendor_name }} - {{ $v->product_from }}
                                        </span>
                                    </td>
                                    <td colspan="3" class="delete-item" style="text-align:left;vertical-align:middle;width:auto;background-color:#cc00006e;">
                                        <button type="button" onclick="checkUpdateCart('{{ $v->vendor_id }}', '{{ $v->product_from }}', '{{ $index1 }}');" class="pull-right btn btn-secondary">Proceed Checkout</button>
                                    </td>
                                    @foreach ($data['data']->get() as $index2 => $d)
                                        @if ($d->vendor_id == $v->vendor_id && $d->product_from == $v->product_from)
                                        <input type="hidden" class="input-info" name="cart_number" value="{{ $d->cart_number }}">
                                        <input type="hidden" class="input-info" name="cart_id{{ $index1 }}[]" value="{{ $d->id }}">
                                        <tr class="each-item" id="rowqty{{ $d->id }}{{ $index1 }}" style="border-bottom:none;line-height:30px;">
                                            <td width="15%" class="product-thumb">
                                                @if($d->image == null)
                                                <div style="height:100px;width:100px;">
                                                    <img src="{{ asset('techone/images/product_placeholder.jpg') }}" style="width:100px;height:100px;">
                                                </div>
                                                @else
                                                <div style="height:100px;width:100px;">
                                                    <img src="{{ $d->image }}" style="width:100px;height:100px;">
                                                </div>
                                                @endif
                                            </td>
                                            <td width="25%" style="text-align:left;vertical-align:middle" class="product-name" data-title="Product Name">
                                                {{ $d->name }}
                                            </td>
                                            <td width="20%" style="text-align:right;vertical-align:middle" class="price" data-title="Unit Price" id="price{{ $d->id }}{{ $index1 }}">
                                                {{ number_format($d->price, 0) }}
                                            </td>
                                            <td width="25%" style="text-align:center;vertical-align:middle" class="quantity-item" data-title="Qty">
                                                <div class="quantity">
                                                    <div class="group-quantity-button">
                                                        @php
                                                        $prods = new \App\Product\Product;
                                                        $prods->setConnection($d->product_froms->katalog);
                                                        $prods = $prods->find($d->product_id);
                                                        @endphp
                                                        <a class="minus btn btn-default" id="minus{{ $d->id }}{{ $index1 }}" onclick="qty('min','{{ $d->id }}','{{ $d->price }}', '{{ $prods->product_vendors->stock }}', '{{ $index1 }}', '{{Auth::user()->group->currency}}')">
                                                            <i class="fa fa-minus" aria-hidden="true"></i>
                                                        </a>
                                                        <input class="input-text qty text" id='qty{{ $d->id }}{{ $index1 }}' type="text" size="4" title="Qty" value="{{ $d->qty }}" name="qty{{ $d->id }}" oninput="inputqty('{{ $d->id }}','{{ $d->price }}', '{{ $prods->product_vendors->stock }}', '{{ $index1 }}', '{{Auth::user()->group->currency}}')">
                                                        <a class="plus btn btn-default" id="plus{{ $d->id }}{{ $index1 }}" onclick="qty('plus','{{ $d->id }}','{{ $d->price }}', '{{ $prods->product_vendors->stock }}', '{{ $index1 }}', '{{Auth::user()->group->currency}}')">
                                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                            <?php $subtotal = $d->price * $d->qty; ?>
                                            <?php $total += $d->price * $d->qty; ?>
                                            <?php $totals += $d->price * $d->qty; ?>
                                            <input type="hidden" id="subtotals{{ $d->id }}{{ $index1 }}" name="subtotalinput{{ $index1 }}[]" value="{{ number_format($subtotal, 0) }}">
                                            <td width="20%" style="text-align:right;vertical-align:middle" class="total" id="subtotal{{ $d->id }}{{ $index1 }}" data-title="SubTotal">
                                                {{ number_format($subtotal, 0) }}
                                            </td>
                                            <td width="5%" style="text-align:center;vertical-align:middle" class="icon" data-title="Remove">
                                                <a style="background-color: #EFEFEF; color: black; vertical-align:middle" class="btn btn-secondary" onclick="showAlert('{{ $d->id }}',' {{ $index2 }}', '{{ $index1 }}')">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        @endif
                                        <?php
                                            $totalPervendor = $totals;
                                        ?>
                                    @endforeach
                                    <?php
                                        $totals = 0;
                                    ?>
                                    <tr style="border-bottom:none;">
                                        <td style="font-size:14px;color:#555;font-weight:bold;text-align:left;vertical-align:middle; background-color: #F9F7F6;" align="left" colspan="2">
                                            Total
                                            <br>
                                            <br>
                                            <br>
                                        </td>
                                        <td style="border-bottom:none;text-align:right;vertical-align:middle; background-color: #F9F7F6;" class="price" align="right" colspan="3">
                                            <span id="span{{ $index1 }}" >{{Auth::user()->group->currency}}. {{ number_format($totalPervendor, 0) }}</span>
                                            <br>
                                            <br>
                                            <br>
                                        </td>
                                        <td style="border-bottom:none;text-align:right;vertical-align:middle;background-color: #F9F7F6;" class="price" align="right" colspan="1">
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @if(count($data['data']->get()) == 0)
                    <div class="main-content shop-page page-404">
                        <div>
                            <img src="{{ asset('techone/images/icon/noproduct.png') }}" style="width:250px;height:140px;margin: auto; top: 0; left: 0; right: 0; bottom: 0;">
                            <h2 class="title">Maaf, tidak ada produk dalam keranjang </h2>
                        </div>
                    </div>
                    @endif
                    <div class="table-responsive" style="border:none;">
                        <table class="shopping-cart-content">
                            <thead>
                            </thead>
                            <tbody>
                            <tr class="checkout-cart group-button">
                                <td colspan="6" class="left">
                                    <div class="left">
                                        <a href="{{ route('katalog.index') }}" class="continue-shopping submit">Continue Shopping</a>
                                    </div>
                                    <div class="right">
                                        <button type="submit" class="submit continue-shopping">Update Cart</button>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="proceed-checkout">
                                <h4 class="main-title">Most recent booking</h4>
                                <div class="content">
                                    <h5 class="title">Total Cart</h5>
                                    <div class="info-checkout" style="display: none;">
                                        <span class="text">Sub Total : </span>
                                        <span class="item" id="granditem"><?= number_format($total, 0) ?></span>
                                    </div>
                                    <div class="total-checkout" style="border:none;">
                                        <span class="text" style="font-size: 16px;">Total </span>
                                        <span class="price" id="grandtotal">{{Auth::user()->group->currency}}. <?= number_format($total, 0) ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="equal-container widget-featrue-box">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="featrue-item">
                                            <div class="featrue-box layout2 equal-elem">
                                                <div class="block-icon"><a><span class="fa fa-life-ring"></span></a></div>
                                                <div class="block-inner">
                                                    <a class="title">Online support 24/7</a>
                                                    <p class="des">Online support 24/7</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <ul class="list-socials">
                                            <li><a><i class="fa fa-facebook" aria-hidden="true">&nbsp;&nbsp;&nbsp;Facebook</i></a></li>
                                            <li><a><i class="fa fa-twitter" aria-hidden="true">&nbsp;&nbsp;&nbsp;Twitter</i></a></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-7">
                                        <ul class="list-socials">
                                            <li><a><i class="fa fa-phone" aria-hidden="true">&nbsp;&nbsp;&nbsp;(021)22737851</i></a></li>
                                            <li><a><i class="fa fa-envelope" aria-hidden="true">&nbsp;&nbsp;&nbsp;cs@inamart.co.id</i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="widget widget-banner row-banner">
                                <div class="banner banner-effect1">
                                    <a><img src="images/banner23.jpg" alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="proceed-checkout-modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-primary modal-sm">
        <form id="cout" method="post" action="{{ route('katalog.checkout.index', [Auth::user()->group->code]) }}">
            {{ csrf_field() }}
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Proceed Checkout</h4>
                </div>
                <div class="modal-body" >
                    <div class="form-group row">
                        <input type="hidden" name="order_number" id="order_number"/>
                    </div>
                    <h5>
                        Are you sure you're going to process checkout?
                    </h5>
                    <div id="cartIdPlace">

                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary pull-left" type="button" data-dismiss="modal">No</button>
                    <button class="btn btn-primary pull-right" type="submit">Yes</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="update-cart-modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-primary modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Proceed Checkout</h4>
            </div>
            <div class="modal-body" >
                <div class="form-group row">
                    <input type="hidden" name="order_number" id="order_number"/>
                </div>
                <h5>
                    You haven't update cart
                </h5>
                <input type="hidden" name="product_from" id="product_from">
                <input type="hidden" name="vendor_id" id="vendor_id">
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary pull-left" type="button" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="delete-item-modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-primary modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Delete Item</h4>
            </div>
            <div class="modal-body" >
                <div class="form-group row">
                    <input type="hidden" name="order_number" id="order_number"/>
                </div>
                <h5>
                    Are you sure?
                </h5>
                <input type="hidden" name="id" id="id">
                <input type="hidden" name="idarr" id="idarr">
                <input type="hidden" name="index1" id="index1">
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary pull-left" type="button" data-dismiss="modal">No</button>
                <button class="btn btn-primary pull-right" type="button" onclick="removecart(document.getElementById('id').value, document.getElementById('idarr').value, document.getElementById('index1').value, '{{Auth::user()->group->currency}}')">Yes</button>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    var updateCart = false;

    var table_list = null;
    table_list = $('#tabel-cart').DataTable();
    var totals = parseInt(<?= $total ?>);
    var arridcart = <?= json_encode($data['data']->get()) ?>;
    $("#datacart_id").val(JSON.stringify(arridcart));
    var data = <?= $data['data']->get() ?>;

    function inputqty(id, price, stock, index1, currency) {
        console.log(id);
        console.log(price);
        console.log(stock);
        console.log(index1);

        updateCart = true;
        var inputid = "qty" + id+index1;
        var subid = "subtotal" + id+index1;
        var subids = "subtotals" + id+index1;
        var priceid = "price" + id+index1;
        var total = totals;
        var qty = parseInt(document.getElementById(inputid).value);

        console.log(total);
        if(qty == 0 || document.getElementById(inputid).value == '') {
            document.getElementById(inputid).value = 1;
            total = total - parseInt((document.getElementById(subid).innerText).replace(/\,/g,''));
            document.getElementById(subid).innerText = (1 * parseInt(price)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            total = total + parseInt((document.getElementById(subid).innerText).replace(/\,/g,''));
        } else {
            total = total - parseInt((document.getElementById(subid).innerText).replace(/\,/g,''));
            document.getElementById(subid).innerText = (qty * parseInt(price)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            total = total + parseInt((document.getElementById(subid).innerText).replace(/\,/g,''));
            if(qty > stock) {
                document.getElementById(inputid).value = stock;
                total = total - parseInt((document.getElementById(subid).innerText).replace(/\,/g,''));
                document.getElementById(subid).innerText = (stock * parseInt(price)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                total = total + parseInt((document.getElementById(subid).innerText).replace(/\,/g,''));
                console.log(document.getElementById(inputid).value);
                console.log(stock);
            }
        }
        totals = total;
        document.getElementById("granditem").innerHTML = "" + totals.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        document.getElementById("grandtotal").innerHTML = currency + ". " + totals.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    function qty(stat, id, price, stock, index1, currency) {

        updateCart = true;
        var inputid = "qty" + id+index1;
        var subid = "subtotal" + id+index1;
        var subids = "subtotals" + id+index1;
        var priceid = "price" + id+index1;
        var total = totals;
        var qty = parseInt(document.getElementById(inputid).value);

        if (stat == "plus") {
            if(qty+1 <= stock) {
                $("#qty" + id+index1).val(qty+1);
                total = total - parseInt((document.getElementById(subid).innerText).replace(/\,/g,''));
                document.getElementById(subid).innerText = ((qty + 1) * parseInt((document.getElementById(priceid).innerText).replace(/\,/g,''))).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                document.getElementById(subids).value = ((qty + 1) * parseInt((document.getElementById(priceid).innerText).replace(/\,/g,''))).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                total = total + parseInt((document.getElementById(subid).innerText).replace(/\,/g,''));
            }
        } else if (stat == "min") {
            if((qty-1) != 0) {
                $("#qty" + id+index1).val(qty-1);
                if (qty == 1) {
                    document.getElementById("minus" + id+index1).disabled = true;
                } else if (qty > 1) {
                    document.getElementById("minus" + id+index1).disabled = false;
                    total = total - parseInt((document.getElementById(subid).innerText).replace(/\,/g,''));
                    document.getElementById(subid).innerText = ((qty - 1) * parseInt((document.getElementById(priceid).innerText).replace(/\,/g,''))).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    document.getElementById(subids).value = ((qty - 1) * parseInt((document.getElementById(priceid).innerText).replace(/\,/g,''))).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    total = total + parseInt((document.getElementById(subid).innerText).replace(/\,/g,''));
                }
            }
        }

        totals = total;
        document.getElementById("granditem").innerHTML = "" + totals.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        document.getElementById("grandtotal").innerHTML = currency +". " + totals.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");


        var inps = document.getElementsByName('subtotalinput'+index1+'[]');
        var totalPerVendor = 0;
        for (var i = 0; i <inps.length; i++) {
            var inp = inps[i];
            console.log(inp.value)
            totalPerVendor += parseInt(inp.value.replace(/\,/g,''));
        }
        console.log(totalPerVendor.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        document.getElementById("span"+index1).textContent = currency + '. ' + totalPerVendor.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    function showAlert(id, idarr, index1) {
        $("#delete-item-modal").modal('show');
        $("#delete-item-modal .modal-content .modal-body #id").val(id);
        $("#delete-item-modal .modal-content .modal-body #idarr").val(idarr);
        $("#delete-item-modal .modal-content .modal-body #index1").val(index1);
        // removecart(id, idarr)
    }

    function removecart(id, idarr, index1, currency) {
        $("#delete-item-modal").modal('hide');
        $('.modal-loading').modal('show');
        var subhtml  = "subtotal" + id + index1;
        var grandhtml  = "granditem";
        var subtotal = parseInt((document.getElementById(subhtml).innerText).replace(/\,/g,''));
        var grandtotal = parseInt((document.getElementById(grandhtml).innerHTML).replace(/\,/g,''));
        grandtotal = grandtotal - subtotal;
        $.ajax({
            type:'GET',
            url:'{{ route('katalog.cart.delete', [Auth::user()->group->code]) }}',
            data:{
                idcart: id
            },
            success:function(response){
                $('.modal-loading').modal('hide');
                var htmlrowqty = "#rowqty"+id;
                $(htmlrowqty).remove();
                document.getElementById("granditem").innerHTML= grandtotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                document.getElementById("grandtotal").innerHTML= currency + "." + grandtotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                delete arridcart[idarr];
                $("#datacart_id").val(JSON.stringify(arridcart));
                calculatecart();
                history.go(0);
            }
        });
    }

    function checkUpdateCart(vendor_id, product_from, index1) {
        if (updateCart) {
            $("#update-cart-modal").modal('show');
        } else {
            var inps = document.getElementsByName('cart_id'+index1+'[]');
            var totalPerVendor = 0;
            var html = '';
            for (var i = 0; i <inps.length; i++) {
                var inp = inps[i];
                html += '<input type="hidden" name="carts_id[]" id="carts_id" value="'+inp.value+'">';
            }

            $("#proceed-checkout-modal").modal('show');
            $("#proceed-checkout-modal .modal-content .modal-body #cartIdPlace").empty().append(html);
        }
    }
</script>
@endpush
