<footer>
    <div class="footer layout2 layout3">
        <div class="container">
            <div class="footer-note">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8 left-content">
                        <div class="coppy-right">
                            <h3 class="content">© Powered by <span class="site-name"> <a href="http://www.inamart.co.id" target="_blank">PT Inamart Sukses Jaya</a></span></h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

{{-- <footer>
    <div class="footer layout1 ">
        <div class="container">
            <div class="main-footer">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8">
                        <div class="coppy-right">
                            <h3 class="content">© Copyright 2019 <span class="site-name"> PT Inamart Sukses Jaya</span></h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer> --}}


<div class="modal modal-loading fade bd-example-modal-lg" data-backdrop="static" data-keyboard="false" tabindex="-1">
	<div class="modal-dialog modal-sm"><center>
		<div class="modal-content" style="width:75px;color:#1d66ad;background-color:white;padding:15px">
			<span class="fa fa-refresh fa-spin fa-3x"></span>
		</div>
	</div>
</div>
<div class="modal fade" id="assign-modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-primary modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Detail</h4>
            </div>
			<div class="modal-body">
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-danger" type="button" data-dismiss="modal">Close</button>
			</div>
        </div>
    </div>
</div>
<div class="modal fade" id="keranjang-modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" tabindex="-1">
    <div class="modal-dialog modal-primary modal-lg">
        <div class="modal-content">
            <div class="modal-header">
            </div>
			<div class="modal-body">
			</div>
			<div class="modal-footer">
				<button class="btn btn-secondary" type="button" data-dismiss="modal">Continue Shopping</button>
				<a class="btn btn-primary" href="{{ route('katalog.cart.index', [Auth::user()->group()->first()->code, Illuminate\Support\Facades\Crypt::encryptString('cart')]) }}">Buy</a>
			</div>
        </div>
    </div>
</div>
<div class="modal fade" id="perbandingan-modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-primary modal-lg">
        <div class="modal-content">
            <div class="modal-header">
            </div>
			<div class="modal-body">
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Continue Shopping</button>
				<a class="btn btn-sm btn-primary" href="{{ route('katalog.comparison.index', [Auth::user()->group()->first()->code, Illuminate\Support\Facades\Crypt::encryptString('comparison')]) }}">Check</a>
			</div>
        </div>
    </div>
</div>
<div class="modal fade" id="detail-checkout-modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-primary modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Booking Details</h4>
            </div>
			<div class="modal-body">
			</div>
			<div class="modal-footer">
				<button class="btn btn-sm btn-danger" type="button" data-dismiss="modal">Close</button>
			</div>
        </div>
    </div>
</div>
<div class="modal fade" id="detail-order-modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-primary modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Order Details</h4>
            </div>
			<div class="modal-body">
			</div>
			<div class="modal-footer">
				<button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
				<a class="btn btn-primary" onclick="checkout()">Checkout</a>
			</div>
        </div>
    </div>
</div>
<div class="modal fade" id="catatan-customer-modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-primary modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Note</h4>
            </div>
            <div class="modal-body" >
                <div class="form-group row">
                    <input type="hidden" name="order_number" id="order_number"/>
                </div>
                <div id="body-catatan">
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary pull-left" type="button" data-dismiss="modal">Close</button>
                <button class="btn btn-primary pull-right" type="button" onclick="checkCatatanCustomer()">Send</button>
            </div>
        </div>
    </div>
</div>
